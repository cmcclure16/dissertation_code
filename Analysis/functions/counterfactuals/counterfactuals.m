function [ counter_effect ] = counterfactuals(d, params, params_counter, Rmat, xmat, simDat, audMat, shocks, real_data, counter)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Function Name: 	counterfactuals
% Date created: 	2018-11-13                                                                          
% Author: 			Charles McClure                                                                     
% Purpose: 			Computes the effects from counterfactuals
%------------------------------------------------------------------------------------------------------ 
% Inputs:  d - dataset with parameters not estimated                                                    %
%          params - vector of parameters for baseline results                                                               %
%          params_counter - vector of parameters for counterfactual dataset%
%          Rmat - a matrix of beginning reserves that is the same dimensionality as the state space     %
%          xmat - a matrix of core earnings that is the same dimensionality as the state space     %
%          Rvec - a vector of beginning reserves. Only used if I keep the audit rate fixed with old FIN 48 reporting
%          xvec - a vector of core earnings. Only used if I keep the audit rate fixed with old FIN 48 reporting
%          simDat - a matrix of simulated data where column 1 (2) is R (x) for each NxS observation    %
%          audMat - a matrix random numbers that set whether the firm is audited                        %
%          shocks - a matrix random profitability shocks unrelated to tax avoidance                        %
%          real_data - a structure of real data %
%          counter - a value to indicate which counterfactual is used
% Outputs: counter_effect - Table that reports the effect of the counterfactual                                                        
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% Compute lagged assets for each simulated observation. Used to compute
% dollar amounts
at = repelem( real_data.at_lag , d.S );

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% TASK: 	Compute "base" data
%                                                                                                       
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

base_data = simulate_data(d, params, Rmat, xmat, simDat, audMat, shocks);

% Split out columns into variables
preTax = base_data( : , 4 );
tau = base_data( : , 9 );
V = base_data( : , 10 );

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% TASK: 	Compute differences betwee the two data sets
%                                                                                                       
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if counter < 5 
    counter_data = simulate_data_counter(d, params_counter, Rmat, xmat, simDat, audMat, shocks, counter);
    
else 
    counter_data = simulate_data_tcja(d, params_counter, Rmat, xmat, simDat, audMat, shocks);
end
    
% Split out columns into variables
res_counter = counter_data( : , 3 );
preTax_counter = counter_data( : , 4 );
x_counter = counter_data( : , 8 );
tau_counter = counter_data( : , 9 );
V_counter = counter_data( : , 10 );


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% TASK: 	Compute counterfactual data
%                                                                                                       
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Differences in Tax Rates
counter_effect.tauLevel = mean(tau_counter);
counter_effect.tau_diff = mean(tau_counter) - mean(tau);


counter_effect.tauDollar = mean(tau_counter .* preTax_counter .* at / 100) - mean(tau .* preTax .* at / 100);

% Differences in pre-tax income
counter_effect.preTax = mean( (preTax_counter - preTax) ./ preTax);
counter_effect.ROA = (mean(preTax_counter) - mean(preTax)) / 100;
counter_effect.preTaxDollar = mean(preTax_counter.* at / 100) - mean(preTax.* at / 100);

% Differences in firm value
counter_effect.value = ( mean( V_counter ) - mean(V) ) /  mean(V);


% Compute the informativeness of FIN 48    
parm_counter.alpha = params_counter(1);
parm_counter.eta =  params_counter(2); 
parm_counter.p = params_counter(3);
parm_counter.theta = params_counter(4);
parm_counter.phi = params_counter(5);
parm_counter.beta = params_counter(6);

if counter < 5
    lambda_counter = x_counter.* underlying( parm_counter , tau_counter );
else 
    lambda_counter = x_counter.* underlying_tcja( parm_counter , tau_counter );
end

counter_effect.inform = mean( res_counter ./ lambda_counter ); 
    

end
