%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Program Name: 	diagnostic                                                                          %
% Date created: 	2018-09-14                                                                          %
% Author: 			Charles McClure                                                                     %
% Purpose: 			Simulates data for different parameters and  measures moment sensitivity            %
%------------------------------------------------------------------------------------------------------ %
% Notes: 			functions are defined in folder "functions". Should run the beginning of main file  %
%                   first                                                                               %
%--------------------------------------------------------------------------------------------------     %
% Last Modified: 	2018-09-14                                                                          %
% Modified by: 	Charles McClure                                                                         %
% Description: 	First working version                                                                   %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% TASK: 	Set simulation data   - See Main Code                                                                      %
%                                                                                                       %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% TASK: 	Split possible parameter values to see how moments vary                                     %
%                                                                                                       %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
alphaRange = linspace(alphalb, alphaub, d.numParamValues);
etaRange = linspace(etalb, etaub, d.numParamValues);
pRange = linspace(plb, pub, d.numParamValues);
thetaRange = linspace(thetalb, thetaub, d.numParamValues);
phiRange = linspace(philb, phiub, d.numParamValues);
betaRange = linspace(betalb, betaub, d.numParamValues);

%% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% TASK: 	Simulate data for each parameter value                                                      %
%                                                                                                       %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Set "temporary" parameter values that will change each iteration
paramsAlpha = params;
paramsEta = params;
paramsP = params;
paramsTheta = params;
paramsPhi = params;
paramsBeta = params;

% Initialize cell arrays
alphaOutput = cell(d.numParamValues, 1);
etaOutput = cell(d.numParamValues, 1);
pOutput = cell(d.numParamValues, 1);
thetaOutput = cell(d.numParamValues, 1);
phiOutput = cell(d.numParamValues, 1);
betaOutput = cell(d.numParamValues, 1);


% Loop through parameter value
for ii = 1: d.numParamValues
    
    % Alpha    
    paramsAlpha(1) = alphaRange(ii);
    simResult = simulate_data(d, paramsAlpha, Rmat, xmat, simDat, audMat, shocks);
    cellOutput{1} = paramsAlpha;
    cellOutput{2} = simResult;
    alphaOutput{ii} = cellOutput;
    
    % Eta1
    paramsEta(2) = etaRange(ii);
    simResult = simulate_data(d, paramsEta, Rmat, xmat, simDat, audMat, shocks);
    cellOutput{1} = paramsEta;
    cellOutput{2} = simResult;
    etaOutput{ii} = cellOutput;
    
    % P    
    paramsP(3) = pRange(ii);
    simResult = simulate_data(d, paramsP,  Rmat, xmat, simDat, audMat, shocks);
    cellOutput{1} = paramsP;
    cellOutput{2} = simResult;
    pOutput{ii} = cellOutput;

    % Theta   
    paramsTheta(4) = thetaRange(ii);
    simResult = simulate_data(d, paramsTheta,  Rmat, xmat, simDat, audMat, shocks);
    cellOutput{1} = paramsTheta;
    cellOutput{2} = simResult;
    thetaOutput{ii} = cellOutput;
    
    % Phi    
    paramsPhi(5) = phiRange(ii);
    simResult = simulate_data(d, paramsPhi,  Rmat, xmat, simDat, audMat, shocks);
    cellOutput{1} = paramsPhi;
    cellOutput{2} = simResult;
    phiOutput{ii} = cellOutput;
    
    % Beta    
    paramsBeta(5) = betaRange(ii);
    simResult = simulate_data(d, paramsBeta,  Rmat, xmat, simDat, audMat, shocks);
    cellOutput{1} = paramsBeta;
    cellOutput{2} = simResult;
    betaOutput{ii} = cellOutput;


    % Print periodic updates    
    if(~mod(ii, 5)==1)
        fprintf('%d iterations complete \n',ii);
    end
    
    
end

% Save datasets down
save('alphaOutput.mat','alphaOutput');
save('etaOutput.mat','etaOutput');
save('pOutput.mat','pOutput');
save('thetaOutput.mat','thetaOutput');
save('phiOutput.mat','phiOutput');
save('betaOutput.mat','betaOutput');

%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% TASK: 	Compute the moments                                                                         %
%                                                                                                       %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear alphaMoments etaMoments pMoments thetaMoments phiMoments betaMoments
for ii = 1:d.numParamValues
    
    % Extract data from each cell array
    alphaData = alphaOutput{ii};
    etaData = etaOutput{ii};
    pData = pOutput{ii};
    thetaData = thetaOutput{ii};
    phiData = phiOutput{ii};
    betaData = betaOutput{ii};
    
    
    
    % Compute Moments    
    alphaMoments(ii) = simulate_moments(d, alphaData);
    etaMoments(ii) = simulate_moments(d, etaData);
    pMoments(ii) = simulate_moments(d, pData);
    thetaMoments(ii) = simulate_moments(d, thetaData);
    phiMoments(ii) = simulate_moments(d, phiData);
    betaMoments(ii) = simulate_moments(d, betaData);
  
   
end


%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% TASK: 	Plot the moments for each moment. Report a super plot for each parameter                    %
%                                                                                                       %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Create title/filenames for each moment. Need to manually check that the
% names line up to the actual moments
names = ["res", "Current Reserve"; ...
            "R", "Total Reserve"; ...
          "preTax", "Pre-tax Income"; ...
          "lapse", "Lapse Amount"; ...
          "settle", "Settle Rate"; ...
          "settleAmt", "Settle Amount"; ...
          "lapse", "Lapse Rate"; ...
          "lapseAud", "Lapse Rate - Audited Firms"; ...
          "resPI", "Reserve to Pre-tax Income"; ...
          "PIRes", "Pre-tax Income to Reserve"; ...
          "PIR", "Pre-tax Income to Total Reserve"; ...
          "PIRRes", "Pre-tax Income to Total Reserve and Current"; ...
          "resR", "Reserve to Total Reserve"; ...
          "audit", "Audit Rate"; ...
          "inspectR", "Settle Rate x R"; ...
          "resVar", "Variance of Res"; ...
          "preTaxVar", "Variance of Pre-tax Income"; ...
          "logResVar", "Variance of Log Res"; ...
          "logPretaxVar", "Variance of Log PI"; ...
          "settleAmountAud", "Settle Amt - Audited Firms"; ...
          "settleRateAud", "Settle Rate - Audited Firms"; ...
          "lapseAmtNoAud", "Lapse Amount - Not Audited Firms"; ...
          "lapseNoAud", "Lapse Rate - Not Audited Firms"; ...
          "settleAudVar", "Variance of Settle Rate - Audited Firms"; ...
          "lapseNoAudVar", "Variance of Lapse Amount - Not Audited Firms";...
          "preTaxRVar", "Variance of PI to R";...
          "preTaxResVar", "Variance of PI to Res";...
          "resPreTaxVar", "Variance of Res to PI";...
          "resPreTaxCov", "Covariance of Res and PI";...
          "preTaxRResRCov", "Covariance of Res to R and PI to R";...
          "resRCov", "Covariance of Res and R";...
          "resSettleCov", "Covariance of Res and Settle Rate";...
          "resLapseCov", "Covariance of Res and Lapse Amount";...
          "RPreTaxCov", "Covariance of R and PI";...
          "RSettleCov", "Covariance of R and Settle Rate";...
          "RLapseCov", "Covariance of R and Lapse Amount";...
          "PreTaxSettleCov", "Covariance of PI and Settle Rate";...
          "PreTaxLapseCov", "Covariance of PI and Lapse Amount";...
          "resSettleAudCov", "Covariance of res and Settle Rate - Audited";...
          "RSettleAudCov", "Covariance of R and Settle Rate - Audited";...
          "piSettleAudCov", "Covariance of PI and Settle Rate - Audited";...
         "resSkew", "Skew of reserve";...
         "resRSkew", "Skew of reserve to R";...
          "resRVar", "Variance of Res to R"];

%   Additional moments that I can include if I use lagged rp    
 %           "serialIntercept", "Regression of rp and res - Intercept";...
%           "serialSlope", "Regression of rp and res - Slope";...
      
for jj =1:length(names(:,2))
    fname = names(jj,2);
    
    % Create output vectors for each parameter result that aligns with naming above     
    name = names(jj,1);
    for ii = 1:d.numParamValues
        alphaSensitivity(ii) = alphaMoments(ii).(name);
        etaSensitivity(ii) = etaMoments(ii).(name);
        pSensitivity(ii) = pMoments(ii).(name);
        thetaSensitivity(ii) = thetaMoments(ii).(name);
        phiSensitivity(ii) = phiMoments(ii).(name);
        betaSensitivity(ii) = betaMoments(ii).(name);
    end
    
    %Alpha     
    subplot(2, 3, 1)
    splineAlpha = spap2(d.sections, d.power, alphaRange, alphaSensitivity);    
    fnplt(splineAlpha,'b',2);
    hold on;
    plot(alphaRange, alphaSensitivity, '.')
    hold on;
    ytickformat('%.2f');
    title('\alpha','Fontname','Palatino','FontSize',10,'FontWeight','bold')
    hold off;

    %Eta 
    subplot(2, 3, 2)
    splineEta = spap2(d.sections, d.power, etaRange, etaSensitivity);
    fnplt(splineEta,'b',2);
    hold on;
    plot(etaRange, etaSensitivity ,'.'); hold on;
    ytickformat('%.3f');
    title('\eta','Fontname','Palatino','FontSize',10,'FontWeight','bold')
    hold off;

    %p 
    subplot(2, 3, 3)
    splineP = spap2(d.sections, d.power, pRange, pSensitivity);
    fnplt(splineP,'b',2);
    hold on;
    plot(pRange, pSensitivity ,'.'); hold on;
    ytickformat('%.2f');
    title('p','Fontname','Palatino','FontSize',10,'FontWeight','bold')
    hold off;

    %theta 
    subplot(2, 3, 4)
    splineTheta = spap2(d.sections, d.power, thetaRange, thetaSensitivity);
    fnplt(splineTheta,'b',2);
    hold on;
    plot(thetaRange, thetaSensitivity ,'.'); hold on;
    ytickformat('%.2f');
    title('\theta','Fontname','Palatino','FontSize',10,'FontWeight','bold')
    hold off;

    %phi 
    subplot(2, 3, 5)
    splinePhi = spap2(d.sections, d.power, phiRange, phiSensitivity);
    fnplt(splinePhi,'b',2);
    hold on;
    plot(phiRange, phiSensitivity ,'.'); hold on;
    ytickformat('%.2f');
    title('\phi','Fontname','Palatino','FontSize',10,'FontWeight','bold')
    hold off;

    
     %phi 
    subplot(2, 3, 6)
    splineBeta = spap2(d.sections, d.power, betaRange, betaSensitivity);
    fnplt(splineBeta,'b',2);
    hold on;
    plot(betaRange, betaSensitivity ,'.'); hold on;
    ytickformat('%.2f');
    title('\beta','Fontname','Palatino','FontSize',10,'FontWeight','bold')
    hold off;
    supertitle(fname);
    print(gcf, '-djpeg100', fname);

end