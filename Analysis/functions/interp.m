function V_new_mat = interp(Rmat, xmat, V, R1, x1)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Function Name: 	interp                                                                              %
% Date created: 	2018-09-05                                                                          %
% Author: 			Charles McClure                                                                     %
% Purpose: 			Linearly interpolates the value function                                            %
%------------------------------------------------------------------------------------------------------ %
% Inputs:  Rmat - the 2D matrix of R at time t                                                             %
%          xmat - the 2D matrix of x                                                        %
%          V - The value function at t-1 using R and rp                                                 %
%          R1 - R at time t+1                                                          %
%          x1 - x at time t+1                                                     %
% Outputs: V_new_mat - Interpolated value function                                                      %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


F = griddedInterpolant(Rmat, xmat, V);
V_new_mat = F(R1, x1);

end
