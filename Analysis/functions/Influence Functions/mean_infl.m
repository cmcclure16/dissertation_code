function [ infl ] = mean_infl(x)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Function Name: 	mean_infl                                                                           %
% Date created: 	2018-10-04                                                                          %
% Author: 			Charles McClure                                                                     %
% Purpose: 			Computes the influence function for an unconditional mean                           %
%------------------------------------------------------------------------------------------------------ %
% Inputs:  x - vector from which to compute the mean                                                    %
% Outputs: infl - influence function as (x - mean(x))                                                   %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

infl = x - mean(x);

end
