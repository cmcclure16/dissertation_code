function [ infl ] = influence(real_data)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Function Name: 	influence                                                                           %
% Date created: 	2018-09-05                                                                          %
% Author: 			Charles McClure                                                                     %
% Purpose: 			Computes all possible influence functions                                           %
%------------------------------------------------------------------------------------------------------ %
% Inputs:  real_data - table with real data                                                             %
% Outputs: infl - A table with all of the influence functions                                      %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Initialize output
infl = cell2table({});


% Create indicator variables for audit and unaudited observations
A = boolean(real_data.audit);
NA = boolean(abs( 1.0  - real_data.audit ));


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Unconditional means                                                     %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Settle Rate x R
infl.settleR = mean_infl( real_data.settleR );

% Audit Rate
infl.audit = mean_infl( real_data.audit );

% Reserve to Pre-tax Income
infl.resPi = mean_infl( real_data.resPi);

% Pre-tax Income to Reserve
infl.piRes = mean_infl( real_data.piRes);


% Pre-tax Income to Total Reserve
infl.piR = mean_infl( real_data.piR);


% Pre-tax Income to Total Reserve + Res
infl.piRRes = mean_infl( real_data.piRRes);


% Res to Total Reserve
infl.resR = mean_infl( real_data.resR);


% Lapse rate
infl.lapse = mean_infl( real_data.lapse);


% Lapse Amount
infl.lapseAmt = mean_infl( real_data.lapseAmt);


% Settle Rate
infl.settle = mean_infl( real_data.settle);


% Settle Amount
infl.settleAmt = mean_infl( real_data.settleAmt );


% Pre-tax Income
infl.pi = mean_infl( real_data.pi );


% Reserve
infl.res = mean_infl( real_data.res );

% Total Reserve
infl.R = mean_infl( real_data.R );

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Unconditional covariance - these will have an "Var/Cov" Suffix          %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Variance of Pre-tax Income
infl.piVar = cov_infl( real_data.pi, real_data.pi );

% Variance of Reserve
infl.resVar = cov_infl( real_data.res, real_data.res );

% Variance of log(pi)
infl.logPiVar = cov_infl( real_data.logPreTax , real_data.logPreTax );

% Variance of log(res)
infl.logResVar = cov_infl( real_data.logRes , real_data.logRes );

% Variance of Pi / R
infl.piRVar = cov_infl( real_data.piR , real_data.piR );


% Variance of lapseAmount
infl.lapseAmtVar = cov_infl( real_data.lapseAmt , real_data.lapseAmt );


% Variance of PI / Res
infl.piResVar = cov_infl( real_data.piRes , real_data.piRes );


% Variance of Res / PI
infl.resPiVar = cov_infl( real_data.resPi , real_data.resPi );


% Variance of Res / R
infl.resRVar = cov_infl( real_data.resR , real_data.resR );


% Cov of Res and PI
infl.resPiCov = cov_infl( real_data.res , real_data.pi );


% Cov of Res /R and PI / R
infl.resRPiRCov = cov_infl( real_data.resR , real_data.piR );


% Cov of Res and R
infl.resRCov = cov_infl( real_data.res , real_data.R );


% Cov of Res and Settle Rate
infl.resSettleCov = cov_infl( real_data.res , real_data.settle );


% Cov of Res and Lapse Amount
infl.resLapseAmtCov = cov_infl( real_data.res , real_data.lapseAmt );


% Cov of R and PI
infl.RPiCov = cov_infl( real_data.R , real_data.pi );


% Cov of R and Settle Rate
infl.RSettleCov = cov_infl( real_data.R , real_data.settle );


% Cov of R and Lapse Amount
infl.RLapseAmtCov = cov_infl( real_data.R , real_data.lapseAmt );


% Cov of PI and Settle Rate
infl.piSettleCov = cov_infl( real_data.pi , real_data.settle );


% Cov of PI and Lapse Amount
infl.piLapseAmtCov = cov_infl( real_data.pi , real_data.lapseAmt );



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Mean of subsets - these will have a "NA/A" suffix                       %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Settle Rate for audited firms
infl.settleA = mean_cond_infl(real_data.settle, A);

% Settle Amount for audited firms
infl.settleAmtA = mean_cond_infl(real_data.settleAmt, A);

% Lapse Rate for audited firms
infl.lapseA = mean_cond_infl(real_data.lapse, A);

% Lapse Amount for audited firms
infl.lapseAmtA = mean_cond_infl(real_data.lapseAmt, A);

% Lapse Amount for unaudited firms
infl.lapseAmtNA = mean_cond_infl(real_data.lapseAmt, NA);

% Lapse Rate for unaudited firms
infl.lapseNA = mean_cond_infl(real_data.lapse, NA);



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Covariance of subsets - these will have a "Var/Cov" and "NA/A" suffixes %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Variance of lapse amount for unaudited firms
infl.lapseVarNA= cov_cond_infl( real_data.lapseAmt , real_data.lapseAmt , NA);


% Variance of settle rate amount for unaudited firms
infl.settleVarA = cov_cond_infl( real_data.settle , real_data.settle , A);


% Cov of res and settle rate - Audited
infl.resSettleCovA = cov_cond_infl( real_data.res , real_data.settle, A );


% Cov of R and settle rate - Audited
infl.RSettleCovA = cov_cond_infl( real_data.R , real_data.settle, A );


% Cov of pi and settle rate - Audited
infl.piSettleCovA = cov_cond_infl( real_data.pi , real_data.settle, A );

end