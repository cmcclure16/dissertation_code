function [ output ] = underlying(parm, t)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Function Name: 	underlying
% Date created: 	2018-11-13                                                                          
% Author: 			Charles McClure                                                                     
% Purpose: 			Computes the underlying repayment. Note this is the same as 
%                       underlyingout but is only used in the counterfactual analysis for speed reasons 
%---------------------------------------------------------------------------------------------------------------------- 
% Inputs:  parm - dataset of parameters                                                               
%              t - tax rate. Can be a vector or a scalar
% Outputs: counter_effect - Table that reports the effect of the counterfactual                                                        
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


output = (1.0 - parm.phi .* (0.35 - t)) .* ( ( parm.alpha * 0.35+ t .* ( ( t / 0.35 ) .^ parm.alpha - 1 - parm.alpha ) ) ./ ( 1 + parm.alpha ) );

end