    
% Compute optimal policy
[t1, s1] = Counter_Policy(11 , 11 , d, parm, r, counter, real_data);
[t2, s2] = Counter_Policy(25 , 11 , d, parm, r, counter, real_data);

% Check that states are roughly the same magnitude
s1(1,1,5)
s2(1,1,11)

% Plot
plot1 = Plot_Counter( t1 , s1 , 5 , 6 , '-bo');
hold on;
plot2 = Plot_Counter( t2 , s2 , 11 , 6,  '-ro');
hold off;

