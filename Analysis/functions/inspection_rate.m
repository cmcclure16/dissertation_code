function [ inspect ] = inspection_rate(parm, R, res)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Function Name: 	inspection_rate                                                                     %
% Date created: 	2018-09-13                                                                          %
% Author: 			Charles McClure                                                                     %
% Purpose: 			Computes inspection rate                                                            %
%------------------------------------------------------------------------------------------------------ %
% Notes: 			Can either take 1 element or a matrix of values. t, V, R must the same dimension    %
%------------------------------------------------------------------------------------------------------ %
% Inputs:  parm - dataset with model parameters                                                         %
%          R - Total Reserve at risk after current period. Same dimensions as R                         %
%          res - Current period reserve. Same dimensions as R                                           %
% Outputs: inspect - Inspection rate                                                                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

inspection = ( parm.eta ).*(R + res); 
inspect =   (inspection) ./(1 + inspection);


end