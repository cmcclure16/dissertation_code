function [ infl ] = beta_infl(x, y)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Function Name: 	beta_infl                                                                           %
% Date created: 	2018-10-04                                                                          %
% Author: 			Charles McClure                                                                     %
% Purpose: 			Computes the influence function for a linear regression y = x*beta + u              %
%------------------------------------------------------------------------------------------------------ %
% Inputs:  x - vector of independent covariates in the regression                                       %
%          y - vector for the dependent variable                                                        %
% Outputs: infl - influence function which has dimensions N x k, where k = dim(x)                       %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


n = length(x);
beta = (x'*x)\x'*y;
uhat = y - x*beta;

%CHECK THE DIMENSIONS HERE
infl = (1/n)*(x'*x)\(x.*uhat);
end
