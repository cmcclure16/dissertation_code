function [ infl ] = mean_ratio_infl(x, y)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Function Name: 	mean_cond_infl                                                                      %
% Date created: 	2018-10-04                                                                          %
% Author: 			Charles McClure                                                                     %
% Purpose: 			Computes the influence function for a ratio of means (E(x) / E(y))                  %
%------------------------------------------------------------------------------------------------------ %
% Inputs:  x - vector of independent covariates in the regression                                       %
%          y - vector from which to compute the covariance                                              %
% Outputs: infl - influence function which has dimensions N x 1                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

mu_x = mean(x);
mu_y = mean(y);

infl =  (mu_y*(x - mu_x) - mu_x*(y - mu_y))./(mu_y^2);

end
