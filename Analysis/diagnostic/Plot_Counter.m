function [output] = Plot_Counter(t_update, Rpmat, elem1, elem2, color)

dims = size(t_update); 
policy = reshape(t_update( elem1 , elem2 , :), [dims(1) 1 1]);
state = reshape(Rpmat( elem1 , elem2 , :), [dims(1) 1 1]);

if exist('color')
    output = plot(state,policy,color,'MarkerIndices',1:5:length(state));
else
    output = plot(state,policy,'-b','MarkerIndices',1:5:length(state));
end


end

