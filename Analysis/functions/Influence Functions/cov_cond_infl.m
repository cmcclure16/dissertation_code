function [ infl ] = cov_cond_infl(x, y, dum)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Function Name: 	cov_cond_infl                                                                       %
% Date created: 	2018-10-04                                                                          %
% Author: 			Charles McClure                                                                     %
% Purpose: 			Computes the influence function for a conditional covariance Cov(x, y|dum ==1)      %
%------------------------------------------------------------------------------------------------------ %
% Inputs:  x - vector of independent covariates in the regression                                       %
%          y - vector from which to compute the covariance                                              %
%          dum - vector to indicate if observation is included in the covariance                        %
% Outputs: infl - influence function which has dimensions N x 1                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


mu_dum = mean(dum);
mu_x_y_dum = mean(x.*y.*dum);
mu_x_dum = mean(x.*dum);
mu_y_dum = mean(y.*dum);


infl = (mu_dum*(x.*y.*dum - mu_x_y_dum) - mu_x_y_dum*(dum - mu_dum))./(mu_dum^2) ...
        - mu_y_dum/mu_dum*(mu_dum*(x.*dum - mu_x_dum) - mu_x_dum*(dum - mu_dum))/(mu_dum^2) ...
        - mu_x_dum / mu_dum*(mu_dum*(y.*dum - mu_y_dum) - mu_y_dum*(dum - mu_dum))/(mu_dum^2);
end
