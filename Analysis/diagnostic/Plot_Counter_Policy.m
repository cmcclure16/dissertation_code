function  [policy ] = Counter_Policy(pts1 , pts2 , elem1 , elem2, d, parm, r, counter, real_data, color)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Function Name: 	Plot_Counter_Policy                                                                          %
% Date created: 	2018-11-16                                                                          %
% Author: 			Charles McClure                                                                     %
% Purpose: 			Plots the policy function in the third dimension
%------------------------------------------------------------------------------------------------------ %
% Notes: 			Can can either 1 element for a matrix of values. t, V, R must the same dimension    %
%------------------------------------------------------------------------------------------------------ %
% Inputs:  pts - number of points for R and Rp dimesnions
%             pts2 - number of points for x dimension
%             elem1 - which point in R to hold fixed
%             elem2 - which point in x to hold fixed
%             d - dataset with parameters not estimated                                                    %
%             parm - dataset with model parameters                                                         %
%          r - dataset of current reserves computed from the function rho. Also includes the gridded    % 
%              data interpolant                                                                         %
%          counter - a value to indicate which counterfactual is used  
%           real_data - Table of real data
% Outputs: V1 - Update value. Will be the same size as t, V, R, and rp                                  %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% TASK: 	Compute Grid Space
%                                                                                                       
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

d.pts = pts1; %Number of grid points in state space
d.pts2 = pts2; %Number of grid points in x dimesion


Rvec = zeros([1, d.pts]);
xvec = zeros([1, d.pts2]);

states( : , 1 ) = real_data.R;
states( : , 2 ) = real_data.pi;

Rvec(1) = quantile(states(:,1), 0)/10;

quants = linspace(0, 0.95, d.pts-1); %Split up the non-winsorized space into quantiles
for ii = 2:d.pts-1
    Rvec(ii) = quantile(states(:,1),quants(ii));
end
Rvec(d.pts) = max(states(:,1))*1.5;

% Set mins and max for R
d.minR = min(Rvec);
d.maxR = max(Rvec);


xvec = linspace(d.xmin, d.xmax, d.pts2);


Rpvec = Rvec;
[Rmat, xmat, Rpmat] = ndgrid(Rvec,xvec, Rpvec);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% TASK:  Compute optimal policy
%                                                                                                       
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
t = 0.325;
[ t_update, ~ ] = model_solve_counter_fixed(parm, d, t, Rmat, xmat, Rpmat, r, counter);



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% TASK:  Extract the optimal policy based on holding R and x elements fixed
%                                                                                                       
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
policy = reshape(t_update( elem1 , elem2 , :), [d.pts 1 1]);
state = reshape(Rpmat( elem1 , elem2 , :), [d.pts 1 1]);
if exist('color')
    output = plot(state,policy,color,'MarkerIndices',1:5:length(state));
else
    output = plot(state,policy,'-b','MarkerIndices',1:5:length(state));
end

end

