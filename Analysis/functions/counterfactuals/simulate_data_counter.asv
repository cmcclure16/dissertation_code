function [ output ] = simulate_data_counter(d, params, Rmat, xmat, simDat, audMat, shocks, counter)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Function Name: 	simulate_data_counter                                                                       
% Date created: 	2018-09-13                                                                          
% Author: 			Charles McClure                                                                    
% Purpose: 			Simulates data based on a set of parameters for counterfactuals
%------------------------------------------------------------------------------------------------------ 
% Inputs:  d - dataset with parameters not estimated                                                  
%          params - vector of parameters                                                                
%          Rmat - a matrix of beginning reserves that is the same dimensionality as the state space     
%          simDat - a matrix of simulated data where column 1 (2) is R (rp) for each NxS observation  
%          audMat - a matrix random numbers that set whether the firm is audited                        
%          shocks - a matrix random profitability shocks unrelated to tax avoidance                
%          counter - a value to indicate which counterfactual is used  
% Outputs: output - a matrix with three columns. 1. Tau, 2. R, 3. rp                                    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Create a structure with the vector of parameters
parm.alpha = params(1);
parm.eta =  params(2); 
parm.p = params(3);
parm.theta = params(4);
parm.phi = params(5);
parm.beta = params(6);

% Calculate the expected reserve
r.rhos = rho(d, parm);
r.rhos = fliplr(r.rhos);
r.F = griddedInterpolant(r.rhos(1,:),r.rhos(3,:));


% Calculate 
xnew = NaN(size(audMat));

for ii = 2:d.T
    
        if ii == 1       
            tempX = exp( parm.beta ) .* simDat( : , 2 ).^d.beta1 .* shocks( : , ii);        
        else
            tempX = exp( parm.beta ).* xnew( : , ii - 1).^d.beta1 .* shocks( : , ii);       
        end

        %     Define the vector of xnew. If the expectation of xt+1 will cause
        %     it to be out of the bounds of xmat, pull back in
        xnew( : , ii ) = max( min( tempX , d.xmax/d.z(d.nodes) ) , d.xmin/d.z(1) );
    
end

% Solve the model using model_solve
t = 0.325;
[t_optimal, ~] = model_solve_counter(parm, d, t, Rmat, xmat, r, counter);

% Determine audit outcomes
audits = audMat<parm.p;

% Set initial state variables to the simDat matrix
R = simDat( : , 1 );
x = simDat( : , 2 );

% Loop through the time periods to find the optimal action and state
% variables at time T

for tt = 1:d.T
    t0 = interp( Rmat , xmat, t_optimal , R , x );
    res = xnew( : , tt) .* r.F(t0);
    audit_risk  = (R + res);
    audit_rate = inspection_rate( parm, R, res );
    R = audit_risk.*(1 - audit_rate.*audits(:,tt));    
    R   = max(min(R, d.maxR),d.minR);
    x = xnew( : , tt);
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% TASK: 	Compute additional observables at time T                                                    %
%                                                                                                       %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Number of observations;
N = length (simDat)/d.S;

% Column to indicate simulation number;
sim = repmat(linspace (1,d.S,d.S)', [N 1]);

% Current period reserve
t0 = interp( Rmat , xmat, t_optimal , R , x );
if counter == 1 
    res = x .* underlying(parm , t0 );
    
else
    
    res = x .* r.F(t0);

end


% Pre-tax Income
preTax = x .* (1.0 - parm.phi * 0.35 + parm.phi .*t0);

% Determine audit outcomes using the last columns of outcomes but keep it
% the same across simulations
audits = repelem(audits(1:N,end),d.S);


% Insection Rates
audit_risk  = (R + res);
auditRate = audits.*inspection_rate( parm, audit_risk, res );

% Lapse Rates
lapse = parm.theta*(1.0 - auditRate);

% Create output. This matrix has the following columns:
% 1. simulation number
% 2. R
% 3. res
% 4. preTax
% 5. Audit binary outcome
% 6. inspection rate
% 7. lapse rate
% 8. x
% 9. tau
output = [sim R res preTax audits auditRate lapse x t0];

end

