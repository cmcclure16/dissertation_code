* Other Macros;
/****************************************************************
* This macro can be used with datastep processing to display	*
* a status bar while the datastep is being processed.		 	*
* set= the dataset (set statement)							 	*
*		if you plan to include options like keep= or drop=	 	*
*		then you'll need to use %bquote( ) and include the		*
*		entire set statement.									*
*																*
*	e.g. #1														*
*			data one;											*
*				%progressbar(set=two);							*
*				a=b+c;											*
*			run;												*
*	e.g. #2														*
*			data one;											*
*				%progressbar(set=%bquote(two (keep=b c)));		*
*				a=b+c;											*
*			run;												*
****************************************************************/

%macro progressbar(set=);
   RETAIN PROGRESS_N NEXT_N PROGRESS 0;
   WINDOW Status 
       IROW=9 ICOLUMN=14 ROWS=11 COLUMNS=54
       GROUP=STATIC
       #2 @8 'Datastep running...' PERSIST=YES
       #4 @6 '|' PERSIST=YES @52 '|'  PERSIST=YES
       #5 @6 "|    '    25%    '    50%    '    75%    '    |" PERSIST=YES
       GROUP=PROGRESS
       #4 @(6 + PROGRESS) '*' PERSIST=YES
       ;
   IF _N_ = 1 THEN DO;
      DISPLAY Status.STATIC NOINPUT;
      PROGRESS_N = ROUND((NOBS/51) MAX 1);
      NEXT_N = PROGRESS_N/2;
      END;
	set &set. nobs=NOBS;
   IF _N_ > NEXT_N THEN DO;
      PROGRESS + 1;
      NEXT_N + PROGRESS_N;
      DISPLAY Status.PROGRESS NOINPUT;
      END;
%mend progressbar;


*****************************************************************
*	Name:			Interactions Macro							*
*	Created By:		Kurt H. Gee (Stanford University)			*
*	Last Modified: 	30 March 2013								*
*																*
*	This macro creates interaction variables and creates 		*
*	a macro variable ('interactions' by default) 				*
*	that can be used to	reference all of the interaction 		*
*	variables. The macro prints the names of the interaction	*
*	variables to the SAS log when the macro finishes running.	*	
*																*
*	dset= dataset												*
*	varlist1= list of variables									*
*	varlist2= list of interacting variables						*
*	interactlist= macro name for list of interaction variables	*
*																*
*	e.g.														*
*	%interact(dset=data,varlist1=effect1 effect2,				*
*		varlist2=size bmratio,interactlist=ints1)				*
*																*
*****************************************************************;

%macro interact(dset=,varlist1=,varlist2=,interactlist=interactions);
%let interactindex1=1;
	%do %until ( %scan(&varlist1.,&interactindex1.,%STR( ))= );
		%let var1_&interactindex1.=%scan(&varlist1.,&interactindex1.,%STR( ));
		%let interactindex1=%eval(&interactindex1.+1);
	%end;
%let interactindex2=1;
	%do %until ( %scan(&varlist2.,&interactindex2.,%STR( ))= );
		%let var2_&interactindex2.=%scan(&varlist2.,&interactindex2.,%STR( ));
		%let interactindex2=%eval(&interactindex2.+1);
	%end;

data &dset.;
	set &dset.;
	%global &interactlist.;
	%let interactions= ;
	%do interactindex3=1 %to %eval(&interactindex1.-1);
		%do interactindex4=1 %to %eval(&interactindex2.-1);
			&&var1_&interactindex3..X&&var2_&interactindex4..=&&var1_&interactindex3..*&&var2_&interactindex4..;
			if &&var1_&interactindex3..^=. & &&var2_&interactindex4..^=. & &&var1_&interactindex3..X&&var2_&interactindex4..=. then &&var1_&interactindex3..X&&var2_&interactindex4..=0;
			%let interactions= &interactions. &&var1_&interactindex3..X&&var2_&interactindex4..;
		%end;
	%end;
run;
%put Macro variable "&interactlist." = &interactions.;
%let &interactlist.=&interactions;
%mend interact;

/**********************************
CORRPS
Pearson on upper diagonal, spearman
on lower diagonal
***********************************/

%macro corrps(data=_data_,vars=);
%let i=1;
%let j=1;
%do %until (%SCAN(&vars,&i,%STR( ))=);
    %let v&j=%SCAN(&vars,&i,%STR( ));
    %let j=%EVAL(&j+1);
	 %let i=%EVAL(&i+1);
%end;

%let i=1;
%let j=1;
%do %until (%SCAN(&vars,&i,%STR( ))=);
    %let w&j=P%SCAN(&vars,&i,%STR( ));
    %let j=%EVAL(&j+1);
	 %let i=%EVAL(&i+1);
%end;

%let nvars=%eval(&i-1);




proc corr data=&data pearson spearman;
    var &vars;
	ods output pearsoncorr=_xp spearmancorr=_xs;
    run;
data _xp;
   set _xp;
   N=_N_;
   run;
data _xs;
   set _xs;
   N=_N_;
   run;

    %macro loop;
    %do j=1 %to &nvars;
	   ,case when _xp.N <= &j then _xp.&&v&j else _xs.&&v&j end as &&v&j, case when _xp.N <= &j then _xp.&&w&j else _xs.&&w&j end as &&w&j format=pvalue6.4
    %end;
    %mend;
  

proc sql;
   create table _xps
   as select _xp.variable
             %loop
   from _xp,_xs
   where _xp.variable = _xs.variable;
   quit;

proc print data=_xps;
   run;


proc datasets nolist lib=work;
   delete _:;
	run;
	quit;

%mend;



/**********************************************************
This macro displays all duplicate observations in a dataset
based on specified unique keys.
Output dataset is named 'duplicates'.

dset = dataset
keys = unique ids or combination of ids

E.g. %dupl(dset=data,keys=gvkey datadate);

***********************************************************/

%macro dupl(dset=,keys=);

data _null_;
	keys="&keys.";
	call symput('sql',tranwrd(keys,' ',', '));
	call symput('ord',tranwrd(keys,' ',', b.'));
run;

%let num=%eval(%sysfunc(countc(&keys,' '))+1);
%let crit= ;
%let str= ;

%do a=1 %to &num;
	%let key&a.=%scan(&keys,%eval(&a.),' ');
	%if &a>1 %then %let str=%str( & );
	%let crit= &crit.&str.a.&&key&a..%str(=)b.&&key&a..;
%end;

data dl;
	set &dset (keep=&keys);
	occur=1;
run;

proc sql;
create table dlx as
select distinct &sql, sum(occur) as repeat
	from dl
	group by &sql
	having repeat>1;

create table duplicates as
select b.* from dlx as a inner join &dset as b
	on &crit.
	order by b.&ord.;
quit;

option nonotes;
proc datasets lib=work nolist;
	delete dl dlx;
run;
quit;
option notes;

%mend dupl;


/**********************************************************************************************/
/* FILENAME:        Winsorize_Truncate.sas                                                    */
/* ORIGINAL AUTHOR: Steve Stubben (Stanford University)                                       */
/* MODIFIED BY:     Ryan Ball (UNC-Chapel Hill)                                               */
/* DATE CREATED:    August 3, 2005                                                            */
/* LAST MODIFIED:   August 3, 2005                                                            */
/* MACRO NAME:      Winsorize_Truncate                                                        */
/* ARGUMENTS:       1) DSETIN: input dataset containing variables that will be win/trunc.     */
/*                  2) DSETOUT: output dataset (leave blank to overwrite DSETIN)              */
/*                  3) BYVAR: variable(s) used to form groups (leave blank for total sample)  */
/*                  4) VARS: variable(s) that will be winsorized/truncated                    */
/*                  5) TYPE: = W to winsorize and = T (or anything else) to truncate          */
/*                  6) PCTL = percentile points (in ascending order) to truncate/winsorize    */
/*                            values.  Default is 1st and 99th percentiles.                   */
/* DESCRIPTION:     This macro is capable of both truncating and winsorizing one or multiple  */
/*                  variables.  Truncated values are replaced with a missing observation      */
/*                  rather than deleting the observation.  This gives the user more control   */
/*                  over the resulting dataset.                                               */
/* EXAMPLE(S):      1) %Winsorize_Truncate(dsetin = mydata, dsetout = mydata2, byvar = year,  */
/*                          vars = assets earnings, type = W, pctl = 0 98)                    */
/*                      ==> Winsorizes by year at 98% and puts resulting dataset into mydata2 */
/**********************************************************************************************/

%macro Winsorize_Truncate(dsetin = , 
                          dsetout = , 
                          byvar = none, 
                          vars = , 
                          type = W, 
                          pctl = 1 99);

    %if &dsetout = %then %let dsetout = &dsetin;
    
    %let varL=;
    %let varH=;
    %let xn=1;

    %do %until (%scan(&vars,&xn)= );
        %let token = %scan(&vars,&xn);
        %let varL = &varL &token.L;
        %let varH = &varH &token.H;
        %let xn = %EVAL(&xn + 1);
    %end;

    %let xn = %eval(&xn-1);

    data xtemp;
        set &dsetin;

    %let dropvar = ;
    %if &byvar = none %then %do;
        data xtemp;
            set xtemp;
            xbyvar = 1;

        %let byvar = xbyvar;
        %let dropvar = xbyvar;
    %end;

    proc sort data = xtemp;
        by &byvar;

    /*compute percentage cutoff values*/
    proc univariate data = xtemp noprint;
        by &byvar;
        var &vars;
        output out = xtemp_pctl PCTLPTS = &pctl PCTLPRE = &vars PCTLNAME = L H;

    data &dsetout;
        merge xtemp xtemp_pctl; /*merge percentage cutoff values into main dataset*/
        by &byvar;
        array trimvars{&xn} &vars;
        array trimvarl{&xn} &varL;
        array trimvarh{&xn} &varH;

        do xi = 1 to dim(trimvars);
            /*winsorize variables*/
            %if &type = W %then %do;
                if trimvars{xi} ne . then do;
                    if (trimvars{xi} < trimvarl{xi}) then trimvars{xi} = trimvarl{xi};
                    if (trimvars{xi} > trimvarh{xi}) then trimvars{xi} = trimvarh{xi};
                end;
            %end;
            /*truncate variables*/
            %else %do;
                if trimvars{xi} ne . then do;
                    /*insert .T code if value is truncated*/
                    if (trimvars{xi} < trimvarl{xi}) then trimvars{xi} = .T;
                    if (trimvars{xi} > trimvarh{xi}) then trimvars{xi} = .T;
                end;
            %end;
        end;
        drop &varL &varH &dropvar xi;

    /*delete temporary datasets created during macro execution*/
    proc datasets library=work nolist;
        delete xtemp xtemp_pctl; quit; run;

%mend;

***************************************************************************************;
***************************************************************************************;
***************************************************************************************;
*CREATES FAMA-FRENCH 10 INDUSTRY CLASSIFICATIONS
***************************************************************************************;
***************************************************************************************;
***************************************************************************************;
%macro ff10(data=,newvarname=industry,sic=sic,out=&data);
 
data &out;
    set &data;
	if 100 <= &sic. <= 999 then &newvarname. = 1;
	else if 2000 <= &sic. <= 2399 then &newvarname. = 1;
	else if 2700 <= &sic. <= 2749 then &newvarname. = 1;
	else if 2770 <= &sic. <= 2799 then &newvarname. = 1;
	else if 3100 <= &sic. <= 3199 then &newvarname. = 1;
	else if 3940 <= &sic. <= 3989 then &newvarname. = 1;
	else if 2500 <= &sic. <= 2519 then &newvarname. = 2;
	else if 2590 <= &sic. <= 2599 then &newvarname. = 2;
	else if 3630 <= &sic. <= 3659 then &newvarname. = 2;
	else if 3710 <= &sic. <= 3711 then &newvarname. = 2;
	else if 3714 <= &sic. <= 3714 then &newvarname. = 2;
	else if 3716 <= &sic. <= 3716 then &newvarname. = 2;
	else if 3750 <= &sic. <= 3751 then &newvarname. = 2;
	else if 3792 <= &sic. <= 3792 then &newvarname. = 2;
	else if 3900 <= &sic. <= 3939 then &newvarname. = 2;
	else if 3990 <= &sic. <= 3999 then &newvarname. = 2;
	else if 2520 <= &sic. <= 2589 then &newvarname. = 3;
	else if 2600 <= &sic. <= 2699 then &newvarname. = 3;
	else if 2750 <= &sic. <= 2769 then &newvarname. = 3;
	else if 2800 <= &sic. <= 2829 then &newvarname. = 3;
	else if 2840 <= &sic. <= 2899 then &newvarname. = 3;
	else if 3000 <= &sic. <= 3099 then &newvarname. = 3;
	else if 3200 <= &sic. <= 3569 then &newvarname. = 3;
	else if 3580 <= &sic. <= 3621 then &newvarname. = 3;
	else if 3623 <= &sic. <= 3629 then &newvarname. = 3;
	else if 3700 <= &sic. <= 3709 then &newvarname. = 3;
	else if 3712 <= &sic. <= 3713 then &newvarname. = 3;
	else if 3715 <= &sic. <= 3715 then &newvarname. = 3;
	else if 3717 <= &sic. <= 3749 then &newvarname. = 3;
	else if 3752 <= &sic. <= 3791 then &newvarname. = 3;
	else if 3793 <= &sic. <= 3799 then &newvarname. = 3;
	else if 3860 <= &sic. <= 3899 then &newvarname. = 3;
	else if 1200 <= &sic. <= 1399 then &newvarname. = 4;
	else if 2900 <= &sic. <= 2999 then &newvarname. = 4;
	else if 3570 <= &sic. <= 3579 then &newvarname. = 5;
	else if 3622= &sic. then &newvarname. = 5;
	else if 3660 <= &sic. <= 3692 then &newvarname. = 5;
	else if 3694 <= &sic. <= 3699 then &newvarname. = 5;
	else if 3810 <= &sic. <= 3839 then &newvarname. = 5;
	else if 7370= &sic. then &newvarname. = 5;
	else if 7373= &sic. then &newvarname. = 5;
	else if 7374= &sic. then &newvarname. = 5;
	else if 7375= &sic. then &newvarname. = 5;
	else if 7376= &sic. then &newvarname. = 5;
	else if 7377= &sic. then &newvarname. = 5;
	else if 7378= &sic. then &newvarname. = 5;
	else if 7379= &sic. then &newvarname. = 5;
	else if 7391= &sic. then &newvarname. = 5;
	else if 8730= &sic. then &newvarname. = 5;
	else if 4800 <= &sic. <= 4899 then &newvarname. = 6;
	else if 5000 <= &sic. <= 5999 then &newvarname. = 7;
	else if 7200 <= &sic. <= 7299 then &newvarname. = 7;
	else if 7600 <= &sic. <= 7699 then &newvarname. = 7;
	else if 2830 <= &sic. <= 2839 then &newvarname. = 8;
	else if 3693 <= &sic. <= 3693 then &newvarname. = 8;
	else if 3840 <= &sic. <= 3859 then &newvarname. = 8;
	else if 8000 <= &sic. <= 8099 then &newvarname. = 8;
	else if 4900 <= &sic. <= 4949 then &newvarname. = 9;
	else &newvarname = 10;
run;
 
proc format;
   value ff
1    ="NoDur"
2    ="Durbl"
3    ="Manuf"
4    ="Enrgy"
5    ="HiTec"
6    ="Telcm"
7    ="Shops"
8    ="Hlth"
9    ="Utils"
10    ="Other";
run;
 
%mend ff10;



***************************************************************************************;
***************************************************************************************;
***************************************************************************************;
*CREATES FAMA-FRENCH 17 INDUSTRY CLASSIFICATIONS
***************************************************************************************;
***************************************************************************************;
***************************************************************************************;
%macro ff17(data=,newvarname=industry,sic=sic,out=&data);
 
data &out;
    set &data;
    &newvarname = 17;
if    100   <=    &sic  <=    199   then  &newvarname =     17    ;
else  if    200   <=    &sic  <=    299   then  &newvarname =     1     ;
else  if    700   <=    &sic  <=    799   then  &newvarname =     1     ;
else  if    900   <=    &sic  <=    999   then  &newvarname =     1     ;
else  if    2000  <=    &sic  <=    2009  then  &newvarname =     1     ;
else  if    2010  <=    &sic  <=    2019  then  &newvarname =     1     ;
else  if    2020  <=    &sic  <=    2029  then  &newvarname =     1     ;
else  if    2030  <=    &sic  <=    2039  then  &newvarname =     1     ;
else  if    2040  <=    &sic  <=    2046  then  &newvarname =     1     ;
else  if    2047  <=    &sic  <=    2047  then  &newvarname =     1     ;
else  if    2048  <=    &sic  <=    2048  then  &newvarname =     1     ;
else  if    2050  <=    &sic  <=    2059  then  &newvarname =     1     ;
else  if    2060  <=    &sic  <=    2063  then  &newvarname =     1     ;
else  if    2064  <=    &sic  <=    2068  then  &newvarname =     1     ;
else  if    2070  <=    &sic  <=    2079  then  &newvarname =     1     ;
else  if    2080  <=    &sic  <=    2080  then  &newvarname =     1     ;
else  if    2082  <=    &sic  <=    2082  then  &newvarname =     1     ;
else  if    2083  <=    &sic  <=    2083  then  &newvarname =     1     ;
else  if    2084  <=    &sic  <=    2084  then  &newvarname =     1     ;
else  if    2085  <=    &sic  <=    2085  then  &newvarname =     1     ;
else  if    2086  <=    &sic  <=    2086  then  &newvarname =     1     ;
else  if    2087  <=    &sic  <=    2087  then  &newvarname =     1     ;
else  if    2090  <=    &sic  <=    2092  then  &newvarname =     1     ;
else  if    2095  <=    &sic  <=    2095  then  &newvarname =     1     ;
else  if    2096  <=    &sic  <=    2096  then  &newvarname =     1     ;
else  if    2097  <=    &sic  <=    2097  then  &newvarname =     1     ;
else  if    2098  <=    &sic  <=    2099  then  &newvarname =     1     ;
else  if    5140  <=    &sic  <=    5149  then  &newvarname =     1     ;
else  if    5150  <=    &sic  <=    5159  then  &newvarname =     1     ;
else  if    5180  <=    &sic  <=    5182  then  &newvarname =     1     ;
else  if    5191  <=    &sic  <=    5191  then  &newvarname =     1     ;
else  if    1000  <=    &sic  <=    1009  then  &newvarname =     2     ;
else  if    1010  <=    &sic  <=    1019  then  &newvarname =     2     ;
else  if    1020  <=    &sic  <=    1029  then  &newvarname =     2     ;
else  if    1030  <=    &sic  <=    1039  then  &newvarname =     2     ;
else  if    1040  <=    &sic  <=    1049  then  &newvarname =     2     ;
else  if    1060  <=    &sic  <=    1069  then  &newvarname =     2     ;
else  if    1080  <=    &sic  <=    1089  then  &newvarname =     2     ;
else  if    1090  <=    &sic  <=    1099  then  &newvarname =     2     ;
else  if    1200  <=    &sic  <=    1299  then  &newvarname =     2     ;
else  if    1400  <=    &sic  <=    1499  then  &newvarname =     2     ;
else  if    5050  <=    &sic  <=    5052  then  &newvarname =     2     ;
else  if    1300  <=    &sic  <=    1300  then  &newvarname =     3     ;
else  if    1310  <=    &sic  <=    1319  then  &newvarname =     3     ;
else  if    1320  <=    &sic  <=    1329  then  &newvarname =     3     ;
else  if    1380  <=    &sic  <=    1380  then  &newvarname =     3     ;
else  if    1381  <=    &sic  <=    1381  then  &newvarname =     3     ;
else  if    1382  <=    &sic  <=    1382  then  &newvarname =     3     ;
else  if    1389  <=    &sic  <=    1389  then  &newvarname =     3     ;
else  if    2900  <=    &sic  <=    2912  then  &newvarname =     3     ;
else  if    5170  <=    &sic  <=    5172  then  &newvarname =     3     ;
else  if    2200  <=    &sic  <=    2269  then  &newvarname =     4     ;
else  if    2270  <=    &sic  <=    2279  then  &newvarname =     4     ;
else  if    2280  <=    &sic  <=    2284  then  &newvarname =     4     ;
else  if    2290  <=    &sic  <=    2295  then  &newvarname =     4     ;
else  if    2296  <=    &sic  <=    2296  then  &newvarname =     4     ;
else  if    2297  <=    &sic  <=    2297  then  &newvarname =     4     ;
else  if    2298  <=    &sic  <=    2298  then  &newvarname =     4     ;
else  if    2299  <=    &sic  <=    2299  then  &newvarname =     4     ;
else  if    2300  <=    &sic  <=    2390  then  &newvarname =     4     ;
else  if    2391  <=    &sic  <=    2392  then  &newvarname =     4     ;
else  if    2393  <=    &sic  <=    2395  then  &newvarname =     4     ;
else  if    2396  <=    &sic  <=    2396  then  &newvarname =     4     ;
else  if    2397  <=    &sic  <=    2399  then  &newvarname =     4     ;
else  if    3020  <=    &sic  <=    3021  then  &newvarname =     4     ;
else  if    3100  <=    &sic  <=    3111  then  &newvarname =     4     ;
else  if    3130  <=    &sic  <=    3131  then  &newvarname =     4     ;
else  if    3140  <=    &sic  <=    3149  then  &newvarname =     4     ;
else  if    3150  <=    &sic  <=    3151  then  &newvarname =     4     ;
else  if    3963  <=    &sic  <=    3965  then  &newvarname =     4     ;
else  if    5130  <=    &sic  <=    5139  then  &newvarname =     4     ;
else  if    2510  <=    &sic  <=    2519  then  &newvarname =     5     ;
else  if    2590  <=    &sic  <=    2599  then  &newvarname =     5     ;
else  if    3060  <=    &sic  <=    3069  then  &newvarname =     5     ;
else  if    3070  <=    &sic  <=    3079  then  &newvarname =     5     ;
else  if    3080  <=    &sic  <=    3089  then  &newvarname =     5     ;
else  if    3090  <=    &sic  <=    3099  then  &newvarname =     5     ;
else  if    3630  <=    &sic  <=    3639  then  &newvarname =     5     ;
else  if    3650  <=    &sic  <=    3651  then  &newvarname =     5     ;
else  if    3652  <=    &sic  <=    3652  then  &newvarname =     5     ;
else  if    3860  <=    &sic  <=    3861  then  &newvarname =     5     ;
else  if    3870  <=    &sic  <=    3873  then  &newvarname =     5     ;
else  if    3910  <=    &sic  <=    3911  then  &newvarname =     5     ;
else  if    3914  <=    &sic  <=    3914  then  &newvarname =     5     ;
else  if    3915  <=    &sic  <=    3915  then  &newvarname =     5     ;
else  if    3930  <=    &sic  <=    3931  then  &newvarname =     5     ;
else  if    3940  <=    &sic  <=    3949  then  &newvarname =     5     ;
else  if    3960  <=    &sic  <=    3962  then  &newvarname =     5     ;
else  if    5020  <=    &sic  <=    5023  then  &newvarname =     5     ;
else  if    5064  <=    &sic  <=    5064  then  &newvarname =     5     ;
else  if    5094  <=    &sic  <=    5094  then  &newvarname =     5     ;
else  if    5099  <=    &sic  <=    5099  then  &newvarname =     5     ;
else  if    2800  <=    &sic  <=    2809  then  &newvarname =     6     ;
else  if    2810  <=    &sic  <=    2819  then  &newvarname =     6     ;
else  if    2820  <=    &sic  <=    2829  then  &newvarname =     6     ;
else  if    2860  <=    &sic  <=    2869  then  &newvarname =     6     ;
else  if    2870  <=    &sic  <=    2879  then  &newvarname =     6     ;
else  if    2890  <=    &sic  <=    2899  then  &newvarname =     6     ;
else  if    5160  <=    &sic  <=    5169  then  &newvarname =     6     ;
else  if    2100  <=    &sic  <=    2199  then  &newvarname =     7     ;
else  if    2830  <=    &sic  <=    2830  then  &newvarname =     7     ;
else  if    2831  <=    &sic  <=    2831  then  &newvarname =     7     ;
else  if    2833  <=    &sic  <=    2833  then  &newvarname =     7     ;
else  if    2834  <=    &sic  <=    2834  then  &newvarname =     7     ;
else  if    2840  <=    &sic  <=    2843  then  &newvarname =     7     ;
else  if    2844  <=    &sic  <=    2844  then  &newvarname =     7     ;
else  if    5120  <=    &sic  <=    5122  then  &newvarname =     7     ;
else  if    5194  <=    &sic  <=    5194  then  &newvarname =     7     ;
else  if    800   <=    &sic  <=    899   then  &newvarname =     8     ;
else  if    1500  <=    &sic  <=    1511  then  &newvarname =     8     ;
else  if    1520  <=    &sic  <=    1529  then  &newvarname =     8     ;
else  if    1530  <=    &sic  <=    1539  then  &newvarname =     8     ;
else  if    1540  <=    &sic  <=    1549  then  &newvarname =     8     ;
else  if    1600  <=    &sic  <=    1699  then  &newvarname =     8     ;
else  if    1700  <=    &sic  <=    1799  then  &newvarname =     8     ;
else  if    2400  <=    &sic  <=    2439  then  &newvarname =     8     ;
else  if    2440  <=    &sic  <=    2449  then  &newvarname =     8     ;
else  if    2450  <=    &sic  <=    2459  then  &newvarname =     8     ;
else  if    2490  <=    &sic  <=    2499  then  &newvarname =     8     ;
else  if    2850  <=    &sic  <=    2859  then  &newvarname =     8     ;
else  if    2950  <=    &sic  <=    2952  then  &newvarname =     8     ;
else  if    3200  <=    &sic  <=    3200  then  &newvarname =     8     ;
else  if    3210  <=    &sic  <=    3211  then  &newvarname =     8     ;
else  if    3240  <=    &sic  <=    3241  then  &newvarname =     8     ;
else  if    3250  <=    &sic  <=    3259  then  &newvarname =     8     ;
else  if    3261  <=    &sic  <=    3261  then  &newvarname =     8     ;
else  if    3264  <=    &sic  <=    3264  then  &newvarname =     8     ;
else  if    3270  <=    &sic  <=    3275  then  &newvarname =     8     ;
else  if    3280  <=    &sic  <=    3281  then  &newvarname =     8     ;
else  if    3290  <=    &sic  <=    3293  then  &newvarname =     8     ;
else  if    3420  <=    &sic  <=    3429  then  &newvarname =     8     ;
else  if    3430  <=    &sic  <=    3433  then  &newvarname =     8     ;
else  if    3440  <=    &sic  <=    3441  then  &newvarname =     8     ;
else  if    3442  <=    &sic  <=    3442  then  &newvarname =     8     ;
else  if    3446  <=    &sic  <=    3446  then  &newvarname =     8     ;
else  if    3448  <=    &sic  <=    3448  then  &newvarname =     8     ;
else  if    3449  <=    &sic  <=    3449  then  &newvarname =     8     ;
else  if    3450  <=    &sic  <=    3451  then  &newvarname =     8     ;
else  if    3452  <=    &sic  <=    3452  then  &newvarname =     8     ;
else  if    5030  <=    &sic  <=    5039  then  &newvarname =     8     ;
else  if    5070  <=    &sic  <=    5078  then  &newvarname =     8     ;
else  if    5198  <=    &sic  <=    5198  then  &newvarname =     8     ;
else  if    5210  <=    &sic  <=    5211  then  &newvarname =     8     ;
else  if    5230  <=    &sic  <=    5231  then  &newvarname =     8     ;
else  if    5250  <=    &sic  <=    5251  then  &newvarname =     8     ;
else  if    3300  <=    &sic  <=    3300  then  &newvarname =     9     ;
else  if    3310  <=    &sic  <=    3317  then  &newvarname =     9     ;
else  if    3320  <=    &sic  <=    3325  then  &newvarname =     9     ;
else  if    3330  <=    &sic  <=    3339  then  &newvarname =     9     ;
else  if    3340  <=    &sic  <=    3341  then  &newvarname =     9     ;
else  if    3350  <=    &sic  <=    3357  then  &newvarname =     9     ;
else  if    3360  <=    &sic  <=    3369  then  &newvarname =     9     ;
else  if    3390  <=    &sic  <=    3399  then  &newvarname =     9     ;
else  if    3410  <=    &sic  <=    3412  then  &newvarname =     10    ;
else  if    3443  <=    &sic  <=    3443  then  &newvarname =     10    ;
else  if    3444  <=    &sic  <=    3444  then  &newvarname =     10    ;
else  if    3460  <=    &sic  <=    3469  then  &newvarname =     10    ;
else  if    3470  <=    &sic  <=    3479  then  &newvarname =     10    ;
else  if    3480  <=    &sic  <=    3489  then  &newvarname =     10    ;
else  if    3490  <=    &sic  <=    3499  then  &newvarname =     10    ;
else  if    3510  <=    &sic  <=    3519  then  &newvarname =     11    ;
else  if    3520  <=    &sic  <=    3529  then  &newvarname =     11    ;
else  if    3530  <=    &sic  <=    3530  then  &newvarname =     11    ;
else  if    3531  <=    &sic  <=    3531  then  &newvarname =     11    ;
else  if    3532  <=    &sic  <=    3532  then  &newvarname =     11    ;
else  if    3533  <=    &sic  <=    3533  then  &newvarname =     11    ;
else  if    3534  <=    &sic  <=    3534  then  &newvarname =     11    ;
else  if    3535  <=    &sic  <=    3535  then  &newvarname =     11    ;
else  if    3536  <=    &sic  <=    3536  then  &newvarname =     11    ;
else  if    3540  <=    &sic  <=    3549  then  &newvarname =     11    ;
else  if    3550  <=    &sic  <=    3559  then  &newvarname =     11    ;
else  if    3560  <=    &sic  <=    3569  then  &newvarname =     11    ;
else  if    3570  <=    &sic  <=    3579  then  &newvarname =     11    ;
else  if    3580  <=    &sic  <=    3580  then  &newvarname =     11    ;
else  if    3581  <=    &sic  <=    3581  then  &newvarname =     11    ;
else  if    3582  <=    &sic  <=    3582  then  &newvarname =     11    ;
else  if    3585  <=    &sic  <=    3585  then  &newvarname =     11    ;
else  if    3586  <=    &sic  <=    3586  then  &newvarname =     11    ;
else  if    3589  <=    &sic  <=    3589  then  &newvarname =     11    ;
else  if    3590  <=    &sic  <=    3599  then  &newvarname =     11    ;
else  if    3600  <=    &sic  <=    3600  then  &newvarname =     11    ;
else  if    3610  <=    &sic  <=    3613  then  &newvarname =     11    ;
else  if    3620  <=    &sic  <=    3621  then  &newvarname =     11    ;
else  if    3622  <=    &sic  <=    3622  then  &newvarname =     11    ;
else  if    3623  <=    &sic  <=    3629  then  &newvarname =     11    ;
else  if    3670  <=    &sic  <=    3679  then  &newvarname =     11    ;
else  if    3680  <=    &sic  <=    3680  then  &newvarname =     11    ;
else  if    3681  <=    &sic  <=    3681  then  &newvarname =     11    ;
else  if    3682  <=    &sic  <=    3682  then  &newvarname =     11    ;
else  if    3683  <=    &sic  <=    3683  then  &newvarname =     11    ;
else  if    3684  <=    &sic  <=    3684  then  &newvarname =     11    ;
else  if    3685  <=    &sic  <=    3685  then  &newvarname =     11    ;
else  if    3686  <=    &sic  <=    3686  then  &newvarname =     11    ;
else  if    3687  <=    &sic  <=    3687  then  &newvarname =     11    ;
else  if    3688  <=    &sic  <=    3688  then  &newvarname =     11    ;
else  if    3689  <=    &sic  <=    3689  then  &newvarname =     11    ;
else  if    3690  <=    &sic  <=    3690  then  &newvarname =     11    ;
else  if    3691  <=    &sic  <=    3692  then  &newvarname =     11    ;
else  if    3693  <=    &sic  <=    3693  then  &newvarname =     11    ;
else  if    3694  <=    &sic  <=    3694  then  &newvarname =     11    ;
else  if    3695  <=    &sic  <=    3695  then  &newvarname =     11    ;
else  if    3699  <=    &sic  <=    3699  then  &newvarname =     11    ;
else  if    3810  <=    &sic  <=    3810  then  &newvarname =     11    ;
else  if    3811  <=    &sic  <=    3811  then  &newvarname =     11    ;
else  if    3812  <=    &sic  <=    3812  then  &newvarname =     11    ;
else  if    3820  <=    &sic  <=    3820  then  &newvarname =     11    ;
else  if    3821  <=    &sic  <=    3821  then  &newvarname =     11    ;
else  if    3822  <=    &sic  <=    3822  then  &newvarname =     11    ;
else  if    3823  <=    &sic  <=    3823  then  &newvarname =     11    ;
else  if    3824  <=    &sic  <=    3824  then  &newvarname =     11    ;
else  if    3825  <=    &sic  <=    3825  then  &newvarname =     11    ;
else  if    3826  <=    &sic  <=    3826  then  &newvarname =     11    ;
else  if    3827  <=    &sic  <=    3827  then  &newvarname =     11    ;
else  if    3829  <=    &sic  <=    3829  then  &newvarname =     11    ;
else  if    3830  <=    &sic  <=    3839  then  &newvarname =     11    ;
else  if    3950  <=    &sic  <=    3955  then  &newvarname =     11    ;
else  if    5060  <=    &sic  <=    5060  then  &newvarname =     11    ;
else  if    5063  <=    &sic  <=    5063  then  &newvarname =     11    ;
else  if    5065  <=    &sic  <=    5065  then  &newvarname =     11    ;
else  if    5080  <=    &sic  <=    5080  then  &newvarname =     11    ;
else  if    5081  <=    &sic  <=    5081  then  &newvarname =     11    ;
else  if    3710  <=    &sic  <=    3710  then  &newvarname =     12    ;
else  if    3711  <=    &sic  <=    3711  then  &newvarname =     12    ;
else  if    3714  <=    &sic  <=    3714  then  &newvarname =     12    ;
else  if    3716  <=    &sic  <=    3716  then  &newvarname =     12    ;
else  if    3750  <=    &sic  <=    3751  then  &newvarname =     12    ;
else  if    3792  <=    &sic  <=    3792  then  &newvarname =     12    ;
else  if    5010  <=    &sic  <=    5015  then  &newvarname =     12    ;
else  if    5510  <=    &sic  <=    5521  then  &newvarname =     12    ;
else  if    5530  <=    &sic  <=    5531  then  &newvarname =     12    ;
else  if    5560  <=    &sic  <=    5561  then  &newvarname =     12    ;
else  if    5570  <=    &sic  <=    5571  then  &newvarname =     12    ;
else  if    5590  <=    &sic  <=    5599  then  &newvarname =     12    ;
else  if    3713  <=    &sic  <=    3713  then  &newvarname =     13    ;
else  if    3715  <=    &sic  <=    3715  then  &newvarname =     13    ;
else  if    3720  <=    &sic  <=    3720  then  &newvarname =     13    ;
else  if    3721  <=    &sic  <=    3721  then  &newvarname =     13    ;
else  if    3724  <=    &sic  <=    3724  then  &newvarname =     13    ;
else  if    3725  <=    &sic  <=    3725  then  &newvarname =     13    ;
else  if    3728  <=    &sic  <=    3728  then  &newvarname =     13    ;
else  if    3730  <=    &sic  <=    3731  then  &newvarname =     13    ;
else  if    3732  <=    &sic  <=    3732  then  &newvarname =     13    ;
else  if    3740  <=    &sic  <=    3743  then  &newvarname =     13    ;
else  if    3760  <=    &sic  <=    3769  then  &newvarname =     13    ;
else  if    3790  <=    &sic  <=    3790  then  &newvarname =     13    ;
else  if    3795  <=    &sic  <=    3795  then  &newvarname =     13    ;
else  if    3799  <=    &sic  <=    3799  then  &newvarname =     13    ;
else  if    4000  <=    &sic  <=    4013  then  &newvarname =     13    ;
else  if    4100  <=    &sic  <=    4100  then  &newvarname =     13    ;
else  if    4110  <=    &sic  <=    4119  then  &newvarname =     13    ;
else  if    4120  <=    &sic  <=    4121  then  &newvarname =     13    ;
else  if    4130  <=    &sic  <=    4131  then  &newvarname =     13    ;
else  if    4140  <=    &sic  <=    4142  then  &newvarname =     13    ;
else  if    4150  <=    &sic  <=    4151  then  &newvarname =     13    ;
else  if    4170  <=    &sic  <=    4173  then  &newvarname =     13    ;
else  if    4190  <=    &sic  <=    4199  then  &newvarname =     13    ;
else  if    4200  <=    &sic  <=    4200  then  &newvarname =     13    ;
else  if    4210  <=    &sic  <=    4219  then  &newvarname =     13    ;
else  if    4220  <=    &sic  <=    4229  then  &newvarname =     13    ;
else  if    4230  <=    &sic  <=    4231  then  &newvarname =     13    ;
else  if    4400  <=    &sic  <=    4499  then  &newvarname =     13    ;
else  if    4500  <=    &sic  <=    4599  then  &newvarname =     13    ;
else  if    4600  <=    &sic  <=    4699  then  &newvarname =     13    ;
else  if    4700  <=    &sic  <=    4700  then  &newvarname =     13    ;
else  if    4710  <=    &sic  <=    4712  then  &newvarname =     13    ;
else  if    4720  <=    &sic  <=    4729  then  &newvarname =     13    ;
else  if    4730  <=    &sic  <=    4739  then  &newvarname =     13    ;
else  if    4740  <=    &sic  <=    4742  then  &newvarname =     13    ;
else  if    4780  <=    &sic  <=    4780  then  &newvarname =     13    ;
else  if    4783  <=    &sic  <=    4783  then  &newvarname =     13    ;
else  if    4785  <=    &sic  <=    4785  then  &newvarname =     13    ;
else  if    4789  <=    &sic  <=    4789  then  &newvarname =     13    ;
else  if    4900  <=    &sic  <=    4900  then  &newvarname =     14    ;
else  if    4910  <=    &sic  <=    4911  then  &newvarname =     14    ;
else  if    4920  <=    &sic  <=    4922  then  &newvarname =     14    ;
else  if    4923  <=    &sic  <=    4923  then  &newvarname =     14    ;
else  if    4924  <=    &sic  <=    4925  then  &newvarname =     14    ;
else  if    4930  <=    &sic  <=    4931  then  &newvarname =     14    ;
else  if    4932  <=    &sic  <=    4932  then  &newvarname =     14    ;
else  if    4939  <=    &sic  <=    4939  then  &newvarname =     14    ;
else  if    4940  <=    &sic  <=    4942  then  &newvarname =     14    ;
else  if    5260  <=    &sic  <=    5261  then  &newvarname =     15    ;
else  if    5270  <=    &sic  <=    5271  then  &newvarname =     15    ;
else  if    5300  <=    &sic  <=    5300  then  &newvarname =     15    ;
else  if    5310  <=    &sic  <=    5311  then  &newvarname =     15    ;
else  if    5320  <=    &sic  <=    5320  then  &newvarname =     15    ;
else  if    5330  <=    &sic  <=    5331  then  &newvarname =     15    ;
else  if    5334  <=    &sic  <=    5334  then  &newvarname =     15    ;
else  if    5390  <=    &sic  <=    5399  then  &newvarname =     15    ;
else  if    5400  <=    &sic  <=    5400  then  &newvarname =     15    ;
else  if    5410  <=    &sic  <=    5411  then  &newvarname =     15    ;
else  if    5412  <=    &sic  <=    5412  then  &newvarname =     15    ;
else  if    5420  <=    &sic  <=    5421  then  &newvarname =     15    ;
else  if    5430  <=    &sic  <=    5431  then  &newvarname =     15    ;
else  if    5440  <=    &sic  <=    5441  then  &newvarname =     15    ;
else  if    5450  <=    &sic  <=    5451  then  &newvarname =     15    ;
else  if    5460  <=    &sic  <=    5461  then  &newvarname =     15    ;
else  if    5490  <=    &sic  <=    5499  then  &newvarname =     15    ;
else  if    5540  <=    &sic  <=    5541  then  &newvarname =     15    ;
else  if    5550  <=    &sic  <=    5551  then  &newvarname =     15    ;
else  if    5600  <=    &sic  <=    5699  then  &newvarname =     15    ;
else  if    5700  <=    &sic  <=    5700  then  &newvarname =     15    ;
else  if    5710  <=    &sic  <=    5719  then  &newvarname =     15    ;
else  if    5720  <=    &sic  <=    5722  then  &newvarname =     15    ;
else  if    5730  <=    &sic  <=    5733  then  &newvarname =     15    ;
else  if    5734  <=    &sic  <=    5734  then  &newvarname =     15    ;
else  if    5735  <=    &sic  <=    5735  then  &newvarname =     15    ;
else  if    5736  <=    &sic  <=    5736  then  &newvarname =     15    ;
else  if    5750  <=    &sic  <=    5750  then  &newvarname =     15    ;
else  if    5800  <=    &sic  <=    5813  then  &newvarname =     15    ;
else  if    5890  <=    &sic  <=    5890  then  &newvarname =     15    ;
else  if    5900  <=    &sic  <=    5900  then  &newvarname =     15    ;
else  if    5910  <=    &sic  <=    5912  then  &newvarname =     15    ;
else  if    5920  <=    &sic  <=    5921  then  &newvarname =     15    ;
else  if    5930  <=    &sic  <=    5932  then  &newvarname =     15    ;
else  if    5940  <=    &sic  <=    5940  then  &newvarname =     15    ;
else  if    5941  <=    &sic  <=    5941  then  &newvarname =     15    ;
else  if    5942  <=    &sic  <=    5942  then  &newvarname =     15    ;
else  if    5943  <=    &sic  <=    5943  then  &newvarname =     15    ;
else  if    5944  <=    &sic  <=    5944  then  &newvarname =     15    ;
else  if    5945  <=    &sic  <=    5945  then  &newvarname =     15    ;
else  if    5946  <=    &sic  <=    5946  then  &newvarname =     15    ;
else  if    5947  <=    &sic  <=    5947  then  &newvarname =     15    ;
else  if    5948  <=    &sic  <=    5948  then  &newvarname =     15    ;
else  if    5949  <=    &sic  <=    5949  then  &newvarname =     15    ;
else  if    5960  <=    &sic  <=    5963  then  &newvarname =     15    ;
else  if    5980  <=    &sic  <=    5989  then  &newvarname =     15    ;
else  if    5990  <=    &sic  <=    5990  then  &newvarname =     15    ;
else  if    5992  <=    &sic  <=    5992  then  &newvarname =     15    ;
else  if    5993  <=    &sic  <=    5993  then  &newvarname =     15    ;
else  if    5994  <=    &sic  <=    5994  then  &newvarname =     15    ;
else  if    5995  <=    &sic  <=    5995  then  &newvarname =     15    ;
else  if    5999  <=    &sic  <=    5999  then  &newvarname =     15    ;
else  if    6010  <=    &sic  <=    6019  then  &newvarname =     16    ;
else  if    6020  <=    &sic  <=    6020  then  &newvarname =     16    ;
else  if    6021  <=    &sic  <=    6021  then  &newvarname =     16    ;
else  if    6022  <=    &sic  <=    6022  then  &newvarname =     16    ;
else  if    6023  <=    &sic  <=    6023  then  &newvarname =     16    ;
else  if    6025  <=    &sic  <=    6025  then  &newvarname =     16    ;
else  if    6026  <=    &sic  <=    6026  then  &newvarname =     16    ;
else  if    6028  <=    &sic  <=    6029  then  &newvarname =     16    ;
else  if    6030  <=    &sic  <=    6036  then  &newvarname =     16    ;
else  if    6040  <=    &sic  <=    6049  then  &newvarname =     16    ;
else  if    6050  <=    &sic  <=    6059  then  &newvarname =     16    ;
else  if    6060  <=    &sic  <=    6062  then  &newvarname =     16    ;
else  if    6080  <=    &sic  <=    6082  then  &newvarname =     16    ;
else  if    6090  <=    &sic  <=    6099  then  &newvarname =     16    ;
else  if    6100  <=    &sic  <=    6100  then  &newvarname =     16    ;
else  if    6110  <=    &sic  <=    6111  then  &newvarname =     16    ;
else  if    6112  <=    &sic  <=    6112  then  &newvarname =     16    ;
else  if    6120  <=    &sic  <=    6129  then  &newvarname =     16    ;
else  if    6140  <=    &sic  <=    6149  then  &newvarname =     16    ;
else  if    6150  <=    &sic  <=    6159  then  &newvarname =     16    ;
else  if    6160  <=    &sic  <=    6163  then  &newvarname =     16    ;
else  if    6172  <=    &sic  <=    6172  then  &newvarname =     16    ;
else  if    6199  <=    &sic  <=    6199  then  &newvarname =     16    ;
else  if    6200  <=    &sic  <=    6299  then  &newvarname =     16    ;
else  if    6300  <=    &sic  <=    6300  then  &newvarname =     16    ;
else  if    6310  <=    &sic  <=    6312  then  &newvarname =     16    ;
else  if    6320  <=    &sic  <=    6324  then  &newvarname =     16    ;
else  if    6330  <=    &sic  <=    6331  then  &newvarname =     16    ;
else  if    6350  <=    &sic  <=    6351  then  &newvarname =     16    ;
else  if    6360  <=    &sic  <=    6361  then  &newvarname =     16    ;
else  if    6370  <=    &sic  <=    6371  then  &newvarname =     16    ;
else  if    6390  <=    &sic  <=    6399  then  &newvarname =     16    ;
else  if    6400  <=    &sic  <=    6411  then  &newvarname =     16    ;
else  if    6500  <=    &sic  <=    6500  then  &newvarname =     16    ;
else  if    6510  <=    &sic  <=    6510  then  &newvarname =     16    ;
else  if    6512  <=    &sic  <=    6512  then  &newvarname =     16    ;
else  if    6513  <=    &sic  <=    6513  then  &newvarname =     16    ;
else  if    6514  <=    &sic  <=    6514  then  &newvarname =     16    ;
else  if    6515  <=    &sic  <=    6515  then  &newvarname =     16    ;
else  if    6517  <=    &sic  <=    6519  then  &newvarname =     16    ;
else  if    6530  <=    &sic  <=    6531  then  &newvarname =     16    ;
else  if    6532  <=    &sic  <=    6532  then  &newvarname =     16    ;
else  if    6540  <=    &sic  <=    6541  then  &newvarname =     16    ;
else  if    6550  <=    &sic  <=    6553  then  &newvarname =     16    ;
else  if    6611  <=    &sic  <=    6611  then  &newvarname =     16    ;
else  if    6700  <=    &sic  <=    6700  then  &newvarname =     16    ;
else  if    6710  <=    &sic  <=    6719  then  &newvarname =     16    ;
else  if    6720  <=    &sic  <=    6722  then  &newvarname =     16    ;
else  if    6723  <=    &sic  <=    6723  then  &newvarname =     16    ;
else  if    6724  <=    &sic  <=    6724  then  &newvarname =     16    ;
else  if    6725  <=    &sic  <=    6725  then  &newvarname =     16    ;
else  if    6726  <=    &sic  <=    6726  then  &newvarname =     16    ;
else  if    6730  <=    &sic  <=    6733  then  &newvarname =     16    ;
else  if    6790  <=    &sic  <=    6790  then  &newvarname =     16    ;
else  if    6792  <=    &sic  <=    6792  then  &newvarname =     16    ;
else  if    6794  <=    &sic  <=    6794  then  &newvarname =     16    ;
else  if    6795  <=    &sic  <=    6795  then  &newvarname =     16    ;
else  if    6798  <=    &sic  <=    6798  then  &newvarname =     16    ;
else  if    6799  <=    &sic  <=    6799  then  &newvarname =     16    ;
else  if    2520  <=    &sic  <=    2549  then  &newvarname =     17    ;
else  if    2600  <=    &sic  <=    2639  then  &newvarname =     17    ;
else  if    2640  <=    &sic  <=    2659  then  &newvarname =     17    ;
else  if    2661  <=    &sic  <=    2661  then  &newvarname =     17    ;
else  if    2670  <=    &sic  <=    2699  then  &newvarname =     17    ;
else  if    2700  <=    &sic  <=    2709  then  &newvarname =     17    ;
else  if    2710  <=    &sic  <=    2719  then  &newvarname =     17    ;
else  if    2720  <=    &sic  <=    2729  then  &newvarname =     17    ;
else  if    2730  <=    &sic  <=    2739  then  &newvarname =     17    ;
else  if    2740  <=    &sic  <=    2749  then  &newvarname =     17    ;
else  if    2750  <=    &sic  <=    2759  then  &newvarname =     17    ;
else  if    2760  <=    &sic  <=    2761  then  &newvarname =     17    ;
else  if    2770  <=    &sic  <=    2771  then  &newvarname =     17    ;
else  if    2780  <=    &sic  <=    2789  then  &newvarname =     17    ;
else  if    2790  <=    &sic  <=    2799  then  &newvarname =     17    ;
else  if    2835  <=    &sic  <=    2835  then  &newvarname =     17    ;
else  if    2836  <=    &sic  <=    2836  then  &newvarname =     17    ;
else  if    2990  <=    &sic  <=    2999  then  &newvarname =     17    ;
else  if    3000  <=    &sic  <=    3000  then  &newvarname =     17    ;
else  if    3010  <=    &sic  <=    3011  then  &newvarname =     17    ;
else  if    3041  <=    &sic  <=    3041  then  &newvarname =     17    ;
else  if    3050  <=    &sic  <=    3053  then  &newvarname =     17    ;
else  if    3160  <=    &sic  <=    3161  then  &newvarname =     17    ;
else  if    3170  <=    &sic  <=    3171  then  &newvarname =     17    ;
else  if    3172  <=    &sic  <=    3172  then  &newvarname =     17    ;
else  if    3190  <=    &sic  <=    3199  then  &newvarname =     17    ;
else  if    3220  <=    &sic  <=    3221  then  &newvarname =     17    ;
else  if    3229  <=    &sic  <=    3229  then  &newvarname =     17    ;
else  if    3230  <=    &sic  <=    3231  then  &newvarname =     17    ;
else  if    3260  <=    &sic  <=    3260  then  &newvarname =     17    ;
else  if    3262  <=    &sic  <=    3263  then  &newvarname =     17    ;
else  if    3269  <=    &sic  <=    3269  then  &newvarname =     17    ;
else  if    3295  <=    &sic  <=    3299  then  &newvarname =     17    ;
else  if    3537  <=    &sic  <=    3537  then  &newvarname =     17    ;
else  if    3640  <=    &sic  <=    3644  then  &newvarname =     17    ;
else  if    3645  <=    &sic  <=    3645  then  &newvarname =     17    ;
else  if    3646  <=    &sic  <=    3646  then  &newvarname =     17    ;
else  if    3647  <=    &sic  <=    3647  then  &newvarname =     17    ;
else  if    3648  <=    &sic  <=    3649  then  &newvarname =     17    ;
else  if    3660  <=    &sic  <=    3660  then  &newvarname =     17    ;
else  if    3661  <=    &sic  <=    3661  then  &newvarname =     17    ;
else  if    3662  <=    &sic  <=    3662  then  &newvarname =     17    ;
else  if    3663  <=    &sic  <=    3663  then  &newvarname =     17    ;
else  if    3664  <=    &sic  <=    3664  then  &newvarname =     17    ;
else  if    3665  <=    &sic  <=    3665  then  &newvarname =     17    ;
else  if    3666  <=    &sic  <=    3666  then  &newvarname =     17    ;
else  if    3669  <=    &sic  <=    3669  then  &newvarname =     17    ;
else  if    3840  <=    &sic  <=    3849  then  &newvarname =     17    ;
else  if    3850  <=    &sic  <=    3851  then  &newvarname =     17    ;
else  if    3991  <=    &sic  <=    3991  then  &newvarname =     17    ;
else  if    3993  <=    &sic  <=    3993  then  &newvarname =     17    ;
else  if    3995  <=    &sic  <=    3995  then  &newvarname =     17    ;
else  if    3996  <=    &sic  <=    3996  then  &newvarname =     17    ;
else  if    4810  <=    &sic  <=    4813  then  &newvarname =     17    ;
else  if    4820  <=    &sic  <=    4822  then  &newvarname =     17    ;
else  if    4830  <=    &sic  <=    4839  then  &newvarname =     17    ;
else  if    4840  <=    &sic  <=    4841  then  &newvarname =     17    ;
else  if    4890  <=    &sic  <=    4890  then  &newvarname =     17    ;
else  if    4891  <=    &sic  <=    4891  then  &newvarname =     17    ;
else  if    4892  <=    &sic  <=    4892  then  &newvarname =     17    ;
else  if    4899  <=    &sic  <=    4899  then  &newvarname =     17    ;
else  if    4950  <=    &sic  <=    4959  then  &newvarname =     17    ;
else  if    4960  <=    &sic  <=    4961  then  &newvarname =     17    ;
else  if    4970  <=    &sic  <=    4971  then  &newvarname =     17    ;
else  if    4991  <=    &sic  <=    4991  then  &newvarname =     17    ;
else  if    5040  <=    &sic  <=    5042  then  &newvarname =     17    ;
else  if    5043  <=    &sic  <=    5043  then  &newvarname =     17    ;
else  if    5044  <=    &sic  <=    5044  then  &newvarname =     17    ;
else  if    5045  <=    &sic  <=    5045  then  &newvarname =     17    ;
else  if    5046  <=    &sic  <=    5046  then  &newvarname =     17    ;
else  if    5047  <=    &sic  <=    5047  then  &newvarname =     17    ;
else  if    5048  <=    &sic  <=    5048  then  &newvarname =     17    ;
else  if    5049  <=    &sic  <=    5049  then  &newvarname =     17    ;
else  if    5082  <=    &sic  <=    5082  then  &newvarname =     17    ;
else  if    5083  <=    &sic  <=    5083  then  &newvarname =     17    ;
else  if    5084  <=    &sic  <=    5084  then  &newvarname =     17    ;
else  if    5085  <=    &sic  <=    5085  then  &newvarname =     17    ;
else  if    5086  <=    &sic  <=    5087  then  &newvarname =     17    ;
else  if    5088  <=    &sic  <=    5088  then  &newvarname =     17    ;
else  if    5090  <=    &sic  <=    5090  then  &newvarname =     17    ;
else  if    5091  <=    &sic  <=    5092  then  &newvarname =     17    ;
else  if    5093  <=    &sic  <=    5093  then  &newvarname =     17    ;
else  if    5100  <=    &sic  <=    5100  then  &newvarname =     17    ;
else  if    5110  <=    &sic  <=    5113  then  &newvarname =     17    ;
else  if    5199  <=    &sic  <=    5199  then  &newvarname =     17    ;
else  if    7000  <=    &sic  <=    7000  then  &newvarname =     17    ;
else  if    7010  <=    &sic  <=    7011  then  &newvarname =     17    ;
else  if    7020  <=    &sic  <=    7021  then  &newvarname =     17    ;
else  if    7030  <=    &sic  <=    7033  then  &newvarname =     17    ;
else  if    7040  <=    &sic  <=    7041  then  &newvarname =     17    ;
else  if    7200  <=    &sic  <=    7200  then  &newvarname =     17    ;
else  if    7210  <=    &sic  <=    7212  then  &newvarname =     17    ;
else  if    7213  <=    &sic  <=    7213  then  &newvarname =     17    ;
else  if    7215  <=    &sic  <=    7216  then  &newvarname =     17    ;
else  if    7217  <=    &sic  <=    7217  then  &newvarname =     17    ;
else  if    7218  <=    &sic  <=    7218  then  &newvarname =     17    ;
else  if    7219  <=    &sic  <=    7219  then  &newvarname =     17    ;
else  if    7220  <=    &sic  <=    7221  then  &newvarname =     17    ;
else  if    7230  <=    &sic  <=    7231  then  &newvarname =     17    ;
else  if    7240  <=    &sic  <=    7241  then  &newvarname =     17    ;
else  if    7250  <=    &sic  <=    7251  then  &newvarname =     17    ;
else  if    7260  <=    &sic  <=    7269  then  &newvarname =     17    ;
else  if    7290  <=    &sic  <=    7290  then  &newvarname =     17    ;
else  if    7291  <=    &sic  <=    7291  then  &newvarname =     17    ;
else  if    7299  <=    &sic  <=    7299  then  &newvarname =     17    ;
else  if    7300  <=    &sic  <=    7300  then  &newvarname =     17    ;
else  if    7310  <=    &sic  <=    7319  then  &newvarname =     17    ;
else  if    7320  <=    &sic  <=    7323  then  &newvarname =     17    ;
else  if    7330  <=    &sic  <=    7338  then  &newvarname =     17    ;
else  if    7340  <=    &sic  <=    7342  then  &newvarname =     17    ;
else  if    7349  <=    &sic  <=    7349  then  &newvarname =     17    ;
else  if    7350  <=    &sic  <=    7351  then  &newvarname =     17    ;
else  if    7352  <=    &sic  <=    7352  then  &newvarname =     17    ;
else  if    7353  <=    &sic  <=    7353  then  &newvarname =     17    ;
else  if    7359  <=    &sic  <=    7359  then  &newvarname =     17    ;
else  if    7360  <=    &sic  <=    7369  then  &newvarname =     17    ;
else  if    7370  <=    &sic  <=    7372  then  &newvarname =     17    ;
else  if    7373  <=    &sic  <=    7373  then  &newvarname =     17    ;
else  if    7374  <=    &sic  <=    7374  then  &newvarname =     17    ;
else  if    7375  <=    &sic  <=    7375  then  &newvarname =     17    ;
else  if    7376  <=    &sic  <=    7376  then  &newvarname =     17    ;
else  if    7377  <=    &sic  <=    7377  then  &newvarname =     17    ;
else  if    7378  <=    &sic  <=    7378  then  &newvarname =     17    ;
else  if    7379  <=    &sic  <=    7379  then  &newvarname =     17    ;
else  if    7380  <=    &sic  <=    7380  then  &newvarname =     17    ;
else  if    7381  <=    &sic  <=    7382  then  &newvarname =     17    ;
else  if    7383  <=    &sic  <=    7383  then  &newvarname =     17    ;
else  if    7384  <=    &sic  <=    7384  then  &newvarname =     17    ;
else  if    7385  <=    &sic  <=    7385  then  &newvarname =     17    ;
else  if    7389  <=    &sic  <=    7390  then  &newvarname =     17    ;
else  if    7391  <=    &sic  <=    7391  then  &newvarname =     17    ;
else  if    7392  <=    &sic  <=    7392  then  &newvarname =     17    ;
else  if    7393  <=    &sic  <=    7393  then  &newvarname =     17    ;
else  if    7394  <=    &sic  <=    7394  then  &newvarname =     17    ;
else  if    7395  <=    &sic  <=    7395  then  &newvarname =     17    ;
else  if    7397  <=    &sic  <=    7397  then  &newvarname =     17    ;
else  if    7399  <=    &sic  <=    7399  then  &newvarname =     17    ;
else  if    7500  <=    &sic  <=    7500  then  &newvarname =     17    ;
else  if    7510  <=    &sic  <=    7519  then  &newvarname =     17    ;
else  if    7520  <=    &sic  <=    7523  then  &newvarname =     17    ;
else  if    7530  <=    &sic  <=    7539  then  &newvarname =     17    ;
else  if    7540  <=    &sic  <=    7549  then  &newvarname =     17    ;
else  if    7600  <=    &sic  <=    7600  then  &newvarname =     17    ;
else  if    7620  <=    &sic  <=    7620  then  &newvarname =     17    ;
else  if    7622  <=    &sic  <=    7622  then  &newvarname =     17    ;
else  if    7623  <=    &sic  <=    7623  then  &newvarname =     17    ;
else  if    7629  <=    &sic  <=    7629  then  &newvarname =     17    ;
else  if    7630  <=    &sic  <=    7631  then  &newvarname =     17    ;
else  if    7640  <=    &sic  <=    7641  then  &newvarname =     17    ;
else  if    7690  <=    &sic  <=    7699  then  &newvarname =     17    ;
else  if    7800  <=    &sic  <=    7829  then  &newvarname =     17    ;
else  if    7830  <=    &sic  <=    7833  then  &newvarname =     17    ;
else  if    7840  <=    &sic  <=    7841  then  &newvarname =     17    ;
else  if    7900  <=    &sic  <=    7900  then  &newvarname =     17    ;
else  if    7910  <=    &sic  <=    7911  then  &newvarname =     17    ;
else  if    7920  <=    &sic  <=    7929  then  &newvarname =     17    ;
else  if    7930  <=    &sic  <=    7933  then  &newvarname =     17    ;
else  if    7940  <=    &sic  <=    7949  then  &newvarname =     17    ;
else  if    7980  <=    &sic  <=    7980  then  &newvarname =     17    ;
else  if    7990  <=    &sic  <=    7999  then  &newvarname =     17    ;
else  if    8000  <=    &sic  <=    8099  then  &newvarname =     17    ;
else  if    8100  <=    &sic  <=    8199  then  &newvarname =     17    ;
else  if    8200  <=    &sic  <=    8299  then  &newvarname =     17    ;
else  if    8300  <=    &sic  <=    8399  then  &newvarname =     17    ;
else  if    8400  <=    &sic  <=    8499  then  &newvarname =     17    ;
else  if    8600  <=    &sic  <=    8699  then  &newvarname =     17    ;
else  if    8700  <=    &sic  <=    8700  then  &newvarname =     17    ;
else  if    8710  <=    &sic  <=    8713  then  &newvarname =     17    ;
else  if    8720  <=    &sic  <=    8721  then  &newvarname =     17    ;
else  if    8730  <=    &sic  <=    8734  then  &newvarname =     17    ;
else  if    8740  <=    &sic  <=    8748  then  &newvarname =     17    ;
else  if    8800  <=    &sic  <=    8899  then  &newvarname =     17    ;
else  if    8900  <=    &sic  <=    8910  then  &newvarname =     17    ;
else  if    8911  <=    &sic  <=    8911  then  &newvarname =     17    ;
else  if    8920  <=    &sic  <=    8999  then  &newvarname =     17    ;
else &newvarname = 17;
run;
 
proc format;
   value ff
      1     ="Food"
2    ="Mining and Minerals"
3    ="Oil and Petro Products"
4    ="Textiles, Apparel & Footware"
5    ="Consumer Durables"
6    ="Chemicals"
7    ="Drugs, Soap, Prfums, Tobacco"
8    ="Construction"
9    ="Steel"
10    ="Fabricated Products"
11    ="Machinery and Business Equipment"
12    ="Automobiles"
13    ="Transporation"
14    ="Utilities"
15    ="Retail Stores"
16    ="Financial Institutions"
17    ="Other";
run;
 
%mend;
 
***************************************************************************************;
***************************************************************************************;
***************************************************************************************;
*CREATES FAMA-FRENCH 30 INDUSTRY CLASSIFICATIONS
***************************************************************************************;
***************************************************************************************;
***************************************************************************************;
%macro ff30(data=,newvarname=industry,sic=sic,out=&data);
 
data &out;
    set &data;
    &newvarname = 30;
if    100   <= &sic <=  199   then &newvarname =      1     ;
else if     200   <= &sic <=  299   then &newvarname =      1     ;
else if     700   <= &sic <=  799   then &newvarname =      1     ;
else if     910   <= &sic <=  919   then &newvarname =      1     ;
else if     2000  <= &sic <=  2009  then &newvarname =      1     ;
else if     2010  <= &sic <=  2019  then &newvarname =      1     ;
else if     2020  <= &sic <=  2029  then &newvarname =      1     ;
else if     2030  <= &sic <=  2039  then &newvarname =      1     ;
else if     2040  <= &sic <=  2046  then &newvarname =      1     ;
else if     2048  <= &sic <=  2048  then &newvarname =      1     ;
else if     2050  <= &sic <=  2059  then &newvarname =      1     ;
else if     2060  <= &sic <=  2063  then &newvarname =      1     ;
else if     2064  <= &sic <=  2068  then &newvarname =      1     ;
else if     2070  <= &sic <=  2079  then &newvarname =      1     ;
else if     2086  <= &sic <=  2086  then &newvarname =      1     ;
else if     2087  <= &sic <=  2087  then &newvarname =      1     ;
else if     2090  <= &sic <=  2092  then &newvarname =      1     ;
else if     2095  <= &sic <=  2095  then &newvarname =      1     ;
else if     2096  <= &sic <=  2096  then &newvarname =      1     ;
else if     2097  <= &sic <=  2097  then &newvarname =      1     ;
else if     2098  <= &sic <=  2099  then &newvarname =      1     ;
else if     2080  <= &sic <=  2080  then &newvarname =      2     ;
else if     2082  <= &sic <=  2082  then &newvarname =      2     ;
else if     2083  <= &sic <=  2083  then &newvarname =      2     ;
else if     2084  <= &sic <=  2084  then &newvarname =      2     ;
else if     2085  <= &sic <=  2085  then &newvarname =      2     ;
else if     2100  <= &sic <=  2199  then &newvarname =      3     ;
else if     920   <= &sic <=  999   then &newvarname =      4     ;
else if     3650  <= &sic <=  3651  then &newvarname =      4     ;
else if     3652  <= &sic <=  3652  then &newvarname =      4     ;
else if     3732  <= &sic <=  3732  then &newvarname =      4     ;
else if     3930  <= &sic <=  3931  then &newvarname =      4     ;
else if     3940  <= &sic <=  3949  then &newvarname =      4     ;
else if     7800  <= &sic <=  7829  then &newvarname =      4     ;
else if     7830  <= &sic <=  7833  then &newvarname =      4     ;
else if     7840  <= &sic <=  7841  then &newvarname =      4     ;
else if     7900  <= &sic <=  7900  then &newvarname =      4     ;
else if     7910  <= &sic <=  7911  then &newvarname =      4     ;
else if     7920  <= &sic <=  7929  then &newvarname =      4     ;
else if     7930  <= &sic <=  7933  then &newvarname =      4     ;
else if     7940  <= &sic <=  7949  then &newvarname =      4     ;
else if     7980  <= &sic <=  7980  then &newvarname =      4     ;
else if     7990  <= &sic <=  7999  then &newvarname =      4     ;
else if     2700  <= &sic <=  2709  then &newvarname =      5     ;
else if     2710  <= &sic <=  2719  then &newvarname =      5     ;
else if     2720  <= &sic <=  2729  then &newvarname =      5     ;
else if     2730  <= &sic <=  2739  then &newvarname =      5     ;
else if     2740  <= &sic <=  2749  then &newvarname =      5     ;
else if     2750  <= &sic <=  2759  then &newvarname =      5     ;
else if     2770  <= &sic <=  2771  then &newvarname =      5     ;
else if     2780  <= &sic <=  2789  then &newvarname =      5     ;
else if     2790  <= &sic <=  2799  then &newvarname =      5     ;
else if     3993  <= &sic <=  3993  then &newvarname =      5     ;
else if     2047  <= &sic <=  2047  then &newvarname =      6     ;
else if     2391  <= &sic <=  2392  then &newvarname =      6     ;
else if     2510  <= &sic <=  2519  then &newvarname =      6     ;
else if     2590  <= &sic <=  2599  then &newvarname =      6     ;
else if     2840  <= &sic <=  2843  then &newvarname =      6     ;
else if     2844  <= &sic <=  2844  then &newvarname =      6     ;
else if     3160  <= &sic <=  3161  then &newvarname =      6     ;
else if     3170  <= &sic <=  3171  then &newvarname =      6     ;
else if     3172  <= &sic <=  3172  then &newvarname =      6     ;
else if     3190  <= &sic <=  3199  then &newvarname =      6     ;
else if     3229  <= &sic <=  3229  then &newvarname =      6     ;
else if     3260  <= &sic <=  3260  then &newvarname =      6     ;
else if     3262  <= &sic <=  3263  then &newvarname =      6     ;
else if     3269  <= &sic <=  3269  then &newvarname =      6     ;
else if     3230  <= &sic <=  3231  then &newvarname =      6     ;
else if     3630  <= &sic <=  3639  then &newvarname =      6     ;
else if     3750  <= &sic <=  3751  then &newvarname =      6     ;
else if     3800  <= &sic <=  3800  then &newvarname =      6     ;
else if     3860  <= &sic <=  3861  then &newvarname =      6     ;
else if     3870  <= &sic <=  3873  then &newvarname =      6     ;
else if     3910  <= &sic <=  3911  then &newvarname =      6     ;
else if     3914  <= &sic <=  3914  then &newvarname =      6     ;
else if     3915  <= &sic <=  3915  then &newvarname =      6     ;
else if     3960  <= &sic <=  3962  then &newvarname =      6     ;
else if     3991  <= &sic <=  3991  then &newvarname =      6     ;
else if     3995  <= &sic <=  3995  then &newvarname =      6     ;
else if     2300  <= &sic <=  2390  then &newvarname =      7     ;
else if     3020  <= &sic <=  3021  then &newvarname =      7     ;
else if     3100  <= &sic <=  3111  then &newvarname =      7     ;
else if     3130  <= &sic <=  3131  then &newvarname =      7     ;
else if     3140  <= &sic <=  3149  then &newvarname =      7     ;
else if     3150  <= &sic <=  3151  then &newvarname =      7     ;
else if     3963  <= &sic <=  3965  then &newvarname =      7     ;
else if     2830  <= &sic <=  2830  then &newvarname =      8     ;
else if     2831  <= &sic <=  2831  then &newvarname =      8     ;
else if     2833  <= &sic <=  2833  then &newvarname =      8     ;
else if     2834  <= &sic <=  2834  then &newvarname =      8     ;
else if     2835  <= &sic <=  2835  then &newvarname =      8     ;
else if     2836  <= &sic <=  2836  then &newvarname =      8     ;
else if     3693  <= &sic <=  3693  then &newvarname =      8     ;
else if     3840  <= &sic <=  3849  then &newvarname =      8     ;
else if     3850  <= &sic <=  3851  then &newvarname =      8     ;
else if     8000  <= &sic <=  8099  then &newvarname =      8     ;
else if     2800  <= &sic <=  2809  then &newvarname =      9     ;
else if     2810  <= &sic <=  2819  then &newvarname =      9     ;
else if     2820  <= &sic <=  2829  then &newvarname =      9     ;
else if     2850  <= &sic <=  2859  then &newvarname =      9     ;
else if     2860  <= &sic <=  2869  then &newvarname =      9     ;
else if     2870  <= &sic <=  2879  then &newvarname =      9     ;
else if     2890  <= &sic <=  2899  then &newvarname =      9     ;
else if     2200  <= &sic <=  2269  then &newvarname =      10    ;
else if     2270  <= &sic <=  2279  then &newvarname =      10    ;
else if     2280  <= &sic <=  2284  then &newvarname =      10    ;
else if     2290  <= &sic <=  2295  then &newvarname =      10    ;
else if     2297  <= &sic <=  2297  then &newvarname =      10    ;
else if     2298  <= &sic <=  2298  then &newvarname =      10    ;
else if     2299  <= &sic <=  2299  then &newvarname =      10    ;
else if     2393  <= &sic <=  2395  then &newvarname =      10    ;
else if     2397  <= &sic <=  2399  then &newvarname =      10    ;
else if     800   <= &sic <=  899   then &newvarname =      11    ;
else if     1500  <= &sic <=  1511  then &newvarname =      11    ;
else if     1520  <= &sic <=  1529  then &newvarname =      11    ;
else if     1530  <= &sic <=  1539  then &newvarname =      11    ;
else if     1540  <= &sic <=  1549  then &newvarname =      11    ;
else if     1600  <= &sic <=  1699  then &newvarname =      11    ;
else if     1700  <= &sic <=  1799  then &newvarname =      11    ;
else if     2400  <= &sic <=  2439  then &newvarname =      11    ;
else if     2450  <= &sic <=  2459  then &newvarname =      11    ;
else if     2490  <= &sic <=  2499  then &newvarname =      11    ;
else if     2660  <= &sic <=  2661  then &newvarname =      11    ;
else if     2950  <= &sic <=  2952  then &newvarname =      11    ;
else if     3200  <= &sic <=  3200  then &newvarname =      11    ;
else if     3210  <= &sic <=  3211  then &newvarname =      11    ;
else if     3240  <= &sic <=  3241  then &newvarname =      11    ;
else if     3250  <= &sic <=  3259  then &newvarname =      11    ;
else if     3261  <= &sic <=  3261  then &newvarname =      11    ;
else if     3264  <= &sic <=  3264  then &newvarname =      11    ;
else if     3270  <= &sic <=  3275  then &newvarname =      11    ;
else if     3280  <= &sic <=  3281  then &newvarname =      11    ;
else if     3290  <= &sic <=  3293  then &newvarname =      11    ;
else if     3295  <= &sic <=  3299  then &newvarname =      11    ;
else if     3420  <= &sic <=  3429  then &newvarname =      11    ;
else if     3430  <= &sic <=  3433  then &newvarname =      11    ;
else if     3440  <= &sic <=  3441  then &newvarname =      11    ;
else if     3442  <= &sic <=  3442  then &newvarname =      11    ;
else if     3446  <= &sic <=  3446  then &newvarname =      11    ;
else if     3448  <= &sic <=  3448  then &newvarname =      11    ;
else if     3449  <= &sic <=  3449  then &newvarname =      11    ;
else if     3450  <= &sic <=  3451  then &newvarname =      11    ;
else if     3452  <= &sic <=  3452  then &newvarname =      11    ;
else if     3490  <= &sic <=  3499  then &newvarname =      11    ;
else if     3996  <= &sic <=  3996  then &newvarname =      11    ;
else if     3300  <= &sic <=  3300  then &newvarname =      12    ;
else if     3310  <= &sic <=  3317  then &newvarname =      12    ;
else if     3320  <= &sic <=  3325  then &newvarname =      12    ;
else if     3330  <= &sic <=  3339  then &newvarname =      12    ;
else if     3340  <= &sic <=  3341  then &newvarname =      12    ;
else if     3350  <= &sic <=  3357  then &newvarname =      12    ;
else if     3360  <= &sic <=  3369  then &newvarname =      12    ;
else if     3370  <= &sic <=  3379  then &newvarname =      12    ;
else if     3390  <= &sic <=  3399  then &newvarname =      12    ;
else if     3400  <= &sic <=  3400  then &newvarname =      13    ;
else if     3443  <= &sic <=  3443  then &newvarname =      13    ;
else if     3444  <= &sic <=  3444  then &newvarname =      13    ;
else if     3460  <= &sic <=  3469  then &newvarname =      13    ;
else if     3470  <= &sic <=  3479  then &newvarname =      13    ;
else if     3510  <= &sic <=  3519  then &newvarname =      13    ;
else if     3520  <= &sic <=  3529  then &newvarname =      13    ;
else if     3530  <= &sic <=  3530  then &newvarname =      13    ;
else if     3531  <= &sic <=  3531  then &newvarname =      13    ;
else if     3532  <= &sic <=  3532  then &newvarname =      13    ;
else if     3533  <= &sic <=  3533  then &newvarname =      13    ;
else if     3534  <= &sic <=  3534  then &newvarname =      13    ;
else if     3535  <= &sic <=  3535  then &newvarname =      13    ;
else if     3536  <= &sic <=  3536  then &newvarname =      13    ;
else if     3538  <= &sic <=  3538  then &newvarname =      13    ;
else if     3540  <= &sic <=  3549  then &newvarname =      13    ;
else if     3550  <= &sic <=  3559  then &newvarname =      13    ;
else if     3560  <= &sic <=  3569  then &newvarname =      13    ;
else if     3580  <= &sic <=  3580  then &newvarname =      13    ;
else if     3581  <= &sic <=  3581  then &newvarname =      13    ;
else if     3582  <= &sic <=  3582  then &newvarname =      13    ;
else if     3585  <= &sic <=  3585  then &newvarname =      13    ;
else if     3586  <= &sic <=  3586  then &newvarname =      13    ;
else if     3589  <= &sic <=  3589  then &newvarname =      13    ;
else if     3590  <= &sic <=  3599  then &newvarname =      13    ;
else if     3600  <= &sic <=  3600  then &newvarname =      14    ;
else if     3610  <= &sic <=  3613  then &newvarname =      14    ;
else if     3620  <= &sic <=  3621  then &newvarname =      14    ;
else if     3623  <= &sic <=  3629  then &newvarname =      14    ;
else if     3640  <= &sic <=  3644  then &newvarname =      14    ;
else if     3645  <= &sic <=  3645  then &newvarname =      14    ;
else if     3646  <= &sic <=  3646  then &newvarname =      14    ;
else if     3648  <= &sic <=  3649  then &newvarname =      14    ;
else if     3660  <= &sic <=  3660  then &newvarname =      14    ;
else if     3690  <= &sic <=  3690  then &newvarname =      14    ;
else if     3691  <= &sic <=  3692  then &newvarname =      14    ;
else if     3699  <= &sic <=  3699  then &newvarname =      14    ;
else if     2296  <= &sic <=  2296  then &newvarname =      15    ;
else if     2396  <= &sic <=  2396  then &newvarname =      15    ;
else if     3010  <= &sic <=  3011  then &newvarname =      15    ;
else if     3537  <= &sic <=  3537  then &newvarname =      15    ;
else if     3647  <= &sic <=  3647  then &newvarname =      15    ;
else if     3694  <= &sic <=  3694  then &newvarname =      15    ;
else if     3700  <= &sic <=  3700  then &newvarname =      15    ;
else if     3710  <= &sic <=  3710  then &newvarname =      15    ;
else if     3711  <= &sic <=  3711  then &newvarname =      15    ;
else if     3713  <= &sic <=  3713  then &newvarname =      15    ;
else if     3714  <= &sic <=  3714  then &newvarname =      15    ;
else if     3715  <= &sic <=  3715  then &newvarname =      15    ;
else if     3716  <= &sic <=  3716  then &newvarname =      15    ;
else if     3792  <= &sic <=  3792  then &newvarname =      15    ;
else if     3790  <= &sic <=  3791  then &newvarname =      15    ;
else if     3799  <= &sic <=  3799  then &newvarname =      15    ;
else if     3720  <= &sic <=  3720  then &newvarname =      16    ;
else if     3721  <= &sic <=  3721  then &newvarname =      16    ;
else if     3723  <= &sic <=  3724  then &newvarname =      16    ;
else if     3725  <= &sic <=  3725  then &newvarname =      16    ;
else if     3728  <= &sic <=  3729  then &newvarname =      16    ;
else if     3730  <= &sic <=  3731  then &newvarname =      16    ;
else if     3740  <= &sic <=  3743  then &newvarname =      16    ;
else if     1000  <= &sic <=  1009  then &newvarname =      17    ;
else if     1010  <= &sic <=  1019  then &newvarname =      17    ;
else if     1020  <= &sic <=  1029  then &newvarname =      17    ;
else if     1030  <= &sic <=  1039  then &newvarname =      17    ;
else if     1040  <= &sic <=  1049  then &newvarname =      17    ;
else if     1050  <= &sic <=  1059  then &newvarname =      17    ;
else if     1060  <= &sic <=  1069  then &newvarname =      17    ;
else if     1070  <= &sic <=  1079  then &newvarname =      17    ;
else if     1080  <= &sic <=  1089  then &newvarname =      17    ;
else if     1090  <= &sic <=  1099  then &newvarname =      17    ;
else if     1100  <= &sic <=  1119  then &newvarname =      17    ;
else if     1400  <= &sic <=  1499  then &newvarname =      17    ;
else if     1200  <= &sic <=  1299  then &newvarname =      18    ;
else if     1300  <= &sic <=  1300  then &newvarname =      19    ;
else if     1310  <= &sic <=  1319  then &newvarname =      19    ;
else if     1320  <= &sic <=  1329  then &newvarname =      19    ;
else if     1330  <= &sic <=  1339  then &newvarname =      19    ;
else if     1370  <= &sic <=  1379  then &newvarname =      19    ;
else if     1380  <= &sic <=  1380  then &newvarname =      19    ;
else if     1381  <= &sic <=  1381  then &newvarname =      19    ;
else if     1382  <= &sic <=  1382  then &newvarname =      19    ;
else if     1389  <= &sic <=  1389  then &newvarname =      19    ;
else if     2900  <= &sic <=  2912  then &newvarname =      19    ;
else if     2990  <= &sic <=  2999  then &newvarname =      19    ;
else if     4900  <= &sic <=  4900  then &newvarname =      20    ;
else if     4910  <= &sic <=  4911  then &newvarname =      20    ;
else if     4920  <= &sic <=  4922  then &newvarname =      20    ;
else if     4923  <= &sic <=  4923  then &newvarname =      20    ;
else if     4924  <= &sic <=  4925  then &newvarname =      20    ;
else if     4930  <= &sic <=  4931  then &newvarname =      20    ;
else if     4932  <= &sic <=  4932  then &newvarname =      20    ;
else if     4939  <= &sic <=  4939  then &newvarname =      20    ;
else if     4940  <= &sic <=  4942  then &newvarname =      20    ;
else if     4800  <= &sic <=  4800  then &newvarname =      21    ;
else if     4810  <= &sic <=  4813  then &newvarname =      21    ;
else if     4820  <= &sic <=  4822  then &newvarname =      21    ;
else if     4830  <= &sic <=  4839  then &newvarname =      21    ;
else if     4840  <= &sic <=  4841  then &newvarname =      21    ;
else if     4880  <= &sic <=  4889  then &newvarname =      21    ;
else if     4890  <= &sic <=  4890  then &newvarname =      21    ;
else if     4891  <= &sic <=  4891  then &newvarname =      21    ;
else if     4892  <= &sic <=  4892  then &newvarname =      21    ;
else if     4899  <= &sic <=  4899  then &newvarname =      21    ;
else if     7020  <= &sic <=  7021  then &newvarname =      22    ;
else if     7030  <= &sic <=  7033  then &newvarname =      22    ;
else if     7200  <= &sic <=  7200  then &newvarname =      22    ;
else if     7210  <= &sic <=  7212  then &newvarname =      22    ;
else if     7214  <= &sic <=  7214  then &newvarname =      22    ;
else if     7215  <= &sic <=  7216  then &newvarname =      22    ;
else if     7217  <= &sic <=  7217  then &newvarname =      22    ;
else if     7218  <= &sic <=  7218  then &newvarname =      22    ;
else if     7219  <= &sic <=  7219  then &newvarname =      22    ;
else if     7220  <= &sic <=  7221  then &newvarname =      22    ;
else if     7230  <= &sic <=  7231  then &newvarname =      22    ;
else if     7240  <= &sic <=  7241  then &newvarname =      22    ;
else if     7250  <= &sic <=  7251  then &newvarname =      22    ;
else if     7260  <= &sic <=  7269  then &newvarname =      22    ;
else if     7270  <= &sic <=  7290  then &newvarname =      22    ;
else if     7291  <= &sic <=  7291  then &newvarname =      22    ;
else if     7292  <= &sic <=  7299  then &newvarname =      22    ;
else if     7300  <= &sic <=  7300  then &newvarname =      22    ;
else if     7310  <= &sic <=  7319  then &newvarname =      22    ;
else if     7320  <= &sic <=  7329  then &newvarname =      22    ;
else if     7330  <= &sic <=  7339  then &newvarname =      22    ;
else if     7340  <= &sic <=  7342  then &newvarname =      22    ;
else if     7349  <= &sic <=  7349  then &newvarname =      22    ;
else if     7350  <= &sic <=  7351  then &newvarname =      22    ;
else if     7352  <= &sic <=  7352  then &newvarname =      22    ;
else if     7353  <= &sic <=  7353  then &newvarname =      22    ;
else if     7359  <= &sic <=  7359  then &newvarname =      22    ;
else if     7360  <= &sic <=  7369  then &newvarname =      22    ;
else if     7370  <= &sic <=  7372  then &newvarname =      22    ;
else if     7374  <= &sic <=  7374  then &newvarname =      22    ;
else if     7375  <= &sic <=  7375  then &newvarname =      22    ;
else if     7376  <= &sic <=  7376  then &newvarname =      22    ;
else if     7377  <= &sic <=  7377  then &newvarname =      22    ;
else if     7378  <= &sic <=  7378  then &newvarname =      22    ;
else if     7379  <= &sic <=  7379  then &newvarname =      22    ;
else if     7380  <= &sic <=  7380  then &newvarname =      22    ;
else if     7381  <= &sic <=  7382  then &newvarname =      22    ;
else if     7383  <= &sic <=  7383  then &newvarname =      22    ;
else if     7384  <= &sic <=  7384  then &newvarname =      22    ;
else if     7385  <= &sic <=  7385  then &newvarname =      22    ;
else if     7389  <= &sic <=  7390  then &newvarname =      22    ;
else if     7391  <= &sic <=  7391  then &newvarname =      22    ;
else if     7392  <= &sic <=  7392  then &newvarname =      22    ;
else if     7393  <= &sic <=  7393  then &newvarname =      22    ;
else if     7394  <= &sic <=  7394  then &newvarname =      22    ;
else if     7395  <= &sic <=  7395  then &newvarname =      22    ;
else if     7396  <= &sic <=  7396  then &newvarname =      22    ;
else if     7397  <= &sic <=  7397  then &newvarname =      22    ;
else if     7399  <= &sic <=  7399  then &newvarname =      22    ;
else if     7500  <= &sic <=  7500  then &newvarname =      22    ;
else if     7510  <= &sic <=  7519  then &newvarname =      22    ;
else if     7520  <= &sic <=  7529  then &newvarname =      22    ;
else if     7530  <= &sic <=  7539  then &newvarname =      22    ;
else if     7540  <= &sic <=  7549  then &newvarname =      22    ;
else if     7600  <= &sic <=  7600  then &newvarname =      22    ;
else if     7620  <= &sic <=  7620  then &newvarname =      22    ;
else if     7622  <= &sic <=  7622  then &newvarname =      22    ;
else if     7623  <= &sic <=  7623  then &newvarname =      22    ;
else if     7629  <= &sic <=  7629  then &newvarname =      22    ;
else if     7630  <= &sic <=  7631  then &newvarname =      22    ;
else if     7640  <= &sic <=  7641  then &newvarname =      22    ;
else if     7690  <= &sic <=  7699  then &newvarname =      22    ;
else if     8100  <= &sic <=  8199  then &newvarname =      22    ;
else if     8200  <= &sic <=  8299  then &newvarname =      22    ;
else if     8300  <= &sic <=  8399  then &newvarname =      22    ;
else if     8400  <= &sic <=  8499  then &newvarname =      22    ;
else if     8600  <= &sic <=  8699  then &newvarname =      22    ;
else if     8700  <= &sic <=  8700  then &newvarname =      22    ;
else if     8710  <= &sic <=  8713  then &newvarname =      22    ;
else if     8720  <= &sic <=  8721  then &newvarname =      22    ;
else if     8730  <= &sic <=  8734  then &newvarname =      22    ;
else if     8740  <= &sic <=  8748  then &newvarname =      22    ;
else if     8800  <= &sic <=  8899  then &newvarname =      22    ;
else if     8900  <= &sic <=  8910  then &newvarname =      22    ;
else if     8911  <= &sic <=  8911  then &newvarname =      22    ;
else if     8920  <= &sic <=  8999  then &newvarname =      22    ;
else if     3570  <= &sic <=  3579  then &newvarname =      23    ;
else if     3622  <= &sic <=  3622  then &newvarname =      23    ;
else if     3661  <= &sic <=  3661  then &newvarname =      23    ;
else if     3662  <= &sic <=  3662  then &newvarname =      23    ;
else if     3663  <= &sic <=  3663  then &newvarname =      23    ;
else if     3664  <= &sic <=  3664  then &newvarname =      23    ;
else if     3665  <= &sic <=  3665  then &newvarname =      23    ;
else if     3666  <= &sic <=  3666  then &newvarname =      23    ;
else if     3669  <= &sic <=  3669  then &newvarname =      23    ;
else if     3670  <= &sic <=  3679  then &newvarname =      23    ;
else if     3680  <= &sic <=  3680  then &newvarname =      23    ;
else if     3681  <= &sic <=  3681  then &newvarname =      23    ;
else if     3682  <= &sic <=  3682  then &newvarname =      23    ;
else if     3683  <= &sic <=  3683  then &newvarname =      23    ;
else if     3684  <= &sic <=  3684  then &newvarname =      23    ;
else if     3685  <= &sic <=  3685  then &newvarname =      23    ;
else if     3686  <= &sic <=  3686  then &newvarname =      23    ;
else if     3687  <= &sic <=  3687  then &newvarname =      23    ;
else if     3688  <= &sic <=  3688  then &newvarname =      23    ;
else if     3689  <= &sic <=  3689  then &newvarname =      23    ;
else if     3695  <= &sic <=  3695  then &newvarname =      23    ;
else if     3810  <= &sic <=  3810  then &newvarname =      23    ;
else if     3811  <= &sic <=  3811  then &newvarname =      23    ;
else if     3812  <= &sic <=  3812  then &newvarname =      23    ;
else if     3820  <= &sic <=  3820  then &newvarname =      23    ;
else if     3821  <= &sic <=  3821  then &newvarname =      23    ;
else if     3822  <= &sic <=  3822  then &newvarname =      23    ;
else if     3823  <= &sic <=  3823  then &newvarname =      23    ;
else if     3824  <= &sic <=  3824  then &newvarname =      23    ;
else if     3825  <= &sic <=  3825  then &newvarname =      23    ;
else if     3826  <= &sic <=  3826  then &newvarname =      23    ;
else if     3827  <= &sic <=  3827  then &newvarname =      23    ;
else if     3829  <= &sic <=  3829  then &newvarname =      23    ;
else if     3830  <= &sic <=  3839  then &newvarname =      23    ;
else if     7373  <= &sic <=  7373  then &newvarname =      23    ;
else if     2440  <= &sic <=  2449  then &newvarname =      24    ;
else if     2520  <= &sic <=  2549  then &newvarname =      24    ;
else if     2600  <= &sic <=  2639  then &newvarname =      24    ;
else if     2640  <= &sic <=  2659  then &newvarname =      24    ;
else if     2670  <= &sic <=  2699  then &newvarname =      24    ;
else if     2760  <= &sic <=  2761  then &newvarname =      24    ;
else if     3220  <= &sic <=  3221  then &newvarname =      24    ;
else if     3410  <= &sic <=  3412  then &newvarname =      24    ;
else if     3950  <= &sic <=  3955  then &newvarname =      24    ;
else if     4000  <= &sic <=  4013  then &newvarname =      25    ;
else if     4040  <= &sic <=  4049  then &newvarname =      25    ;
else if     4100  <= &sic <=  4100  then &newvarname =      25    ;
else if     4110  <= &sic <=  4119  then &newvarname =      25    ;
else if     4120  <= &sic <=  4121  then &newvarname =      25    ;
else if     4130  <= &sic <=  4131  then &newvarname =      25    ;
else if     4140  <= &sic <=  4142  then &newvarname =      25    ;
else if     4150  <= &sic <=  4151  then &newvarname =      25    ;
else if     4170  <= &sic <=  4173  then &newvarname =      25    ;
else if     4190  <= &sic <=  4199  then &newvarname =      25    ;
else if     4200  <= &sic <=  4200  then &newvarname =      25    ;
else if     4210  <= &sic <=  4219  then &newvarname =      25    ;
else if     4220  <= &sic <=  4229  then &newvarname =      25    ;
else if     4230  <= &sic <=  4231  then &newvarname =      25    ;
else if     4240  <= &sic <=  4249  then &newvarname =      25    ;
else if     4400  <= &sic <=  4499  then &newvarname =      25    ;
else if     4500  <= &sic <=  4599  then &newvarname =      25    ;
else if     4600  <= &sic <=  4699  then &newvarname =      25    ;
else if     4700  <= &sic <=  4700  then &newvarname =      25    ;
else if     4710  <= &sic <=  4712  then &newvarname =      25    ;
else if     4720  <= &sic <=  4729  then &newvarname =      25    ;
else if     4730  <= &sic <=  4739  then &newvarname =      25    ;
else if     4740  <= &sic <=  4749  then &newvarname =      25    ;
else if     4780  <= &sic <=  4780  then &newvarname =      25    ;
else if     4782  <= &sic <=  4782  then &newvarname =      25    ;
else if     4783  <= &sic <=  4783  then &newvarname =      25    ;
else if     4784  <= &sic <=  4784  then &newvarname =      25    ;
else if     4785  <= &sic <=  4785  then &newvarname =      25    ;
else if     4789  <= &sic <=  4789  then &newvarname =      25    ;
else if     5000  <= &sic <=  5000  then &newvarname =      26    ;
else if     5010  <= &sic <=  5015  then &newvarname =      26    ;
else if     5020  <= &sic <=  5023  then &newvarname =      26    ;
else if     5030  <= &sic <=  5039  then &newvarname =      26    ;
else if     5040  <= &sic <=  5042  then &newvarname =      26    ;
else if     5043  <= &sic <=  5043  then &newvarname =      26    ;
else if     5044  <= &sic <=  5044  then &newvarname =      26    ;
else if     5045  <= &sic <=  5045  then &newvarname =      26    ;
else if     5046  <= &sic <=  5046  then &newvarname =      26    ;
else if     5047  <= &sic <=  5047  then &newvarname =      26    ;
else if     5048  <= &sic <=  5048  then &newvarname =      26    ;
else if     5049  <= &sic <=  5049  then &newvarname =      26    ;
else if     5050  <= &sic <=  5059  then &newvarname =      26    ;
else if     5060  <= &sic <=  5060  then &newvarname =      26    ;
else if     5063  <= &sic <=  5063  then &newvarname =      26    ;
else if     5064  <= &sic <=  5064  then &newvarname =      26    ;
else if     5065  <= &sic <=  5065  then &newvarname =      26    ;
else if     5070  <= &sic <=  5078  then &newvarname =      26    ;
else if     5080  <= &sic <=  5080  then &newvarname =      26    ;
else if     5081  <= &sic <=  5081  then &newvarname =      26    ;
else if     5082  <= &sic <=  5082  then &newvarname =      26    ;
else if     5083  <= &sic <=  5083  then &newvarname =      26    ;
else if     5084  <= &sic <=  5084  then &newvarname =      26    ;
else if     5085  <= &sic <=  5085  then &newvarname =      26    ;
else if     5086  <= &sic <=  5087  then &newvarname =      26    ;
else if     5088  <= &sic <=  5088  then &newvarname =      26    ;
else if     5090  <= &sic <=  5090  then &newvarname =      26    ;
else if     5091  <= &sic <=  5092  then &newvarname =      26    ;
else if     5093  <= &sic <=  5093  then &newvarname =      26    ;
else if     5094  <= &sic <=  5094  then &newvarname =      26    ;
else if     5099  <= &sic <=  5099  then &newvarname =      26    ;
else if     5100  <= &sic <=  5100  then &newvarname =      26    ;
else if     5110  <= &sic <=  5113  then &newvarname =      26    ;
else if     5120  <= &sic <=  5122  then &newvarname =      26    ;
else if     5130  <= &sic <=  5139  then &newvarname =      26    ;
else if     5140  <= &sic <=  5149  then &newvarname =      26    ;
else if     5150  <= &sic <=  5159  then &newvarname =      26    ;
else if     5160  <= &sic <=  5169  then &newvarname =      26    ;
else if     5170  <= &sic <=  5172  then &newvarname =      26    ;
else if     5180  <= &sic <=  5182  then &newvarname =      26    ;
else if     5190  <= &sic <=  5199  then &newvarname =      26    ;
else if     5200  <= &sic <=  5200  then &newvarname =      27    ;
else if     5210  <= &sic <=  5219  then &newvarname =      27    ;
else if     5220  <= &sic <=  5229  then &newvarname =      27    ;
else if     5230  <= &sic <=  5231  then &newvarname =      27    ;
else if     5250  <= &sic <=  5251  then &newvarname =      27    ;
else if     5260  <= &sic <=  5261  then &newvarname =      27    ;
else if     5270  <= &sic <=  5271  then &newvarname =      27    ;
else if     5300  <= &sic <=  5300  then &newvarname =      27    ;
else if     5310  <= &sic <=  5311  then &newvarname =      27    ;
else if     5320  <= &sic <=  5320  then &newvarname =      27    ;
else if     5330  <= &sic <=  5331  then &newvarname =      27    ;
else if     5334  <= &sic <=  5334  then &newvarname =      27    ;
else if     5340  <= &sic <=  5349  then &newvarname =      27    ;
else if     5390  <= &sic <=  5399  then &newvarname =      27    ;
else if     5400  <= &sic <=  5400  then &newvarname =      27    ;
else if     5410  <= &sic <=  5411  then &newvarname =      27    ;
else if     5412  <= &sic <=  5412  then &newvarname =      27    ;
else if     5420  <= &sic <=  5429  then &newvarname =      27    ;
else if     5430  <= &sic <=  5439  then &newvarname =      27    ;
else if     5440  <= &sic <=  5449  then &newvarname =      27    ;
else if     5450  <= &sic <=  5459  then &newvarname =      27    ;
else if     5460  <= &sic <=  5469  then &newvarname =      27    ;
else if     5490  <= &sic <=  5499  then &newvarname =      27    ;
else if     5500  <= &sic <=  5500  then &newvarname =      27    ;
else if     5510  <= &sic <=  5529  then &newvarname =      27    ;
else if     5530  <= &sic <=  5539  then &newvarname =      27    ;
else if     5540  <= &sic <=  5549  then &newvarname =      27    ;
else if     5550  <= &sic <=  5559  then &newvarname =      27    ;
else if     5560  <= &sic <=  5569  then &newvarname =      27    ;
else if     5570  <= &sic <=  5579  then &newvarname =      27    ;
else if     5590  <= &sic <=  5599  then &newvarname =      27    ;
else if     5600  <= &sic <=  5699  then &newvarname =      27    ;
else if     5700  <= &sic <=  5700  then &newvarname =      27    ;
else if     5710  <= &sic <=  5719  then &newvarname =      27    ;
else if     5720  <= &sic <=  5722  then &newvarname =      27    ;
else if     5730  <= &sic <=  5733  then &newvarname =      27    ;
else if     5734  <= &sic <=  5734  then &newvarname =      27    ;
else if     5735  <= &sic <=  5735  then &newvarname =      27    ;
else if     5736  <= &sic <=  5736  then &newvarname =      27    ;
else if     5750  <= &sic <=  5799  then &newvarname =      27    ;
else if     5900  <= &sic <=  5900  then &newvarname =      27    ;
else if     5910  <= &sic <=  5912  then &newvarname =      27    ;
else if     5920  <= &sic <=  5929  then &newvarname =      27    ;
else if     5930  <= &sic <=  5932  then &newvarname =      27    ;
else if     5940  <= &sic <=  5940  then &newvarname =      27    ;
else if     5941  <= &sic <=  5941  then &newvarname =      27    ;
else if     5942  <= &sic <=  5942  then &newvarname =      27    ;
else if     5943  <= &sic <=  5943  then &newvarname =      27    ;
else if     5944  <= &sic <=  5944  then &newvarname =      27    ;
else if     5945  <= &sic <=  5945  then &newvarname =      27    ;
else if     5946  <= &sic <=  5946  then &newvarname =      27    ;
else if     5947  <= &sic <=  5947  then &newvarname =      27    ;
else if     5948  <= &sic <=  5948  then &newvarname =      27    ;
else if     5949  <= &sic <=  5949  then &newvarname =      27    ;
else if     5950  <= &sic <=  5959  then &newvarname =      27    ;
else if     5960  <= &sic <=  5969  then &newvarname =      27    ;
else if     5970  <= &sic <=  5979  then &newvarname =      27    ;
else if     5980  <= &sic <=  5989  then &newvarname =      27    ;
else if     5990  <= &sic <=  5990  then &newvarname =      27    ;
else if     5992  <= &sic <=  5992  then &newvarname =      27    ;
else if     5993  <= &sic <=  5993  then &newvarname =      27    ;
else if     5994  <= &sic <=  5994  then &newvarname =      27    ;
else if     5995  <= &sic <=  5995  then &newvarname =      27    ;
else if     5999  <= &sic <=  5999  then &newvarname =      27    ;
else if     5800  <= &sic <=  5819  then &newvarname =      28    ;
else if     5820  <= &sic <=  5829  then &newvarname =      28    ;
else if     5890  <= &sic <=  5899  then &newvarname =      28    ;
else if     7000  <= &sic <=  7000  then &newvarname =      28    ;
else if     7010  <= &sic <=  7019  then &newvarname =      28    ;
else if     7040  <= &sic <=  7049  then &newvarname =      28    ;
else if     7213  <= &sic <=  7213  then &newvarname =      28    ;
else if     6000  <= &sic <=  6000  then &newvarname =      29    ;
else if     6010  <= &sic <=  6019  then &newvarname =      29    ;
else if     6020  <= &sic <=  6020  then &newvarname =      29    ;
else if     6021  <= &sic <=  6021  then &newvarname =      29    ;
else if     6022  <= &sic <=  6022  then &newvarname =      29    ;
else if     6023  <= &sic <=  6024  then &newvarname =      29    ;
else if     6025  <= &sic <=  6025  then &newvarname =      29    ;
else if     6026  <= &sic <=  6026  then &newvarname =      29    ;
else if     6027  <= &sic <=  6027  then &newvarname =      29    ;
else if     6028  <= &sic <=  6029  then &newvarname =      29    ;
else if     6030  <= &sic <=  6036  then &newvarname =      29    ;
else if     6040  <= &sic <=  6059  then &newvarname =      29    ;
else if     6060  <= &sic <=  6062  then &newvarname =      29    ;
else if     6080  <= &sic <=  6082  then &newvarname =      29    ;
else if     6090  <= &sic <=  6099  then &newvarname =      29    ;
else if     6100  <= &sic <=  6100  then &newvarname =      29    ;
else if     6110  <= &sic <=  6111  then &newvarname =      29    ;
else if     6112  <= &sic <=  6113  then &newvarname =      29    ;
else if     6120  <= &sic <=  6129  then &newvarname =      29    ;
else if     6130  <= &sic <=  6139  then &newvarname =      29    ;
else if     6140  <= &sic <=  6149  then &newvarname =      29    ;
else if     6150  <= &sic <=  6159  then &newvarname =      29    ;
else if     6160  <= &sic <=  6169  then &newvarname =      29    ;
else if     6170  <= &sic <=  6179  then &newvarname =      29    ;
else if     6190  <= &sic <=  6199  then &newvarname =      29    ;
else if     6200  <= &sic <=  6299  then &newvarname =      29    ;
else if     6300  <= &sic <=  6300  then &newvarname =      29    ;
else if     6310  <= &sic <=  6319  then &newvarname =      29    ;
else if     6320  <= &sic <=  6329  then &newvarname =      29    ;
else if     6330  <= &sic <=  6331  then &newvarname =      29    ;
else if     6350  <= &sic <=  6351  then &newvarname =      29    ;
else if     6360  <= &sic <=  6361  then &newvarname =      29    ;
else if     6370  <= &sic <=  6379  then &newvarname =      29    ;
else if     6390  <= &sic <=  6399  then &newvarname =      29    ;
else if     6400  <= &sic <=  6411  then &newvarname =      29    ;
else if     6500  <= &sic <=  6500  then &newvarname =      29    ;
else if     6510  <= &sic <=  6510  then &newvarname =      29    ;
else if     6512  <= &sic <=  6512  then &newvarname =      29    ;
else if     6513  <= &sic <=  6513  then &newvarname =      29    ;
else if     6514  <= &sic <=  6514  then &newvarname =      29    ;
else if     6515  <= &sic <=  6515  then &newvarname =      29    ;
else if     6517  <= &sic <=  6519  then &newvarname =      29    ;
else if     6520  <= &sic <=  6529  then &newvarname =      29    ;
else if     6530  <= &sic <=  6531  then &newvarname =      29    ;
else if     6532  <= &sic <=  6532  then &newvarname =      29    ;
else if     6540  <= &sic <=  6541  then &newvarname =      29    ;
else if     6550  <= &sic <=  6553  then &newvarname =      29    ;
else if     6590  <= &sic <=  6599  then &newvarname =      29    ;
else if     6610  <= &sic <=  6611  then &newvarname =      29    ;
else if     6700  <= &sic <=  6700  then &newvarname =      29    ;
else if     6710  <= &sic <=  6719  then &newvarname =      29    ;
else if     6720  <= &sic <=  6722  then &newvarname =      29    ;
else if     6723  <= &sic <=  6723  then &newvarname =      29    ;
else if     6724  <= &sic <=  6724  then &newvarname =      29    ;
else if     6725  <= &sic <=  6725  then &newvarname =      29    ;
else if     6726  <= &sic <=  6726  then &newvarname =      29    ;
else if     6730  <= &sic <=  6733  then &newvarname =      29    ;
else if     6740  <= &sic <=  6779  then &newvarname =      29    ;
else if     6790  <= &sic <=  6791  then &newvarname =      29    ;
else if     6792  <= &sic <=  6792  then &newvarname =      29    ;
else if     6793  <= &sic <=  6793  then &newvarname =      29    ;
else if     6794  <= &sic <=  6794  then &newvarname =      29    ;
else if     6795  <= &sic <=  6795  then &newvarname =      29    ;
else if     6798  <= &sic <=  6798  then &newvarname =      29    ;
else if     6799  <= &sic <=  6799  then &newvarname =      29    ;
else if     4950  <= &sic <=  4959  then &newvarname =      30    ;
else if     4960  <= &sic <=  4961  then &newvarname =      30    ;
else if     4970  <= &sic <=  4971  then &newvarname =      30    ;
else if     4990  <= &sic <=  4991  then &newvarname =      30    ;
else &newvarname =30;
run;
 
 
 
proc format;
   value ff
      1="Food Products"
2="Beer and Liquor"
3="Tobacco Products"
4="Recreation"
5="Printing and Publishing"
6="Consumer Goods"
7="Apparel"
8="Healthcare, Medical Equip and Pharmaceuticals"
9="Chemicals"
10="Textiles"
11="Construction and Const. Materials"
12="Steel Works"
13="Fabricated Products"
14="Electrical Equipment"
15="Automobiles"
16="Aircraft, Ships and Equipment"
17="Precious Metals and Industrial Metal Mining"
18="Coal"
19="Petroleum and Natural Gas"
20="Utilities"
21="Communication"
22="Personal and Business Services"
23="Business Equipment"
24="Business Supplies and Shipping Containers"
25="Transportation"
26="Wholesale"
27="Retail"
28="Restaurants, Hotels & Motels"
29="Financial Institutions"
30="Other";
run;
%mend;
 
 
***************************************************************************************;
***************************************************************************************;
***************************************************************************************;
*CREATES FAMA-FRENCH 48 INDUSTRY CLASSIFICATIONS
***************************************************************************************;
***************************************************************************************;
***************************************************************************************;
%macro ff48(data=,newvarname=industry,sic=sic,out=&data);
 
data &out;
    set &data;
    &newvarname = 48;
if    100   <= &sic <=  199   then &newvarname =      1     ;
else  if          200   <=    &sic  <=    299   then  &newvarname =     1     ;
else  if          700   <=    &sic  <=    799   then  &newvarname =     1     ;
else  if          910   <=    &sic  <=    919   then  &newvarname =     1     ;
else  if          2048  <=    &sic  <=    2048  then  &newvarname =     1     ;
else  if          2000  <=    &sic  <=    2009  then  &newvarname =     2     ;
else  if          2010  <=    &sic  <=    2019  then  &newvarname =     2     ;
else  if          2020  <=    &sic  <=    2029  then  &newvarname =     2     ;
else  if          2030  <=    &sic  <=    2039  then  &newvarname =     2     ;
else  if          2040  <=    &sic  <=    2046  then  &newvarname =     2     ;
else  if          2050  <=    &sic  <=    2059  then  &newvarname =     2     ;
else  if          2060  <=    &sic  <=    2063  then  &newvarname =     2     ;
else  if          2070  <=    &sic  <=    2079  then  &newvarname =     2     ;
else  if          2090  <=    &sic  <=    2092  then  &newvarname =     2     ;
else  if          2095  <=    &sic  <=    2095  then  &newvarname =     2     ;
else  if          2098  <=    &sic  <=    2099  then  &newvarname =     2     ;
else  if          2064  <=    &sic  <=    2068  then  &newvarname =     3     ;
else  if          2086  <=    &sic  <=    2086  then  &newvarname =     3     ;
else  if          2087  <=    &sic  <=    2087  then  &newvarname =     3     ;
else  if          2096  <=    &sic  <=    2096  then  &newvarname =     3     ;
else  if          2097  <=    &sic  <=    2097  then  &newvarname =     3     ;
else  if          2080  <=    &sic  <=    2080  then  &newvarname =     4     ;
else  if          2082  <=    &sic  <=    2082  then  &newvarname =     4     ;
else  if          2083  <=    &sic  <=    2083  then  &newvarname =     4     ;
else  if          2084  <=    &sic  <=    2084  then  &newvarname =     4     ;
else  if          2085  <=    &sic  <=    2085  then  &newvarname =     4     ;
else  if          2100  <=    &sic  <=    2199  then  &newvarname =     5     ;
else  if          920   <=    &sic  <=    999   then  &newvarname =     6     ;
else  if          3650  <=    &sic  <=    3651  then  &newvarname =     6     ;
else  if          3652  <=    &sic  <=    3652  then  &newvarname =     6     ;
else  if          3732  <=    &sic  <=    3732  then  &newvarname =     6     ;
else  if          3930  <=    &sic  <=    3931  then  &newvarname =     6     ;
else  if          3940  <=    &sic  <=    3949  then  &newvarname =     6     ;
else  if          7800  <=    &sic  <=    7829  then  &newvarname =     7     ;
else  if          7830  <=    &sic  <=    7833  then  &newvarname =     7     ;
else  if          7840  <=    &sic  <=    7841  then  &newvarname =     7     ;
else  if          7900  <=    &sic  <=    7900  then  &newvarname =     7     ;
else  if          7910  <=    &sic  <=    7911  then  &newvarname =     7     ;
else  if          7920  <=    &sic  <=    7929  then  &newvarname =     7     ;
else  if          7930  <=    &sic  <=    7933  then  &newvarname =     7     ;
else  if          7940  <=    &sic  <=    7949  then  &newvarname =     7     ;
else  if          7980  <=    &sic  <=    7980  then  &newvarname =     7     ;
else  if          7990  <=    &sic  <=    7999  then  &newvarname =     7     ;
else  if          2700  <=    &sic  <=    2709  then  &newvarname =     8     ;
else  if          2710  <=    &sic  <=    2719  then  &newvarname =     8     ;
else  if          2720  <=    &sic  <=    2729  then  &newvarname =     8     ;
else  if          2730  <=    &sic  <=    2739  then  &newvarname =     8     ;
else  if          2740  <=    &sic  <=    2749  then  &newvarname =     8     ;
else  if          2770  <=    &sic  <=    2771  then  &newvarname =     8     ;
else  if          2780  <=    &sic  <=    2789  then  &newvarname =     8     ;
else  if          2790  <=    &sic  <=    2799  then  &newvarname =     8     ;
else  if          2047  <=    &sic  <=    2047  then  &newvarname =     9     ;
else  if          2391  <=    &sic  <=    2392  then  &newvarname =     9     ;
else  if          2510  <=    &sic  <=    2519  then  &newvarname =     9     ;
else  if          2590  <=    &sic  <=    2599  then  &newvarname =     9     ;
else  if          2840  <=    &sic  <=    2843  then  &newvarname =     9     ;
else  if          2844  <=    &sic  <=    2844  then  &newvarname =     9     ;
else  if          3160  <=    &sic  <=    3161  then  &newvarname =     9     ;
else  if          3170  <=    &sic  <=    3171  then  &newvarname =     9     ;
else  if          3172  <=    &sic  <=    3172  then  &newvarname =     9     ;
else  if          3190  <=    &sic  <=    3199  then  &newvarname =     9     ;
else  if          3229  <=    &sic  <=    3229  then  &newvarname =     9     ;
else  if          3260  <=    &sic  <=    3260  then  &newvarname =     9     ;
else  if          3262  <=    &sic  <=    3263  then  &newvarname =     9     ;
else  if          3269  <=    &sic  <=    3269  then  &newvarname =     9     ;
else  if          3230  <=    &sic  <=    3231  then  &newvarname =     9     ;
else  if          3630  <=    &sic  <=    3639  then  &newvarname =     9     ;
else  if          3750  <=    &sic  <=    3751  then  &newvarname =     9     ;
else  if          3800  <=    &sic  <=    3800  then  &newvarname =     9     ;
else  if          3860  <=    &sic  <=    3861  then  &newvarname =     9     ;
else  if          3870  <=    &sic  <=    3873  then  &newvarname =     9     ;
else  if          3910  <=    &sic  <=    3911  then  &newvarname =     9     ;
else  if          3914  <=    &sic  <=    3914  then  &newvarname =     9     ;
else  if          3915  <=    &sic  <=    3915  then  &newvarname =     9     ;
else  if          3960  <=    &sic  <=    3962  then  &newvarname =     9     ;
else  if          3991  <=    &sic  <=    3991  then  &newvarname =     9     ;
else  if          3995  <=    &sic  <=    3995  then  &newvarname =     9     ;
else  if          2300  <=    &sic  <=    2390  then  &newvarname =     10    ;
else  if          3020  <=    &sic  <=    3021  then  &newvarname =     10    ;
else  if          3100  <=    &sic  <=    3111  then  &newvarname =     10    ;
else  if          3130  <=    &sic  <=    3131  then  &newvarname =     10    ;
else  if          3140  <=    &sic  <=    3149  then  &newvarname =     10    ;
else  if          3150  <=    &sic  <=    3151  then  &newvarname =     10    ;
else  if          3963  <=    &sic  <=    3965  then  &newvarname =     10    ;
else  if          8000  <=    &sic  <=    8099  then  &newvarname =     11    ;
else  if          3693  <=    &sic  <=    3693  then  &newvarname =     12    ;
else  if          3840  <=    &sic  <=    3849  then  &newvarname =     12    ;
else  if          3850  <=    &sic  <=    3851  then  &newvarname =     12    ;
else  if          2830  <=    &sic  <=    2830  then  &newvarname =     13    ;
else  if          2831  <=    &sic  <=    2831  then  &newvarname =     13    ;
else  if          2833  <=    &sic  <=    2833  then  &newvarname =     13    ;
else  if          2834  <=    &sic  <=    2834  then  &newvarname =     13    ;
else  if          2835  <=    &sic  <=    2835  then  &newvarname =     13    ;
else  if          2836  <=    &sic  <=    2836  then  &newvarname =     13    ;
else  if          2800  <=    &sic  <=    2809  then  &newvarname =     14    ;
else  if          2810  <=    &sic  <=    2819  then  &newvarname =     14    ;
else  if          2820  <=    &sic  <=    2829  then  &newvarname =     14    ;
else  if          2850  <=    &sic  <=    2859  then  &newvarname =     14    ;
else  if          2860  <=    &sic  <=    2869  then  &newvarname =     14    ;
else  if          2870  <=    &sic  <=    2879  then  &newvarname =     14    ;
else  if          2890  <=    &sic  <=    2899  then  &newvarname =     14    ;
else  if          3031  <=    &sic  <=    3031  then  &newvarname =     15    ;
else  if          3041  <=    &sic  <=    3041  then  &newvarname =     15    ;
else  if          3050  <=    &sic  <=    3053  then  &newvarname =     15    ;
else  if          3060  <=    &sic  <=    3069  then  &newvarname =     15    ;
else  if          3070  <=    &sic  <=    3079  then  &newvarname =     15    ;
else  if          3080  <=    &sic  <=    3089  then  &newvarname =     15    ;
else  if          3090  <=    &sic  <=    3099  then  &newvarname =     15    ;
else  if          2200  <=    &sic  <=    2269  then  &newvarname =     16    ;
else  if          2270  <=    &sic  <=    2279  then  &newvarname =     16    ;
else  if          2280  <=    &sic  <=    2284  then  &newvarname =     16    ;
else  if          2290  <=    &sic  <=    2295  then  &newvarname =     16    ;
else  if          2297  <=    &sic  <=    2297  then  &newvarname =     16    ;
else  if          2298  <=    &sic  <=    2298  then  &newvarname =     16    ;
else  if          2299  <=    &sic  <=    2299  then  &newvarname =     16    ;
else  if          2393  <=    &sic  <=    2395  then  &newvarname =     16    ;
else  if          2397  <=    &sic  <=    2399  then  &newvarname =     16    ;
else  if          800   <=    &sic  <=    899   then  &newvarname =     17    ;
else  if          2400  <=    &sic  <=    2439  then  &newvarname =     17    ;
else  if          2450  <=    &sic  <=    2459  then  &newvarname =     17    ;
else  if          2490  <=    &sic  <=    2499  then  &newvarname =     17    ;
else  if          2660  <=    &sic  <=    2661  then  &newvarname =     17    ;
else  if          2950  <=    &sic  <=    2952  then  &newvarname =     17    ;
else  if          3200  <=    &sic  <=    3200  then  &newvarname =     17    ;
else  if          3210  <=    &sic  <=    3211  then  &newvarname =     17    ;
else  if          3240  <=    &sic  <=    3241  then  &newvarname =     17    ;
else  if          3250  <=    &sic  <=    3259  then  &newvarname =     17    ;
else  if          3261  <=    &sic  <=    3261  then  &newvarname =     17    ;
else  if          3264  <=    &sic  <=    3264  then  &newvarname =     17    ;
else  if          3270  <=    &sic  <=    3275  then  &newvarname =     17    ;
else  if          3280  <=    &sic  <=    3281  then  &newvarname =     17    ;
else  if          3290  <=    &sic  <=    3293  then  &newvarname =     17    ;
else  if          3295  <=    &sic  <=    3299  then  &newvarname =     17    ;
else  if          3420  <=    &sic  <=    3429  then  &newvarname =     17    ;
else  if          3430  <=    &sic  <=    3433  then  &newvarname =     17    ;
else  if          3440  <=    &sic  <=    3441  then  &newvarname =     17    ;
else  if          3442  <=    &sic  <=    3442  then  &newvarname =     17    ;
else  if          3446  <=    &sic  <=    3446  then  &newvarname =     17    ;
else  if          3448  <=    &sic  <=    3448  then  &newvarname =     17    ;
else  if          3449  <=    &sic  <=    3449  then  &newvarname =     17    ;
else  if          3450  <=    &sic  <=    3451  then  &newvarname =     17    ;
else  if          3452  <=    &sic  <=    3452  then  &newvarname =     17    ;
else  if          3490  <=    &sic  <=    3499  then  &newvarname =     17    ;
else  if          3996  <=    &sic  <=    3996  then  &newvarname =     17    ;
else  if          1500  <=    &sic  <=    1511  then  &newvarname =     18    ;
else  if          1520  <=    &sic  <=    1529  then  &newvarname =     18    ;
else  if          1530  <=    &sic  <=    1539  then  &newvarname =     18    ;
else  if          1540  <=    &sic  <=    1549  then  &newvarname =     18    ;
else  if          1600  <=    &sic  <=    1699  then  &newvarname =     18    ;
else  if          1700  <=    &sic  <=    1799  then  &newvarname =     18    ;
else  if          3300  <=    &sic  <=    3300  then  &newvarname =     19    ;
else  if          3310  <=    &sic  <=    3317  then  &newvarname =     19    ;
else  if          3320  <=    &sic  <=    3325  then  &newvarname =     19    ;
else  if          3330  <=    &sic  <=    3339  then  &newvarname =     19    ;
else  if          3340  <=    &sic  <=    3341  then  &newvarname =     19    ;
else  if          3350  <=    &sic  <=    3357  then  &newvarname =     19    ;
else  if          3360  <=    &sic  <=    3369  then  &newvarname =     19    ;
else  if          3370  <=    &sic  <=    3379  then  &newvarname =     19    ;
else  if          3390  <=    &sic  <=    3399  then  &newvarname =     19    ;
else  if          3400  <=    &sic  <=    3400  then  &newvarname =     20    ;
else  if          3443  <=    &sic  <=    3443  then  &newvarname =     20    ;
else  if          3444  <=    &sic  <=    3444  then  &newvarname =     20    ;
else  if          3460  <=    &sic  <=    3469  then  &newvarname =     20    ;
else  if          3470  <=    &sic  <=    3479  then  &newvarname =     20    ;
else  if          3510  <=    &sic  <=    3519  then  &newvarname =     21    ;
else  if          3520  <=    &sic  <=    3529  then  &newvarname =     21    ;
else  if          3530  <=    &sic  <=    3530  then  &newvarname =     21    ;
else  if          3531  <=    &sic  <=    3531  then  &newvarname =     21    ;
else  if          3532  <=    &sic  <=    3532  then  &newvarname =     21    ;
else  if          3533  <=    &sic  <=    3533  then  &newvarname =     21    ;
else  if          3534  <=    &sic  <=    3534  then  &newvarname =     21    ;
else  if          3535  <=    &sic  <=    3535  then  &newvarname =     21    ;
else  if          3536  <=    &sic  <=    3536  then  &newvarname =     21    ;
else  if          3538  <=    &sic  <=    3538  then  &newvarname =     21    ;
else  if          3540  <=    &sic  <=    3549  then  &newvarname =     21    ;
else  if          3550  <=    &sic  <=    3559  then  &newvarname =     21    ;
else  if          3560  <=    &sic  <=    3569  then  &newvarname =     21    ;
else  if          3580  <=    &sic  <=    3580  then  &newvarname =     21    ;
else  if          3581  <=    &sic  <=    3581  then  &newvarname =     21    ;
else  if          3582  <=    &sic  <=    3582  then  &newvarname =     21    ;
else  if          3585  <=    &sic  <=    3585  then  &newvarname =     21    ;
else  if          3586  <=    &sic  <=    3586  then  &newvarname =     21    ;
else  if          3589  <=    &sic  <=    3589  then  &newvarname =     21    ;
else  if          3590  <=    &sic  <=    3599  then  &newvarname =     21    ;
else  if          3600  <=    &sic  <=    3600  then  &newvarname =     22    ;
else  if          3610  <=    &sic  <=    3613  then  &newvarname =     22    ;
else  if          3620  <=    &sic  <=    3621  then  &newvarname =     22    ;
else  if          3623  <=    &sic  <=    3629  then  &newvarname =     22    ;
else  if          3640  <=    &sic  <=    3644  then  &newvarname =     22    ;
else  if          3645  <=    &sic  <=    3645  then  &newvarname =     22    ;
else  if          3646  <=    &sic  <=    3646  then  &newvarname =     22    ;
else  if          3648  <=    &sic  <=    3649  then  &newvarname =     22    ;
else  if          3660  <=    &sic  <=    3660  then  &newvarname =     22    ;
else  if          3690  <=    &sic  <=    3690  then  &newvarname =     22    ;
else  if          3691  <=    &sic  <=    3692  then  &newvarname =     22    ;
else  if          3699  <=    &sic  <=    3699  then  &newvarname =     22    ;
else  if          2296  <=    &sic  <=    2296  then  &newvarname =     23    ;
else  if          2396  <=    &sic  <=    2396  then  &newvarname =     23    ;
else  if          3010  <=    &sic  <=    3011  then  &newvarname =     23    ;
else  if          3537  <=    &sic  <=    3537  then  &newvarname =     23    ;
else  if          3647  <=    &sic  <=    3647  then  &newvarname =     23    ;
else  if          3694  <=    &sic  <=    3694  then  &newvarname =     23    ;
else  if          3700  <=    &sic  <=    3700  then  &newvarname =     23    ;
else  if          3710  <=    &sic  <=    3710  then  &newvarname =     23    ;
else  if          3711  <=    &sic  <=    3711  then  &newvarname =     23    ;
else  if          3713  <=    &sic  <=    3713  then  &newvarname =     23    ;
else  if          3714  <=    &sic  <=    3714  then  &newvarname =     23    ;
else  if          3715  <=    &sic  <=    3715  then  &newvarname =     23    ;
else  if          3716  <=    &sic  <=    3716  then  &newvarname =     23    ;
else  if          3792  <=    &sic  <=    3792  then  &newvarname =     23    ;
else  if          3790  <=    &sic  <=    3791  then  &newvarname =     23    ;
else  if          3799  <=    &sic  <=    3799  then  &newvarname =     23    ;
else  if          3720  <=    &sic  <=    3720  then  &newvarname =     24    ;
else  if          3721  <=    &sic  <=    3721  then  &newvarname =     24    ;
else  if          3723  <=    &sic  <=    3724  then  &newvarname =     24    ;
else  if          3725  <=    &sic  <=    3725  then  &newvarname =     24    ;
else  if          3728  <=    &sic  <=    3729  then  &newvarname =     24    ;
else  if          3730  <=    &sic  <=    3731  then  &newvarname =     25    ;
else  if          3740  <=    &sic  <=    3743  then  &newvarname =     25    ;
else  if          3760  <=    &sic  <=    3769  then  &newvarname =     26    ;
else  if          3795  <=    &sic  <=    3795  then  &newvarname =     26    ;
else  if          3480  <=    &sic  <=    3489  then  &newvarname =     26    ;
else  if          1040  <=    &sic  <=    1049  then  &newvarname =     27    ;
else  if          1000  <=    &sic  <=    1009  then  &newvarname =     28    ;
else  if          1010  <=    &sic  <=    1019  then  &newvarname =     28    ;
else  if          1020  <=    &sic  <=    1029  then  &newvarname =     28    ;
else  if          1030  <=    &sic  <=    1039  then  &newvarname =     28    ;
else  if          1050  <=    &sic  <=    1059  then  &newvarname =     28    ;
else  if          1060  <=    &sic  <=    1069  then  &newvarname =     28    ;
else  if          1070  <=    &sic  <=    1079  then  &newvarname =     28    ;
else  if          1080  <=    &sic  <=    1089  then  &newvarname =     28    ;
else  if          1090  <=    &sic  <=    1099  then  &newvarname =     28    ;
else  if          1100  <=    &sic  <=    1119  then  &newvarname =     28    ;
else  if          1400  <=    &sic  <=    1499  then  &newvarname =     28    ;
else  if          1200  <=    &sic  <=    1299  then  &newvarname =     29    ;
else  if          1300  <=    &sic  <=    1300  then  &newvarname =     30    ;
else  if          1310  <=    &sic  <=    1319  then  &newvarname =     30    ;
else  if          1320  <=    &sic  <=    1329  then  &newvarname =     30    ;
else  if          1330  <=    &sic  <=    1339  then  &newvarname =     30    ;
else  if          1370  <=    &sic  <=    1379  then  &newvarname =     30    ;
else  if          1380  <=    &sic  <=    1380  then  &newvarname =     30    ;
else  if          1381  <=    &sic  <=    1381  then  &newvarname =     30    ;
else  if          1382  <=    &sic  <=    1382  then  &newvarname =     30    ;
else  if          1389  <=    &sic  <=    1389  then  &newvarname =     30    ;
else  if          2900  <=    &sic  <=    2912  then  &newvarname =     30    ;
else  if          2990  <=    &sic  <=    2999  then  &newvarname =     30    ;
else  if          4900  <=    &sic  <=    4900  then  &newvarname =     31    ;
else  if          4910  <=    &sic  <=    4911  then  &newvarname =     31    ;
else  if          4920  <=    &sic  <=    4922  then  &newvarname =     31    ;
else  if          4923  <=    &sic  <=    4923  then  &newvarname =     31    ;
else  if          4924  <=    &sic  <=    4925  then  &newvarname =     31    ;
else  if          4930  <=    &sic  <=    4931  then  &newvarname =     31    ;
else  if          4932  <=    &sic  <=    4932  then  &newvarname =     31    ;
else  if          4939  <=    &sic  <=    4939  then  &newvarname =     31    ;
else  if          4940  <=    &sic  <=    4942  then  &newvarname =     31    ;
else  if          4800  <=    &sic  <=    4800  then  &newvarname =     32    ;
else  if          4810  <=    &sic  <=    4813  then  &newvarname =     32    ;
else  if          4820  <=    &sic  <=    4822  then  &newvarname =     32    ;
else  if          4830  <=    &sic  <=    4839  then  &newvarname =     32    ;
else  if          4840  <=    &sic  <=    4841  then  &newvarname =     32    ;
else  if          4880  <=    &sic  <=    4889  then  &newvarname =     32    ;
else  if          4890  <=    &sic  <=    4890  then  &newvarname =     32    ;
else  if          4891  <=    &sic  <=    4891  then  &newvarname =     32    ;
else  if          4892  <=    &sic  <=    4892  then  &newvarname =     32    ;
else  if          4899  <=    &sic  <=    4899  then  &newvarname =     32    ;
else  if          7020  <=    &sic  <=    7021  then  &newvarname =     33    ;
else  if          7030  <=    &sic  <=    7033  then  &newvarname =     33    ;
else  if          7200  <=    &sic  <=    7200  then  &newvarname =     33    ;
else  if          7210  <=    &sic  <=    7212  then  &newvarname =     33    ;
else  if          7214  <=    &sic  <=    7214  then  &newvarname =     33    ;
else  if          7215  <=    &sic  <=    7216  then  &newvarname =     33    ;
else  if          7217  <=    &sic  <=    7217  then  &newvarname =     33    ;
else  if          7219  <=    &sic  <=    7219  then  &newvarname =     33    ;
else  if          7220  <=    &sic  <=    7221  then  &newvarname =     33    ;
else  if          7230  <=    &sic  <=    7231  then  &newvarname =     33    ;
else  if          7240  <=    &sic  <=    7241  then  &newvarname =     33    ;
else  if          7250  <=    &sic  <=    7251  then  &newvarname =     33    ;
else  if          7260  <=    &sic  <=    7269  then  &newvarname =     33    ;
else  if          7270  <=    &sic  <=    7290  then  &newvarname =     33    ;
else  if          7291  <=    &sic  <=    7291  then  &newvarname =     33    ;
else  if          7292  <=    &sic  <=    7299  then  &newvarname =     33    ;
else  if          7395  <=    &sic  <=    7395  then  &newvarname =     33    ;
else  if          7500  <=    &sic  <=    7500  then  &newvarname =     33    ;
else  if          7520  <=    &sic  <=    7529  then  &newvarname =     33    ;
else  if          7530  <=    &sic  <=    7539  then  &newvarname =     33    ;
else  if          7540  <=    &sic  <=    7549  then  &newvarname =     33    ;
else  if          7600  <=    &sic  <=    7600  then  &newvarname =     33    ;
else  if          7620  <=    &sic  <=    7620  then  &newvarname =     33    ;
else  if          7622  <=    &sic  <=    7622  then  &newvarname =     33    ;
else  if          7623  <=    &sic  <=    7623  then  &newvarname =     33    ;
else  if          7629  <=    &sic  <=    7629  then  &newvarname =     33    ;
else  if          7630  <=    &sic  <=    7631  then  &newvarname =     33    ;
else  if          7640  <=    &sic  <=    7641  then  &newvarname =     33    ;
else  if          7690  <=    &sic  <=    7699  then  &newvarname =     33    ;
else  if          8100  <=    &sic  <=    8199  then  &newvarname =     33    ;
else  if          8200  <=    &sic  <=    8299  then  &newvarname =     33    ;
else  if          8300  <=    &sic  <=    8399  then  &newvarname =     33    ;
else  if          8400  <=    &sic  <=    8499  then  &newvarname =     33    ;
else  if          8600  <=    &sic  <=    8699  then  &newvarname =     33    ;
else  if          8800  <=    &sic  <=    8899  then  &newvarname =     33    ;
else  if          7510  <=    &sic  <=    7515  then  &newvarname =     33    ;
else  if          2750  <=    &sic  <=    2759  then  &newvarname =     34    ;
else  if          3993  <=    &sic  <=    3993  then  &newvarname =     34    ;
else  if          7218  <=    &sic  <=    7218  then  &newvarname =     34    ;
else  if          7300  <=    &sic  <=    7300  then  &newvarname =     34    ;
else  if          7310  <=    &sic  <=    7319  then  &newvarname =     34    ;
else  if          7320  <=    &sic  <=    7329  then  &newvarname =     34    ;
else  if          7330  <=    &sic  <=    7339  then  &newvarname =     34    ;
else  if          7340  <=    &sic  <=    7342  then  &newvarname =     34    ;
else  if          7349  <=    &sic  <=    7349  then  &newvarname =     34    ;
else  if          7350  <=    &sic  <=    7351  then  &newvarname =     34    ;
else  if          7352  <=    &sic  <=    7352  then  &newvarname =     34    ;
else  if          7353  <=    &sic  <=    7353  then  &newvarname =     34    ;
else  if          7359  <=    &sic  <=    7359  then  &newvarname =     34    ;
else  if          7360  <=    &sic  <=    7369  then  &newvarname =     34    ;
else  if          7370  <=    &sic  <=    7372  then  &newvarname =     34    ;
else  if          7374  <=    &sic  <=    7374  then  &newvarname =     34    ;
else  if          7375  <=    &sic  <=    7375  then  &newvarname =     34    ;
else  if          7376  <=    &sic  <=    7376  then  &newvarname =     34    ;
else  if          7377  <=    &sic  <=    7377  then  &newvarname =     34    ;
else  if          7378  <=    &sic  <=    7378  then  &newvarname =     34    ;
else  if          7379  <=    &sic  <=    7379  then  &newvarname =     34    ;
else  if          7380  <=    &sic  <=    7380  then  &newvarname =     34    ;
else  if          7381  <=    &sic  <=    7382  then  &newvarname =     34    ;
else  if          7383  <=    &sic  <=    7383  then  &newvarname =     34    ;
else  if          7384  <=    &sic  <=    7384  then  &newvarname =     34    ;
else  if          7385  <=    &sic  <=    7385  then  &newvarname =     34    ;
else  if          7389  <=    &sic  <=    7390  then  &newvarname =     34    ;
else  if          7391  <=    &sic  <=    7391  then  &newvarname =     34    ;
else  if          7392  <=    &sic  <=    7392  then  &newvarname =     34    ;
else  if          7393  <=    &sic  <=    7393  then  &newvarname =     34    ;
else  if          7394  <=    &sic  <=    7394  then  &newvarname =     34    ;
else  if          7396  <=    &sic  <=    7396  then  &newvarname =     34    ;
else  if          7397  <=    &sic  <=    7397  then  &newvarname =     34    ;
else  if          7399  <=    &sic  <=    7399  then  &newvarname =     34    ;
else  if          7519  <=    &sic  <=    7519  then  &newvarname =     34    ;
else  if          8700  <=    &sic  <=    8700  then  &newvarname =     34    ;
else  if          8710  <=    &sic  <=    8713  then  &newvarname =     34    ;
else  if          8720  <=    &sic  <=    8721  then  &newvarname =     34    ;
else  if          8730  <=    &sic  <=    8734  then  &newvarname =     34    ;
else  if          8740  <=    &sic  <=    8748  then  &newvarname =     34    ;
else  if          8900  <=    &sic  <=    8910  then  &newvarname =     34    ;
else  if          8911  <=    &sic  <=    8911  then  &newvarname =     34    ;
else  if          8920  <=    &sic  <=    8999  then  &newvarname =     34    ;
else  if          4220  <=    &sic  <=    4229  then  &newvarname =     34    ;
else  if          3570  <=    &sic  <=    3579  then  &newvarname =     35    ;
else  if          3680  <=    &sic  <=    3680  then  &newvarname =     35    ;
else  if          3681  <=    &sic  <=    3681  then  &newvarname =     35    ;
else  if          3682  <=    &sic  <=    3682  then  &newvarname =     35    ;
else  if          3683  <=    &sic  <=    3683  then  &newvarname =     35    ;
else  if          3684  <=    &sic  <=    3684  then  &newvarname =     35    ;
else  if          3685  <=    &sic  <=    3685  then  &newvarname =     35    ;
else  if          3686  <=    &sic  <=    3686  then  &newvarname =     35    ;
else  if          3687  <=    &sic  <=    3687  then  &newvarname =     35    ;
else  if          3688  <=    &sic  <=    3688  then  &newvarname =     35    ;
else  if          3689  <=    &sic  <=    3689  then  &newvarname =     35    ;
else  if          3695  <=    &sic  <=    3695  then  &newvarname =     35    ;
else  if          7373  <=    &sic  <=    7373  then  &newvarname =     35    ;
else  if          3622  <=    &sic  <=    3622  then  &newvarname =     36    ;
else  if          3661  <=    &sic  <=    3661  then  &newvarname =     36    ;
else  if          3662  <=    &sic  <=    3662  then  &newvarname =     36    ;
else  if          3663  <=    &sic  <=    3663  then  &newvarname =     36    ;
else  if          3664  <=    &sic  <=    3664  then  &newvarname =     36    ;
else  if          3665  <=    &sic  <=    3665  then  &newvarname =     36    ;
else  if          3666  <=    &sic  <=    3666  then  &newvarname =     36    ;
else  if          3669  <=    &sic  <=    3669  then  &newvarname =     36    ;
else  if          3670  <=    &sic  <=    3679  then  &newvarname =     36    ;
else  if          3810  <=    &sic  <=    3810  then  &newvarname =     36    ;
else  if          3812  <=    &sic  <=    3812  then  &newvarname =     36    ;
else  if          3811  <=    &sic  <=    3811  then  &newvarname =     37    ;
else  if          3820  <=    &sic  <=    3820  then  &newvarname =     37    ;
else  if          3821  <=    &sic  <=    3821  then  &newvarname =     37    ;
else  if          3822  <=    &sic  <=    3822  then  &newvarname =     37    ;
else  if          3823  <=    &sic  <=    3823  then  &newvarname =     37    ;
else  if          3824  <=    &sic  <=    3824  then  &newvarname =     37    ;
else  if          3825  <=    &sic  <=    3825  then  &newvarname =     37    ;
else  if          3826  <=    &sic  <=    3826  then  &newvarname =     37    ;
else  if          3827  <=    &sic  <=    3827  then  &newvarname =     37    ;
else  if          3829  <=    &sic  <=    3829  then  &newvarname =     37    ;
else  if          3830  <=    &sic  <=    3839  then  &newvarname =     37    ;
else  if          2520  <=    &sic  <=    2549  then  &newvarname =     38    ;
else  if          2600  <=    &sic  <=    2639  then  &newvarname =     38    ;
else  if          2670  <=    &sic  <=    2699  then  &newvarname =     38    ;
else  if          2760  <=    &sic  <=    2761  then  &newvarname =     38    ;
else  if          3950  <=    &sic  <=    3955  then  &newvarname =     38    ;
else  if          2440  <=    &sic  <=    2449  then  &newvarname =     39    ;
else  if          2640  <=    &sic  <=    2659  then  &newvarname =     39    ;
else  if          3220  <=    &sic  <=    3221  then  &newvarname =     39    ;
else  if          3410  <=    &sic  <=    3412  then  &newvarname =     39    ;
else  if          4000  <=    &sic  <=    4013  then  &newvarname =     40    ;
else  if          4040  <=    &sic  <=    4049  then  &newvarname =     40    ;
else  if          4100  <=    &sic  <=    4100  then  &newvarname =     40    ;
else  if          4110  <=    &sic  <=    4119  then  &newvarname =     40    ;
else  if          4120  <=    &sic  <=    4121  then  &newvarname =     40    ;
else  if          4130  <=    &sic  <=    4131  then  &newvarname =     40    ;
else  if          4140  <=    &sic  <=    4142  then  &newvarname =     40    ;
else  if          4150  <=    &sic  <=    4151  then  &newvarname =     40    ;
else  if          4170  <=    &sic  <=    4173  then  &newvarname =     40    ;
else  if          4190  <=    &sic  <=    4199  then  &newvarname =     40    ;
else  if          4200  <=    &sic  <=    4200  then  &newvarname =     40    ;
else  if          4210  <=    &sic  <=    4219  then  &newvarname =     40    ;
else  if          4230  <=    &sic  <=    4231  then  &newvarname =     40    ;
else  if          4240  <=    &sic  <=    4249  then  &newvarname =     40    ;
else  if          4400  <=    &sic  <=    4499  then  &newvarname =     40    ;
else  if          4500  <=    &sic  <=    4599  then  &newvarname =     40    ;
else  if          4600  <=    &sic  <=    4699  then  &newvarname =     40    ;
else  if          4700  <=    &sic  <=    4700  then  &newvarname =     40    ;
else  if          4710  <=    &sic  <=    4712  then  &newvarname =     40    ;
else  if          4720  <=    &sic  <=    4729  then  &newvarname =     40    ;
else  if          4730  <=    &sic  <=    4739  then  &newvarname =     40    ;
else  if          4740  <=    &sic  <=    4749  then  &newvarname =     40    ;
else  if          4780  <=    &sic  <=    4780  then  &newvarname =     40    ;
else  if          4782  <=    &sic  <=    4782  then  &newvarname =     40    ;
else  if          4783  <=    &sic  <=    4783  then  &newvarname =     40    ;
else  if          4784  <=    &sic  <=    4784  then  &newvarname =     40    ;
else  if          4785  <=    &sic  <=    4785  then  &newvarname =     40    ;
else  if          4789  <=    &sic  <=    4789  then  &newvarname =     40    ;
else  if          5000  <=    &sic  <=    5000  then  &newvarname =     41    ;
else  if          5010  <=    &sic  <=    5015  then  &newvarname =     41    ;
else  if          5020  <=    &sic  <=    5023  then  &newvarname =     41    ;
else  if          5030  <=    &sic  <=    5039  then  &newvarname =     41    ;
else  if          5040  <=    &sic  <=    5042  then  &newvarname =     41    ;
else  if          5043  <=    &sic  <=    5043  then  &newvarname =     41    ;
else  if          5044  <=    &sic  <=    5044  then  &newvarname =     41    ;
else  if          5045  <=    &sic  <=    5045  then  &newvarname =     41    ;
else  if          5046  <=    &sic  <=    5046  then  &newvarname =     41    ;
else  if          5047  <=    &sic  <=    5047  then  &newvarname =     41    ;
else  if          5048  <=    &sic  <=    5048  then  &newvarname =     41    ;
else  if          5049  <=    &sic  <=    5049  then  &newvarname =     41    ;
else  if          5050  <=    &sic  <=    5059  then  &newvarname =     41    ;
else  if          5060  <=    &sic  <=    5060  then  &newvarname =     41    ;
else  if          5063  <=    &sic  <=    5063  then  &newvarname =     41    ;
else  if          5064  <=    &sic  <=    5064  then  &newvarname =     41    ;
else  if          5065  <=    &sic  <=    5065  then  &newvarname =     41    ;
else  if          5070  <=    &sic  <=    5078  then  &newvarname =     41    ;
else  if          5080  <=    &sic  <=    5080  then  &newvarname =     41    ;
else  if          5081  <=    &sic  <=    5081  then  &newvarname =     41    ;
else  if          5082  <=    &sic  <=    5082  then  &newvarname =     41    ;
else  if          5083  <=    &sic  <=    5083  then  &newvarname =     41    ;
else  if          5084  <=    &sic  <=    5084  then  &newvarname =     41    ;
else  if          5085  <=    &sic  <=    5085  then  &newvarname =     41    ;
else  if          5086  <=    &sic  <=    5087  then  &newvarname =     41    ;
else  if          5088  <=    &sic  <=    5088  then  &newvarname =     41    ;
else  if          5090  <=    &sic  <=    5090  then  &newvarname =     41    ;
else  if          5091  <=    &sic  <=    5092  then  &newvarname =     41    ;
else  if          5093  <=    &sic  <=    5093  then  &newvarname =     41    ;
else  if          5094  <=    &sic  <=    5094  then  &newvarname =     41    ;
else  if          5099  <=    &sic  <=    5099  then  &newvarname =     41    ;
else  if          5100  <=    &sic  <=    5100  then  &newvarname =     41    ;
else  if          5110  <=    &sic  <=    5113  then  &newvarname =     41    ;
else  if          5120  <=    &sic  <=    5122  then  &newvarname =     41    ;
else  if          5130  <=    &sic  <=    5139  then  &newvarname =     41    ;
else  if          5140  <=    &sic  <=    5149  then  &newvarname =     41    ;
else  if          5150  <=    &sic  <=    5159  then  &newvarname =     41    ;
else  if          5160  <=    &sic  <=    5169  then  &newvarname =     41    ;
else  if          5170  <=    &sic  <=    5172  then  &newvarname =     41    ;
else  if          5180  <=    &sic  <=    5182  then  &newvarname =     41    ;
else  if          5190  <=    &sic  <=    5199  then  &newvarname =     41    ;
else  if          5200  <=    &sic  <=    5200  then  &newvarname =     42    ;
else  if          5210  <=    &sic  <=    5219  then  &newvarname =     42    ;
else  if          5220  <=    &sic  <=    5229  then  &newvarname =     42    ;
else  if          5230  <=    &sic  <=    5231  then  &newvarname =     42    ;
else  if          5250  <=    &sic  <=    5251  then  &newvarname =     42    ;
else  if          5260  <=    &sic  <=    5261  then  &newvarname =     42    ;
else  if          5270  <=    &sic  <=    5271  then  &newvarname =     42    ;
else  if          5300  <=    &sic  <=    5300  then  &newvarname =     42    ;
else  if          5310  <=    &sic  <=    5311  then  &newvarname =     42    ;
else  if          5320  <=    &sic  <=    5320  then  &newvarname =     42    ;
else  if          5330  <=    &sic  <=    5331  then  &newvarname =     42    ;
else  if          5334  <=    &sic  <=    5334  then  &newvarname =     42    ;
else  if          5340  <=    &sic  <=    5349  then  &newvarname =     42    ;
else  if          5390  <=    &sic  <=    5399  then  &newvarname =     42    ;
else  if          5400  <=    &sic  <=    5400  then  &newvarname =     42    ;
else  if          5410  <=    &sic  <=    5411  then  &newvarname =     42    ;
else  if          5412  <=    &sic  <=    5412  then  &newvarname =     42    ;
else  if          5420  <=    &sic  <=    5429  then  &newvarname =     42    ;
else  if          5430  <=    &sic  <=    5439  then  &newvarname =     42    ;
else  if          5440  <=    &sic  <=    5449  then  &newvarname =     42    ;
else  if          5450  <=    &sic  <=    5459  then  &newvarname =     42    ;
else  if          5460  <=    &sic  <=    5469  then  &newvarname =     42    ;
else  if          5490  <=    &sic  <=    5499  then  &newvarname =     42    ;
else  if          5500  <=    &sic  <=    5500  then  &newvarname =     42    ;
else  if          5510  <=    &sic  <=    5529  then  &newvarname =     42    ;
else  if          5530  <=    &sic  <=    5539  then  &newvarname =     42    ;
else  if          5540  <=    &sic  <=    5549  then  &newvarname =     42    ;
else  if          5550  <=    &sic  <=    5559  then  &newvarname =     42    ;
else  if          5560  <=    &sic  <=    5569  then  &newvarname =     42    ;
else  if          5570  <=    &sic  <=    5579  then  &newvarname =     42    ;
else  if          5590  <=    &sic  <=    5599  then  &newvarname =     42    ;
else  if          5600  <=    &sic  <=    5699  then  &newvarname =     42    ;
else  if          5700  <=    &sic  <=    5700  then  &newvarname =     42    ;
else  if          5710  <=    &sic  <=    5719  then  &newvarname =     42    ;
else  if          5720  <=    &sic  <=    5722  then  &newvarname =     42    ;
else  if          5730  <=    &sic  <=    5733  then  &newvarname =     42    ;
else  if          5734  <=    &sic  <=    5734  then  &newvarname =     42    ;
else  if          5735  <=    &sic  <=    5735  then  &newvarname =     42    ;
else  if          5736  <=    &sic  <=    5736  then  &newvarname =     42    ;
else  if          5750  <=    &sic  <=    5799  then  &newvarname =     42    ;
else  if          5900  <=    &sic  <=    5900  then  &newvarname =     42    ;
else  if          5910  <=    &sic  <=    5912  then  &newvarname =     42    ;
else  if          5920  <=    &sic  <=    5929  then  &newvarname =     42    ;
else  if          5930  <=    &sic  <=    5932  then  &newvarname =     42    ;
else  if          5940  <=    &sic  <=    5940  then  &newvarname =     42    ;
else  if          5941  <=    &sic  <=    5941  then  &newvarname =     42    ;
else  if          5942  <=    &sic  <=    5942  then  &newvarname =     42    ;
else  if          5943  <=    &sic  <=    5943  then  &newvarname =     42    ;
else  if          5944  <=    &sic  <=    5944  then  &newvarname =     42    ;
else  if          5945  <=    &sic  <=    5945  then  &newvarname =     42    ;
else  if          5946  <=    &sic  <=    5946  then  &newvarname =     42    ;
else  if          5947  <=    &sic  <=    5947  then  &newvarname =     42    ;
else  if          5948  <=    &sic  <=    5948  then  &newvarname =     42    ;
else  if          5949  <=    &sic  <=    5949  then  &newvarname =     42    ;
else  if          5950  <=    &sic  <=    5959  then  &newvarname =     42    ;
else  if          5960  <=    &sic  <=    5969  then  &newvarname =     42    ;
else  if          5970  <=    &sic  <=    5979  then  &newvarname =     42    ;
else  if          5980  <=    &sic  <=    5989  then  &newvarname =     42    ;
else  if          5990  <=    &sic  <=    5990  then  &newvarname =     42    ;
else  if          5992  <=    &sic  <=    5992  then  &newvarname =     42    ;
else  if          5993  <=    &sic  <=    5993  then  &newvarname =     42    ;
else  if          5994  <=    &sic  <=    5994  then  &newvarname =     42    ;
else  if          5995  <=    &sic  <=    5995  then  &newvarname =     42    ;
else  if          5999  <=    &sic  <=    5999  then  &newvarname =     42    ;
else  if          5800  <=    &sic  <=    5819  then  &newvarname =     43    ;
else  if          5820  <=    &sic  <=    5829  then  &newvarname =     43    ;
else  if          5890  <=    &sic  <=    5899  then  &newvarname =     43    ;
else  if          7000  <=    &sic  <=    7000  then  &newvarname =     43    ;
else  if          7010  <=    &sic  <=    7019  then  &newvarname =     43    ;
else  if          7040  <=    &sic  <=    7049  then  &newvarname =     43    ;
else  if          7213  <=    &sic  <=    7213  then  &newvarname =     43    ;
else  if          6000  <=    &sic  <=    6000  then  &newvarname =     44    ;
else  if          6010  <=    &sic  <=    6019  then  &newvarname =     44    ;
else  if          6020  <=    &sic  <=    6020  then  &newvarname =     44    ;
else  if          6021  <=    &sic  <=    6021  then  &newvarname =     44    ;
else  if          6022  <=    &sic  <=    6022  then  &newvarname =     44    ;
else  if          6023  <=    &sic  <=    6024  then  &newvarname =     44    ;
else  if          6025  <=    &sic  <=    6025  then  &newvarname =     44    ;
else  if          6026  <=    &sic  <=    6026  then  &newvarname =     44    ;
else  if          6027  <=    &sic  <=    6027  then  &newvarname =     44    ;
else  if          6028  <=    &sic  <=    6029  then  &newvarname =     44    ;
else  if          6030  <=    &sic  <=    6036  then  &newvarname =     44    ;
else  if          6040  <=    &sic  <=    6059  then  &newvarname =     44    ;
else  if          6060  <=    &sic  <=    6062  then  &newvarname =     44    ;
else  if          6080  <=    &sic  <=    6082  then  &newvarname =     44    ;
else  if          6090  <=    &sic  <=    6099  then  &newvarname =     44    ;
else  if          6100  <=    &sic  <=    6100  then  &newvarname =     44    ;
else  if          6110  <=    &sic  <=    6111  then  &newvarname =     44    ;
else  if          6112  <=    &sic  <=    6113  then  &newvarname =     44    ;
else  if          6120  <=    &sic  <=    6129  then  &newvarname =     44    ;
else  if          6130  <=    &sic  <=    6139  then  &newvarname =     44    ;
else  if          6140  <=    &sic  <=    6149  then  &newvarname =     44    ;
else  if          6150  <=    &sic  <=    6159  then  &newvarname =     44    ;
else  if          6160  <=    &sic  <=    6169  then  &newvarname =     44    ;
else  if          6170  <=    &sic  <=    6179  then  &newvarname =     44    ;
else  if          6190  <=    &sic  <=    6199  then  &newvarname =     44    ;
else  if          6300  <=    &sic  <=    6300  then  &newvarname =     45    ;
else  if          6310  <=    &sic  <=    6319  then  &newvarname =     45    ;
else  if          6320  <=    &sic  <=    6329  then  &newvarname =     45    ;
else  if          6330  <=    &sic  <=    6331  then  &newvarname =     45    ;
else  if          6350  <=    &sic  <=    6351  then  &newvarname =     45    ;
else  if          6360  <=    &sic  <=    6361  then  &newvarname =     45    ;
else  if          6370  <=    &sic  <=    6379  then  &newvarname =     45    ;
else  if          6390  <=    &sic  <=    6399  then  &newvarname =     45    ;
else  if          6400  <=    &sic  <=    6411  then  &newvarname =     45    ;
else  if          6500  <=    &sic  <=    6500  then  &newvarname =     46    ;
else  if          6510  <=    &sic  <=    6510  then  &newvarname =     46    ;
else  if          6512  <=    &sic  <=    6512  then  &newvarname =     46    ;
else  if          6513  <=    &sic  <=    6513  then  &newvarname =     46    ;
else  if          6514  <=    &sic  <=    6514  then  &newvarname =     46    ;
else  if          6515  <=    &sic  <=    6515  then  &newvarname =     46    ;
else  if          6517  <=    &sic  <=    6519  then  &newvarname =     46    ;
else  if          6520  <=    &sic  <=    6529  then  &newvarname =     46    ;
else  if          6530  <=    &sic  <=    6531  then  &newvarname =     46    ;
else  if          6532  <=    &sic  <=    6532  then  &newvarname =     46    ;
else  if          6540  <=    &sic  <=    6541  then  &newvarname =     46    ;
else  if          6550  <=    &sic  <=    6553  then  &newvarname =     46    ;
else  if          6590  <=    &sic  <=    6599  then  &newvarname =     46    ;
else  if          6610  <=    &sic  <=    6611  then  &newvarname =     46    ;
else  if          6200  <=    &sic  <=    6299  then  &newvarname =     47    ;
else  if          6700  <=    &sic  <=    6700  then  &newvarname =     47    ;
else  if          6710  <=    &sic  <=    6719  then  &newvarname =     47    ;
else  if          6720  <=    &sic  <=    6722  then  &newvarname =     47    ;
else  if          6723  <=    &sic  <=    6723  then  &newvarname =     47    ;
else  if          6724  <=    &sic  <=    6724  then  &newvarname =     47    ;
else  if          6725  <=    &sic  <=    6725  then  &newvarname =     47    ;
else  if          6726  <=    &sic  <=    6726  then  &newvarname =     47    ;
else  if          6730  <=    &sic  <=    6733  then  &newvarname =     47    ;
else  if          6740  <=    &sic  <=    6779  then  &newvarname =     47    ;
else  if          6790  <=    &sic  <=    6791  then  &newvarname =     47    ;
else  if          6792  <=    &sic  <=    6792  then  &newvarname =     47    ;
else  if          6793  <=    &sic  <=    6793  then  &newvarname =     47    ;
else  if          6794  <=    &sic  <=    6794  then  &newvarname =     47    ;
else  if          6795  <=    &sic  <=    6795  then  &newvarname =     47    ;
else  if          6798  <=    &sic  <=    6798  then  &newvarname =     47    ;
else  if          6799  <=    &sic  <=    6799  then  &newvarname =     47    ;
else  if          4950  <=    &sic  <=    4959  then  &newvarname =     48    ;
else  if          4960  <=    &sic  <=    4961  then  &newvarname =     48    ;
else  if          4970  <=    &sic  <=    4971  then  &newvarname =     48    ;
else  if          4990  <=    &sic  <=    4991  then  &newvarname =     48    ;
else &newvarname =30;
run;
 
 
%mend;
 
 
***************************************************************************************;
***************************************************************************************;
***************************************************************************************;
*CREATES INDUSTRY CLASSIFICATIONS USING TWO DIGIT SIC CODES
***************************************************************************************;
***************************************************************************************;
***************************************************************************************;
%macro ind22(data=,newvarname=,sic=sic,out=&data);
 
data &out;
    set &data;
    &newvarname = 0;
    if &sic ge 1000 and &sic le 1999 then &newvarname = 1;
    if &sic ge 2000 and &sic le 2111 then &newvarname = 2;
    if &sic ge 2200 and &sic le 2780 then &newvarname = 3;
    if &sic ge 2800 and &sic le 2824 then &newvarname = 4;
    if &sic ge 2840 and &sic le 2899 then &newvarname = 4;
    if &sic ge 2830 and &sic le 2836 then &newvarname = 5;
    if &sic ge 2900 and &sic le 2999 then &newvarname = 6;
    if &sic ge 1300 and &sic le 1399 then &newvarname = 6;
    if &sic ge 3000 and &sic le 3299 then &newvarname = 7;
    if &sic ge 3300 and &sic le 3499 then &newvarname = 8;
    if &sic ge 3500 and &sic le 3599 then &newvarname = 9;
    if &sic ge 3600 and &sic le 3699 then &newvarname = 10;
    if &sic ge 3700 and &sic le 3799 then &newvarname = 11;
    if &sic ge 3800 and &sic le 3899 then &newvarname = 12;
    if &sic ge 3900 and &sic le 3999 then &newvarname = 13;
    if &sic ge 3570 and &sic le 3579 then &newvarname = 14;
    if &sic ge 3670 and &sic le 3679 then &newvarname = 14;
    if &sic ge 4000 and &sic le 4899 then &newvarname = 15;
    if &sic ge 4900 and &sic le 4999 then &newvarname = 16;
    if &sic ge 5000 and &sic le 5199 then &newvarname = 17;
    if &sic ge 5200 and &sic le 5999 then &newvarname = 18;
    if &sic ge 5800 and &sic le 5899 then &newvarname = 19;
    if &sic ge 6000 and &sic le 6411 then &newvarname = 20;
    if &sic ge 6500 and &sic le 6999 then &newvarname = 21;
    if &sic ge 7000 and &sic le 8999 then &newvarname = 22;
    if &sic ge 7370 and &sic le 7379 then &newvarname = 14;
    run;
 
proc format;
    value indfmt
         0='Not assigned'
         1='Mining/Construction'
         2='Food'
         3='Textiles/Print/Publish'
         4='Chemicals'
         5='Pharmaceuticals'
         6='Extractive'
         7='Manf:Rubber/glass/etc'
         8='Manf:Metal'
         9='Manf:Machinery'
        10='Manf:ElectricalEqpt'
        11='Manf:TransportEqpt'
        12='Manf:Instruments'
        13='Manf:Misc.'
        14='Computers'
        15='Transportation'
        16='Utilities'
        17='Retail:Wholesale'
        18='Retail:Misc.'
        19='Retail:Restaurant'
        20='Financial'
        21='Insurance/RealEstate'
        22='Services';
    run;
 
%mend;


/************************************************
This creates dummy variables for all values of 
the variable requested. This macro is especially 
useful for doing fixed effects when a CLASS 
statement cannot be used.

dset = dataset
var = the variable for which to create dummies

Use class when the variable in question
is a numeric variable.

Use class_char when the variable in question
is a character variable.
*************************************************/

%macro class (dset=,var=);


proc sql noprint;
select count(distinct &var.) into :last
	from &dset.
	where &var. is not missing;

%let last=&last;

select distinct &var. into :sic1-:sic&last
	from &dset.
	where &var. is not missing;

quit;

data &dset.;
	set &dset.;
	%do i=1 %to &last;
		if &var.=&&sic&i then &var._&&sic&i=1;
		else &var._&&sic&i=0;
	%end;
run;

%mend class;

/************************************************
This creates dummy variables for all values of 
the variable requested. This macro is especially 
useful for doing fixed effects when a CLASS 
statement cannot be used.

dset = dataset
var = the variable for which to create dummies

Use class when the variable in question
is a numeric variable.

Use class_char when the variable in question
is a character variable.
*************************************************/

%macro class_char (dset=,var=);


proc sql noprint;
select count(distinct &var.) into :last
	from &dset.
	where &var. is not missing;

%let last=&last;

select distinct &var. into :sic1-:sic&last
	from &dset.
	where &var. is not missing;

quit;

data &dset.;
	set &dset.;
	%do i=1 %to &last;
		if &var.="&&sic&i" then &var._&&sic&i=1;
		else &var._&&sic&i=0;
	%end;
run;

%mend class_char;


************************************************
Transpose macro

************************************************;
%macro matchtarget(dsetin=, dsetout=, dsetbase=);

***Output duplicates;
%dupl(dset=&dsetin, keys=gvkey datadate target);

proc sql;
create table COV_Data as select distinct a.*, b.packageID as InDupe 
from &dsetin a left join duplicates b on a.gvkey = b.gvkey and a.datadate=b.datadate and a.target=b.target
and a.packageid=b.packageId;
run; quit;

data COV_Data;
set COV_Data;
if InDupe~=. then delete;
drop InDupe;
run; quit;
 
***Keep only the most recent deal and append covenant file with most recent deal;
proc sort data=duplicates;
	by  gvkey datadate target descending facilitystartdate;
run; quit;

data duplicates;
	set duplicates;
	by  gvkey datadate target descending facilitystartdate;
	if not first.target then delete;
run; quit;

proc append data=duplicates base=COV_Data force;
run; quit;

data &dsetout;
set COV_Data;
run; quit;

proc append data=&dsetout base=&dsetbase force;
run; quit;

%mend matchtarget;

***Without Covenants;
%macro matchtarget2(dsetin=, dsetout=, dsetbase=);

%dupl(dset=&dsetin, keys=gvkey datadate target);


proc sql;
create table COV_Data as select distinct a.*, b.packageID as InDupe 
from COV a left join duplicates b on a.gvkey = b.gvkey and a.datadate=b.datadate
and a.packageid=b.packageId;
run; quit;

data COV_Data;
set COV_Data;
if InDupe~=. then delete;
drop InDupe;
run; quit;
 
***Keep only the most recent deal and append covenant file with most recent deal;
proc sort data=duplicates;
	by  gvkey datadate covenant descending facilitystartdate;
run; quit;

data duplicates;
	set duplicates;
	by  gvkey datadate covenant descending FacilityStartDate;
	if not first.covenant then delete;
run; quit;


proc append data=duplicates base=COV_Data force;
run; quit;

data &dsetout;
set COV_Data;
run; quit;

proc append data=&dsetout base=&dsetbase force;
run; quit;

%mend matchtarget2;



/****************************************************************************************/
/* PROGRAM:       better_means                                                          */
/* AUTHORS:       Myra A. Oltsik and Peter Crawford                                     */
/* ORIGINAL DATE: 04/20/05                                                              */
/* PURPOSE:       Create a dataset with PROC MEANS statistics, with each record being   */
/*                one variable. Can print stats, too. Fixes ODS problems.               */
/*                                                                                      */
/* CHANGE HISTORY:                                                                      */
/* 04Jan2006   PC: use a  &testing  parm to preserve _better_: data sets                */
/* 01Feb2006   xx: clean for production                                                 */
/* 07Jul2006   xx: add out= parameter  (defaults to use current scheme)                 */
/*                                                                                      */
/* NOTE:          This macro has special handling for N, SUMWGT, KURT and SKEW.         */
/*                Also:   STDEV, Q1, MEDIAN, Q3 are referred as STD, P25, P50, P75.     */
/****************************************************************************************/
/****************************************************************************************/
/* MACRO PARAMETERS:                                                                    */
/*    required:  none                                                                   */
/*    optional:  print   -- whether or not to print results to output                   */
/*               data    -- dataset name to be analysed                                 */
/*               out     -- output dataset (default is &data.means                      */
/*                             but source not always update-able                        */
/*               where   -- where statement                                             */
/*               keepdata -- decide whether to keep an output dataset                   */
/*               sort    -- sort order choice of the file of MEANS, by VARNUM or NAME   */
/*               stts    -- indicate which statistics should included in the output     */
/*               varlst  -- list of variables for means if not all numeric vars in file */
/*               clss    -- variable(s) for a class statement                           */
/*               wghts   -- variable for a weight statement                             */
/*    defaults:                                                                         */
/*               data    -- &syslast  (most recently created data set)                  */
/*               print   -- Y                                                           */
/*               sort    -- VARNUM                                                      */
/*               stts    -- _ALL_                                                       */
/*               varlst  -- _ALL_                                                       */
/*               keepdata -- N                                                          */
/*                                                                                      */
/* Created Macro Variables:                                                             */
/*               locals  --  see inline comments at %local statement                    */
/* Creates Data Sets                                                                    */
/*               results are written to &data._means                                    */
/*               many data sets are created in the work library  all prefixed _better_  */
/*               but unless the testing option is set, the work data stes are deleted   */
/*                                                                                      */
/* SAMPLES:                                                                             */
/*   %better_means(data=test); print all default statistics in a dataset                */
/*   %better_means(data=sashelp.class,stts=MEAN SUM); print only MEAN and SUM stats     */
/*   %better_means(data=sashelp.gnp,print=N,sort=NAME,stts=MIN MAX,varlst=INVEST        */
/*      EXPORTS); suppress list printing, limit output statistics and variables, and    */
/*      sort on NAME                                                                    */
/*   %better_means(data=sasuser.shoes,clss=PRODUCT); run all stats by PRODUCT field     */
/*   %better_means(data=sasuser.weighted,wghts=WGT); run all stats weighted on WGT      */
/****************************************************************************************/
%macro
   better_means(
      data   = &syslast ,
	  keepdata  = N,
      out    =   ,
	  where = ,
      print  = Y,
      sort   = VARNUM,
      stts   = _ALL_,
      varlst = _ALL_,
      clss   =  ,
      wghts  =  ,
      testing= no ,  /* any other value will preserve the _better_: data sets */
/****************************************************************************************/
/* PROVIDE THE COMPLETE PROC MEANS STATISTIC LIST (FROM ONLINE-DOC) IF NONE STATED.     */
/****************************************************************************************/
      _stts  = N MEAN STD MIN MAX CSS CV LCLM NMISS
               P1 P5 P10 P25 P50 P75 P90 P95 P99 QRANGE RANGE
               PROBT STDERR SUM SUMWGT KURT SKEW T UCLM USS VAR
     );
   %local
      vLexist                          /* EXISTENCE OF LABELS ON INPUT DATASET          */
      s                                /* POINTER TO STATISTIC IN THE STATISTICS LIST   */
      stato                            /* HOLDER OF AN INDIVIDUAL STATISTIC NAME :-
                                          USED IN STATISTIC TABLE NAME, AND
                                          USED IN THE IN= VARIABLE DATASET OPTION       */
      full                             /* INDICATOR IN OUTPUT LABEL WHEN ALL STATS USED.*/
   ;
/****************************************************************************************/
/* PUT STATS AND VAR PARAMETER LIST INTO UPPER CASE.                                    */
/****************************************************************************************/
   %let varlst = %upcase(&varlst);
   %let stts   = %upcase(&stts);
   %let data   = &data ;                            /* RESOLVE &syslast, WHEN DEFAULTED */
                                                    /* provide default OUT= dataset     */
   %if %length(&out) < 1 %then %do;
       %let out = &data._means ;
   %end ; 
/****************************************************************************************/
/* GET THE NAMES/NUMBERS OF ALL VARIABLES INTO A LOOKUP FORMAT IF SORT ORDER = VARNUM.  */
/****************************************************************************************/
   %if &sort eq VARNUM %then %do;
      proc contents data= &data out= _BETTER_cols noprint;
      run;
      data _BETTER_cntl;
         retain
            FMTNAME '_bm_VN'
            TYPE    'I'
            HLO     'U'
         ;
         set _BETTER_cols( keep= NAME VARNUM  rename=( VARNUM=LABEL ));
         START = upcase( NAME) ;
      run;
      proc format cntlin= _BETTER_cntl;
      run;
   %end;
/****************************************************************************************/
/* PROCESS STATISTICS CONDITIONS / COMBINATIONS                                         */
/****************************************************************************************/
   %if &stts = _ALL_ or %length(&stts) = 0 %then %do;
      %let stts = &_stts ;
      %let full = FULL STATS;
   %end;
   %if  %length(&wghts) %then %do;
      %* remove KURT and Skew  when weights are present;
      %let stts = %sysfunc( tranwrd( &stts, KURT, %str( ) ));
      %let stts = %sysfunc( tranwrd( &stts, SKEW, %str( ) ));
      %let full = STATS ;
   %end;
   %else %do;
      %* remove SUMWGT  when no weights present ;
      %let stts = %sysfunc( tranwrd( &stts, SUMWGT, %str( ) ));
      %let full = STATS ;
   %end;

/****************************************************************************************/
/* RUN PROC MEANS ON VARIABLES WITH OUTPUT FILE FOR EACH STATISTIC REQUESTED. MERGE     */
/* DATASET OF LIST OF NUMERIC VARIABLES AND THEIR VARNUM.                               */
/****************************************************************************************/
   proc means data= &data noprint missing;
      %if &where ne %then %do;
	  where &where;
	  %end;
      %if &varlst ne _ALL_ & %length(&varlst) %then %do;
         var   &varlst;
      %end;
      %if %length(&clss) %then %do;
         class &clss;
      %end;
      %if %length(&wghts) %then %do;
         weight &wghts;
      %end;
      %let s     =               1  ;
      %let stato = %scan( &stts, 1 );
      %do %while( %length(&stato) > 0 );          /* USING %LENGTH() FOR &STATO WORDS 
                                                              SIGNIFICANT TO %IF/%WHILE */
         output out= _BETTER_&stato &stato= ;
         %let s     = %eval(        &s +1 );
         %let stato = %scan( &stts, &s    );
      %end;
   run;

   data _better_means1;
      length
         _BETTER_ $32.                            /* STATS IDENTITY */
      ;
      set    /* all those output datasets from proc means */
      %let    stato = %scan( &stts, 1 );
      %let    s     =                1  ;
      %do %while(    %length(&stato) gt  0 );
         _BETTER_&stato( in= _in_&stato )         /* NEED IN= VARIABLE TO IDENTIFY INPUT 
                                                                                   DATA */
         %let s     = %eval(        &s +1 );
         %let stato = %scan( &stts, &s    );
      %end;
      ;
      by _TYPE_ &clss;
      %let    stato = %scan( &stts, 1 );          /* GENERATE _BETTER_ TO IDENTIFY EACH 
                                                                         ROW OF RESULTS */
      %let    s     =               1  ;
      %do %while( %length(&stato) > 0 );
         if _in_&stato then _BETTER_ = "%upcase( &stato )" ; else
         %let s     = %eval(        &s +1 );
         %let stato = %scan( &stts, &s    );
      %end;
      ;
   run;

   proc transpose data=_better_means1  out=_better_means2;
      by _TYPE_ &clss ;
      id _BETTER_ ;
   run;
/****************************************************************************************/
/* FROM SAS FAQ # 1806: MACRO TO CHECK IF THE VARIABLE EXISTS IN A DATASET.             */
/****************************************************************************************/
   %macro varcheck(varname,dsname);
      %local dsid vindex rc;
      %let dsid = %sysfunc(open(&dsname,is));

      %if &dsid EQ 0 %then %do;
         %put ERROR: (varcheck) The data set "&dsname" could not be found;
      %end;
      %else %do;
         %let vindex = %sysfunc(varnum(&dsid,&varname));
      %end;

      %let rc = %sysfunc(close(&dsid));
      &vindex
   %mend varcheck;

   %let vLexist = %varcheck(_LABEL_,_better_means2);
/****************************************************************************************/
/* CREATE BASIS FOR OUTPUT DATASET BASED ON DIFFERENT CONDITIONS AND PARAMETER CHOICES. */
/****************************************************************************************/
   %macro inL( list, seek )/ des= "Return TRUE, if &seek in &list, blank delimited";
       %sysfunc( indexw( &list, &seek ))
   %mend inL ;
   %macro now( fmt= datetime21.2 ) / des= "Timestamp";
       %sysfunc( datetime(), &fmt )
   %mend  now;

   data _better_means_out;
      length
         _TYPE_ 3.      ;
      retain                                      /* TO FIX ORDER OF THE FIRST FEW */
         &clss
         %if &sort eq VARNUM %then %do;
            VARNUM
         %end;
         NAME
         %if &vLexist ne 0 %then %do;             /* ADD IF TRANSPOSED DATASET CONTAINS 
                                                                     THE LABEL VARIABLE */
            LABEL
         %end;
         %if %inL(&stts,N) %then %do;             /* ADD % NOT MISSING IF STATISTIC "N" 
                                                                              REQUESTED */
            N
            PCT_POP
            PCT_DEN
         %end;
      ;
      set _better_means2(rename=(
         _NAME_  = NAME
         %if &vLexist ne 0 %then %do;
            _LABEL_ = LABEL
         %end;
      ));
      %if %inL(&stts,N) %then %do;
         format
            PCT_POP percent.4
         ;
         if NAME = "_FREQ_" then do;
            PCT_DEN = N ;
            delete;
         end;
         else do;
            if PCT_DEN then PCT_POP = N / pct_den ;
         end;
         drop
            PCT_DEN
         ;
      %end;
      %else %do;
         if NAME = "_FREQ_" then delete;
      %end;
      %if &sort eq VARNUM %then %do;
         VARNUM = input(NAME,_bm_VN.);
      %end;
      NAMEU = upcase(NAME) ;
   run;

/****************************************************************************************/
/* CREATE FINAL DATASET WITH ALL STATISTICS, SORTED AS REQUESTED ON INVOCATION.         */
/****************************************************************************************/
   proc sort data= _better_means_out
              out= &out(              label= "&FULL FOR &data %NOW"
                                       drop= NAMEU
      %if %length(&clss) = 0 %then %do;
                                             _TYPE_
      %end;
                                                     )      ;
         by _TYPE_ &sort &clss ;
   run;

/****************************************************************************************/
/* IF PRINTED OUTPUT IS REQUESTED, DO SO HERE.                                          */
/****************************************************************************************/
   %if &print = Y %then %do;
      proc print data= &out ;
         %if %length(&clss) > 0 %then %do;
            by _TYPE_;
         %end;
      run;
   %end;

   %if &testing = no  %then %do;
/****************************************************************************************/
/* CLEAN UP REMAINING TEMPORARY DATASETS.                                               */
/****************************************************************************************/
      proc datasets lib= work nolist;
         delete    _BETTER_:        ;
      run; quit;
   %end;

%if &KEEPDATA = N %then %do;
proc sql;
   drop table &out;
   quit;
%end;
%mend  BETTER_MEANS ;


****************************************************************************************
*Fama French Industry classification;

***************************************************************************************
This macro creates the 48 Fama-French industries per Fama and French. 

Inputs: 
dsetin - input dataset
dsetout - dataset for output
sic - Column that specifies sic codes

***************************************************************************************
;

%macro FF_Industry(dsetin=, dsetout=, sic=);

********** FF Ind Codes Macro **********;



data &dsetout;
        set &dsetin;
	

* 1 Agric  Agriculture;
          if &sic ge 0100 and &sic le 0199  then FF_IND=1;
          if &sic ge 0200 and &sic le 0299  then FF_IND=1;
          if &sic ge 0700 and &sic le 0799  then FF_IND=1;
          if &sic ge 0910 and &sic le 0919  then FF_IND=1;
          if &sic ge 2048 and &sic le 2048  then FF_IND=1;
				


* 2 Food   Food Products;
          if &sic ge 2000 and &sic le 2009  then FF_IND=2;
          if &sic ge 2010 and &sic le 2019  then FF_IND=2;
          if &sic ge 2020 and &sic le 2029  then FF_IND=2;
          if &sic ge 2030 and &sic le 2039  then FF_IND=2;
          if &sic ge 2040 and &sic le 2046  then FF_IND=2;
          if &sic ge 2050 and &sic le 2059  then FF_IND=2;
          if &sic ge 2060 and &sic le 2063  then FF_IND=2;
          if &sic ge 2070 and &sic le 2079  then FF_IND=2;
          if &sic ge 2090 and &sic le 2092  then FF_IND=2;
          if &sic ge 2095 and &sic le 2095  then FF_IND=2;
          if &sic ge 2098 and &sic le 2099  then FF_IND=2;


* 3 Soda   Candy & Soda;
          if &sic ge 2064 and &sic le 2068  then FF_IND=3;
          if &sic ge 2086 and &sic le 2086  then FF_IND=3;
          if &sic ge 2087 and &sic le 2087  then FF_IND=3;
          if &sic ge 2096 and &sic le 2096  then FF_IND=3;
          if &sic ge 2097 and &sic le 2097  then FF_IND=3;


* 4 Beer   Beer & Liquor;
          if &sic ge 2080 and &sic le 2080  then FF_IND=4;
          if &sic ge 2082 and &sic le 2082  then FF_IND=4;
          if &sic ge 2083 and &sic le 2083  then FF_IND=4;
          if &sic ge 2084 and &sic le 2084  then FF_IND=4;
          if &sic ge 2085 and &sic le 2085  then FF_IND=4; 


* 5 Smoke  Tobacco Products;
          if &sic ge 2100 and &sic le 2199  then FF_IND=5; 


* 6 Toys   Recreation;
          if &sic ge 0920 and &sic le 0999  then FF_IND=6; 
          if &sic ge 3650 and &sic le 3651  then FF_IND=6; 
          if &sic ge 3652 and &sic le 3652  then FF_IND=6;
          if &sic ge 3732 and &sic le 3732   then FF_IND=6;
          if &sic ge 3930 and &sic le 3931   then FF_IND=6;
          if &sic ge 3940 and &sic le 3949   then FF_IND=6;


* 7 Fun    Entertainment;
          if &sic ge 7800 and &sic le 7829   then FF_IND=7;
          if &sic ge  7830 and &sic le 7833   then FF_IND=7;
          if &sic ge 7840 and &sic le 7841   then FF_IND=7;
          if &sic ge 7900 and &sic le 7900   then FF_IND=7;
          if &sic ge 7910 and &sic le 7911   then FF_IND=7;
          if &sic ge 7920 and &sic le 7929   then FF_IND=7;
          if &sic ge 7930 and &sic le 7933   then FF_IND=7;
          if &sic ge 7940 and &sic le 7949   then FF_IND=7;
          if &sic ge 7980 and &sic le 7980   then FF_IND=7;
          if &sic ge 7990 and &sic le 7999   then FF_IND=7;


* 8 Books  Printing and Publishing;
          if &sic ge 2700 and &sic le 2709   then FF_IND=8;
          if &sic ge 2710 and &sic le 2719   then FF_IND=8;
          if &sic ge 2720 and &sic le 2729   then FF_IND=8;
          if &sic ge 2730 and &sic le 2739   then FF_IND=8;
          if &sic ge 2740 and &sic le 2749   then FF_IND=8;
          if &sic ge 2770 and &sic le 2771   then FF_IND=8;
          if &sic ge 2780 and &sic le 2789   then FF_IND=8;
          if &sic ge 2790 and &sic le 2799   then FF_IND=8;


* 9 Hshld  Consumer Goods;
          if &sic ge 2047 and &sic le 2047   then FF_IND=9;
          if &sic ge 2391 and &sic le 2392   then FF_IND=9;
          if &sic ge 2510 and &sic le 2519   then FF_IND=9;
          if &sic ge 2590 and &sic le 2599   then FF_IND=9;
          if &sic ge 2840 and &sic le 2843   then FF_IND=9;
          if &sic ge 2844 and &sic le 2844   then FF_IND=9;
          if &sic ge 3160 and &sic le 3161   then FF_IND=9;
          if &sic ge 3170 and &sic le 3171   then FF_IND=9;
          if &sic ge 3172 and &sic le 3172   then FF_IND=9;
          if &sic ge 3190 and &sic le 3199   then FF_IND=9;
          if &sic ge 3229 and &sic le 3229   then FF_IND=9;
          if &sic ge 3260 and &sic le 3260   then FF_IND=9;
          if &sic ge 3262 and &sic le 3263   then FF_IND=9;
          if &sic ge 3269 and &sic le 3269   then FF_IND=9;
          if &sic ge 3230 and &sic le 3231   then FF_IND=9;
          if &sic ge 3630 and &sic le 3639   then FF_IND=9;
          if &sic ge 3750 and &sic le 3751   then FF_IND=9;
          if &sic ge 3800 and &sic le 3800   then FF_IND=9;
          if &sic ge 3860 and &sic le 3861   then FF_IND=9;
          if &sic ge 3870 and &sic le 3873   then FF_IND=9;
          if &sic ge 3910 and &sic le 3911   then FF_IND=9;
          if &sic ge 3914 and &sic le 3914   then FF_IND=9;
          if &sic ge 3915 and &sic le 3915   then FF_IND=9;
          if &sic ge 3960 and &sic le 3962   then FF_IND=9;
          if &sic ge 3991 and &sic le 3991   then FF_IND=9;
          if &sic ge 3995 and &sic le 3995   then FF_IND=9;


*10 Clths  Apparel;
          if &sic ge 2300 and &sic le 2390   then FF_IND=10;
          if &sic ge 3020 and &sic le 3021   then FF_IND=10;
          if &sic ge 3100 and &sic le 3111   then FF_IND=10;
          if &sic ge 3130 and &sic le 3131   then FF_IND=10;
          if &sic ge 3140 and &sic le 3149   then FF_IND=10;
          if &sic ge 3150 and &sic le 3151   then FF_IND=10;
          if &sic ge 3963 and &sic le 3965   then FF_IND=10;


*11 Hlth   Healthcare;
          if &sic ge 8000 and &sic le 8099   then FF_IND=11;


*12 MedEq  Medical Equipment;
          if &sic ge 3693 and &sic le 3693   then FF_IND=12;
          if &sic ge 3840 and &sic le 3849   then FF_IND=12;
          if &sic ge 3850 and &sic le 3851   then FF_IND=12;

*13 Drugs  Pharmaceutical Products;
          if &sic ge 2830 and &sic le 2830   then FF_IND=13;
          if &sic ge 2831 and &sic le 2831   then FF_IND=13;
          if &sic ge 2833 and &sic le 2833   then FF_IND=13;
          if &sic ge 2834 and &sic le 2834   then FF_IND=13;
          if &sic ge 2835 and &sic le 2835   then FF_IND=13;
          if &sic ge 2836 and &sic le 2836   then FF_IND=13;


*14 Chems  Chemicals;
          if &sic ge 2800 and &sic le 2809   then FF_IND=14;
          if &sic ge 2810 and &sic le 2819   then FF_IND=14;
          if &sic ge 2820 and &sic le 2829   then FF_IND=14;
          if &sic ge 2850 and &sic le 2859   then FF_IND=14;
          if &sic ge 2860 and &sic le 2869   then FF_IND=14;
          if &sic ge 2870 and &sic le 2879   then FF_IND=14;
          if &sic ge 2890 and &sic le 2899   then FF_IND=14;


*15 Rubbr  Rubber and Plastic Products;
          if &sic ge 3031 and &sic le 3031   then FF_IND=15;
          if &sic ge 3041 and &sic le 3041   then FF_IND=15;
          if &sic ge 3050 and &sic le 3053   then FF_IND=15;
          if &sic ge 3060 and &sic le 3069   then FF_IND=15;
          if &sic ge 3070 and &sic le 3079   then FF_IND=15;
          if &sic ge 3080 and &sic le 3089   then FF_IND=15;
          if &sic ge 3090 and &sic le 3099   then FF_IND=15;


*16 Txtls  Textiles;
          if &sic ge 2200 and &sic le 2269   then FF_IND=16;
          if &sic ge 2270 and &sic le 2279   then FF_IND=16;
          if &sic ge 2280 and &sic le 2284   then FF_IND=16;
          if &sic ge 2290 and &sic le 2295   then FF_IND=16;
          if &sic ge 2297 and &sic le 2297   then FF_IND=16;
          if &sic ge 2298 and &sic le 2298   then FF_IND=16;
          if &sic ge 2299 and &sic le 2299   then FF_IND=16;
          if &sic ge 2393 and &sic le 2395   then FF_IND=16;
          if &sic ge 2397 and &sic le 2399   then FF_IND=16;


*17 BldMt  Construction Materials;
          if &sic ge 0800 and &sic le 0899   then FF_IND=17;
          if &sic ge 2400 and &sic le 2439   then FF_IND=17;
          if &sic ge 2450 and &sic le 2459   then FF_IND=17;
          if &sic ge 2490 and &sic le 2499   then FF_IND=17;
          if &sic ge 2660 and &sic le 2661   then FF_IND=17;
          if &sic ge 2950 and &sic le 2952   then FF_IND=17;
          if &sic ge 3200 and &sic le 3200   then FF_IND=17;
          if &sic ge 3210 and &sic le 3211   then FF_IND=17;
          if &sic ge 3240 and &sic le 3241   then FF_IND=17;
          if &sic ge 3250 and &sic le 3259   then FF_IND=17;
          if &sic ge 3261 and &sic le 3261   then FF_IND=17;
          if &sic ge 3264 and &sic le 3264   then FF_IND=17;
          if &sic ge 3270 and &sic le 3275   then FF_IND=17;
          if &sic ge 3280 and &sic le 3281   then FF_IND=17;
          if &sic ge 3290 and &sic le 3293   then FF_IND=17;
          if &sic ge 3295 and &sic le 3299   then FF_IND=17;
          if &sic ge 3420 and &sic le 3429   then FF_IND=17;
          if &sic ge 3430 and &sic le 3433   then FF_IND=17;
          if &sic ge 3440 and &sic le 3441   then FF_IND=17;
          if &sic ge 3442 and &sic le 3442   then FF_IND=17;
          if &sic ge 3446 and &sic le 3446   then FF_IND=17;
          if &sic ge 3448 and &sic le 3448   then FF_IND=17;
          if &sic ge 3449 and &sic le 3449   then FF_IND=17;
          if &sic ge 3450 and &sic le 3451   then FF_IND=17;
          if &sic ge 3452 and &sic le 3452   then FF_IND=17;
          if &sic ge 3490 and &sic le 3499   then FF_IND=17;
          if &sic ge 3996 and &sic le 3996   then FF_IND=17;


*18 Cnstr  Construction;
          if &sic ge 1500 and &sic le 1511   then FF_IND=18;
          if &sic ge 1520 and &sic le 1529   then FF_IND=18;
          if &sic ge 1530 and &sic le 1539   then FF_IND=18;
          if &sic ge 1540 and &sic le 1549   then FF_IND=18;
          if &sic ge 1600 and &sic le 1699   then FF_IND=18;
          if &sic ge 1700 and &sic le 1799   then FF_IND=18;


*19 Steel  Steel Works Etc;
          if &sic ge 3300 and &sic le 3300   then FF_IND=19;
          if &sic ge 3310 and &sic le 3317   then FF_IND=19;
          if &sic ge 3320 and &sic le 3325   then FF_IND=19;
          if &sic ge 3330 and &sic le 3339   then FF_IND=19;
          if &sic ge 3340 and &sic le 3341   then FF_IND=19;
          if &sic ge 3350 and &sic le 3357   then FF_IND=19;
          if &sic ge 3360 and &sic le 3369   then FF_IND=19;
          if &sic ge 3370 and &sic le 3379   then FF_IND=19;
          if &sic ge 3390 and &sic le 3399   then FF_IND=19;


*20 FabPr  Fabricated Products;
          if &sic ge 3400 and &sic le 3400   then FF_IND=20;
          if &sic ge 3443 and &sic le 3443   then FF_IND=20;
          if &sic ge 3444 and &sic le 3444   then FF_IND=20;
          if &sic ge 3460 and &sic le 3469   then FF_IND=20;
          if &sic ge 3470 and &sic le 3479   then FF_IND=20;


*21 Mach   Machinery;
          if &sic ge 3510 and &sic le 3519   then FF_IND=21;
          if &sic ge 3520 and &sic le 3529   then FF_IND=21;
          if &sic ge 3530 and &sic le 3530   then FF_IND=21;
          if &sic ge 3531 and &sic le 3531   then FF_IND=21;
          if &sic ge 3532 and &sic le 3532   then FF_IND=21;
          if &sic ge 3533 and &sic le 3533   then FF_IND=21;
          if &sic ge 3534 and &sic le 3534   then FF_IND=21;
          if &sic ge 3535 and &sic le 3535   then FF_IND=21;
          if &sic ge 3536 and &sic le 3536   then FF_IND=21;
          if &sic ge 3538 and &sic le 3538   then FF_IND=21;
          if &sic ge 3540 and &sic le 3549   then FF_IND=21;
          if &sic ge 3550 and &sic le 3559   then FF_IND=21;
          if &sic ge 3560 and &sic le 3569   then FF_IND=21;
          if &sic ge 3580 and &sic le 3580   then FF_IND=21;
          if &sic ge 3581 and &sic le 3581   then FF_IND=21;
          if &sic ge 3582 and &sic le 3582   then FF_IND=21;
          if &sic ge 3585 and &sic le 3585   then FF_IND=21;
          if &sic ge 3586 and &sic le 3586   then FF_IND=21;
          if &sic ge 3589 and &sic le 3589   then FF_IND=21;
          if &sic ge 3590 and &sic le 3599   then FF_IND=21;


*22 ElcEq  Electrical Equipment;
          if &sic ge 3600 and &sic le 3600   then FF_IND=22;
          if &sic ge 3610 and &sic le 3613   then FF_IND=22;
          if &sic ge 3620 and &sic le 3621   then FF_IND=22;
          if &sic ge 3623 and &sic le 3629   then FF_IND=22;
          if &sic ge 3640 and &sic le 3644   then FF_IND=22;
          if &sic ge 3645 and &sic le 3645   then FF_IND=22; 
          if &sic ge 3646 and &sic le 3646   then FF_IND=22;
          if &sic ge 3648 and &sic le 3649   then FF_IND=22;
          if &sic ge 3660 and &sic le 3660   then FF_IND=22;
          if &sic ge 3690 and &sic le 3690   then FF_IND=22;
          if &sic ge 3691 and &sic le 3692   then FF_IND=22;
          if &sic ge 3699 and &sic le 3699   then FF_IND=22;


*23 Autos  Automobiles and Trucks;
          if &sic ge 2296 and &sic le 2296   then FF_IND=23;
          if &sic ge 2396 and &sic le 2396   then FF_IND=23;
          if &sic ge 3010 and &sic le 3011   then FF_IND=23;
          if &sic ge 3537 and &sic le 3537   then FF_IND=23;
          if &sic ge 3647 and &sic le 3647   then FF_IND=23;
          if &sic ge 3694 and &sic le 3694   then FF_IND=23;
          if &sic ge 3700 and &sic le 3700   then FF_IND=23;
          if &sic ge 3710 and &sic le 3710   then FF_IND=23;
          if &sic ge 3711 and &sic le 3711   then FF_IND=23;
          if &sic ge 3713 and &sic le 3713   then FF_IND=23;
          if &sic ge 3714 and &sic le 3714   then FF_IND=23;
          if &sic ge 3715 and &sic le 3715   then FF_IND=23;
          if &sic ge 3716 and &sic le 3716   then FF_IND=23;
          if &sic ge 3792 and &sic le 3792   then FF_IND=23;
          if &sic ge 3790 and &sic le 3791   then FF_IND=23;
          if &sic ge 3799 and &sic le 3799   then FF_IND=23;


*24 Aero   Aircraft;
          if &sic ge 3720 and &sic le 3720   then FF_IND=24;
          if &sic ge 3721 and &sic le 3721   then FF_IND=24;
          if &sic ge 3723 and &sic le 3724   then FF_IND=24;
          if &sic ge 3725 and &sic le 3725   then FF_IND=24;
          if &sic ge 3728 and &sic le 3729   then FF_IND=24;


*25 Ships  Shipbuilding, Railroad Equipment;
          if &sic ge 3730 and &sic le 3731   then FF_IND=25;
          if &sic ge 3740 and &sic le 3743   then FF_IND=25;


*26 Guns   Defense;
          if &sic ge 3760 and &sic le 3769   then FF_IND=26;
          if &sic ge 3795 and &sic le 3795   then FF_IND=26;
          if &sic ge 3480 and &sic le 3489   then FF_IND=26;


*27 Gold   Precious Metals;
          if &sic ge 1040 and &sic le 1049   then FF_IND=27;


*28 Mines  Non and &sic le Metallic and Industrial Metal Mining;;
          if &sic ge 1000 and &sic le 1009   then FF_IND=28;
          if &sic ge 1010 and &sic le 1019   then FF_IND=28;
          if &sic ge 1020 and &sic le 1029   then FF_IND=28;
          if &sic ge 1030 and &sic le 1039   then FF_IND=28;
          if &sic ge 1050 and &sic le 1059   then FF_IND=28;
          if &sic ge 1060 and &sic le 1069   then FF_IND=28;
          if &sic ge 1070 and &sic le 1079   then FF_IND=28;
          if &sic ge 1080 and &sic le 1089   then FF_IND=28;
          if &sic ge 1090 and &sic le 1099   then FF_IND=28;
          if &sic ge 1100 and &sic le 1119   then FF_IND=28;
          if &sic ge 1400 and &sic le 1499   then FF_IND=28;


*29 Coal   Coal;
          if &sic ge 1200 and &sic le 1299   then FF_IND=29;

*30 Oil    Petroleum and Natural Gas;
          if &sic ge 1300 and &sic le 1300   then FF_IND=30;
          if &sic ge 1310 and &sic le 1319   then FF_IND=30;
          if &sic ge 1320 and &sic le 1329   then FF_IND=30;
          if &sic ge 1330 and &sic le 1339   then FF_IND=30;
          if &sic ge 1370 and &sic le 1379   then FF_IND=30;
          if &sic ge 1380 and &sic le 1380   then FF_IND=30;
          if &sic ge 1381 and &sic le 1381   then FF_IND=30;
          if &sic ge 1382 and &sic le 1382   then FF_IND=30;
          if &sic ge 1389 and &sic le 1389   then FF_IND=30;
          if &sic ge 2900 and &sic le 2912   then FF_IND=30;
          if &sic ge 2990 and &sic le 2999   then FF_IND=30;


*31 Util   Utilities;
          if &sic ge 4900 and &sic le 4900   then FF_IND=31;
          if &sic ge 4910 and &sic le 4911   then FF_IND=31;
          if &sic ge 4920 and &sic le 4922   then FF_IND=31;
          if &sic ge 4923 and &sic le 4923   then FF_IND=31;
          if &sic ge 4924 and &sic le 4925   then FF_IND=31;
          if &sic ge 4930 and &sic le 4931   then FF_IND=31;
          if &sic ge 4932 and &sic le 4932   then FF_IND=31;
          if &sic ge 4939 and &sic le 4939   then FF_IND=31;
          if &sic ge 4940 and &sic le 4942   then FF_IND=31;


*32 Telcm  Communication;
          if &sic ge 4800 and &sic le 4800   then FF_IND=32;
          if &sic ge 4810 and &sic le 4813   then FF_IND=32;
          if &sic ge 4820 and &sic le 4822   then FF_IND=32;
          if &sic ge 4830 and &sic le 4839   then FF_IND=32;
          if &sic ge 4840 and &sic le 4841   then FF_IND=32;
          if &sic ge 4880 and &sic le 4889   then FF_IND=32;
          if &sic ge 4890 and &sic le 4890   then FF_IND=32;
          if &sic ge 4891 and &sic le 4891   then FF_IND=32;
          if &sic ge 4892 and &sic le 4892   then FF_IND=32;
          if &sic ge 4899 and &sic le 4899   then FF_IND=32;


*33 PerSv  Personal Services;
          if &sic ge 7020 and &sic le 7021   then FF_IND=33;
          if &sic ge 7030 and &sic le 7033   then FF_IND=33;
          if &sic ge 7200 and &sic le 7200   then FF_IND=33;
          if &sic ge 7210 and &sic le 7212   then FF_IND=33;
          if &sic ge 7214 and &sic le 7214   then FF_IND=33;
          if &sic ge 7215 and &sic le 7216   then FF_IND=33;
          if &sic ge 7217 and &sic le 7217   then FF_IND=33;
          if &sic ge 7219 and &sic le 7219   then FF_IND=33;
          if &sic ge 7220 and &sic le 7221   then FF_IND=33;
          if &sic ge 7230 and &sic le 7231   then FF_IND=33;
          if &sic ge 7240 and &sic le 7241   then FF_IND=33;
          if &sic ge 7250 and &sic le 7251   then FF_IND=33;
          if &sic ge 7260 and &sic le 7269   then FF_IND=33;
          if &sic ge 7270 and &sic le 7290   then FF_IND=33;
          if &sic ge 7291 and &sic le 7291   then FF_IND=33;
          if &sic ge 7292 and &sic le 7299   then FF_IND=33;
          if &sic ge 7395 and &sic le 7395   then FF_IND=33;
          if &sic ge 7500 and &sic le 7500   then FF_IND=33;
          if &sic ge 7520 and &sic le 7529   then FF_IND=33;
          if &sic ge 7530 and &sic le 7539   then FF_IND=33;
          if &sic ge 7540 and &sic le 7549   then FF_IND=33;
          if &sic ge 7600 and &sic le 7600   then FF_IND=33;
          if &sic ge 7620 and &sic le 7620   then FF_IND=33;
          if &sic ge 7622 and &sic le 7622   then FF_IND=33;
          if &sic ge 7623 and &sic le 7623   then FF_IND=33;
          if &sic ge 7629 and &sic le 7629   then FF_IND=33;
          if &sic ge 7630 and &sic le 7631   then FF_IND=33;
          if &sic ge 7640 and &sic le 7641   then FF_IND=33;
          if &sic ge 7690 and &sic le 7699   then FF_IND=33;
          if &sic ge 8100 and &sic le 8199   then FF_IND=33;
          if &sic ge 8200 and &sic le 8299   then FF_IND=33;
          if &sic ge 8300 and &sic le 8399   then FF_IND=33;
          if &sic ge 8400 and &sic le 8499   then FF_IND=33;
          if &sic ge 8600 and &sic le 8699   then FF_IND=33;
          if &sic ge 8800 and &sic le 8899   then FF_IND=33;
          if &sic ge 7510 and &sic le 7515   then FF_IND=33;


*34 BusSv  Business Services;
          if &sic ge 2750 and &sic le 2759   then FF_IND=34;
          if &sic ge 3993 and &sic le 3993   then FF_IND=34;
          if &sic ge 7218 and &sic le 7218   then FF_IND=34;
          if &sic ge 7300 and &sic le 7300   then FF_IND=34;
          if &sic ge 7310 and &sic le 7319   then FF_IND=34;
          if &sic ge 7320 and &sic le 7329   then FF_IND=34;
          if &sic ge 7330 and &sic le 7339   then FF_IND=34;
          if &sic ge 7340 and &sic le 7342   then FF_IND=34;
          if &sic ge 7349 and &sic le 7349   then FF_IND=34;
          if &sic ge 7350 and &sic le 7351   then FF_IND=34;
          if &sic ge 7352 and &sic le 7352   then FF_IND=34;
          if &sic ge 7353 and &sic le 7353   then FF_IND=34;
          if &sic ge 7359 and &sic le 7359   then FF_IND=34;
          if &sic ge 7360 and &sic le 7369   then FF_IND=34;
          if &sic ge 7370 and &sic le 7372   then FF_IND=34;
          if &sic ge 7374 and &sic le 7374   then FF_IND=34;
          if &sic ge 7375 and &sic le 7375   then FF_IND=34;
          if &sic ge 7376 and &sic le 7376   then FF_IND=34;
          if &sic ge 7377 and &sic le 7377   then FF_IND=34;
          if &sic ge 7378 and &sic le 7378   then FF_IND=34;
          if &sic ge 7379 and &sic le 7379   then FF_IND=34;
          if &sic ge 7380 and &sic le 7380   then FF_IND=34;
          if &sic ge 7381 and &sic le 7382   then FF_IND=34;
          if &sic ge 7383 and &sic le 7383   then FF_IND=34;
          if &sic ge 7384 and &sic le 7384   then FF_IND=34;
          if &sic ge 7385 and &sic le 7385   then FF_IND=34;
          if &sic ge 7389 and &sic le 7390   then FF_IND=34;
          if &sic ge 7391 and &sic le 7391   then FF_IND=34;
          if &sic ge 7392 and &sic le 7392   then FF_IND=34;
          if &sic ge 7393 and &sic le 7393   then FF_IND=34;
          if &sic ge 7394 and &sic le 7394   then FF_IND=34;
          if &sic ge 7396 and &sic le 7396   then FF_IND=34;
          if &sic ge 7397 and &sic le 7397   then FF_IND=34;
          if &sic ge 7399 and &sic le 7399   then FF_IND=34;
          if &sic ge 7519 and &sic le 7519   then FF_IND=34;
          if &sic ge 8700 and &sic le 8700   then FF_IND=34;
          if &sic ge 8710 and &sic le 8713   then FF_IND=34;
          if &sic ge 8720 and &sic le 8721   then FF_IND=34;
          if &sic ge 8730 and &sic le 8734   then FF_IND=34;
          if &sic ge 8740 and &sic le 8748   then FF_IND=34;
          if &sic ge 8900 and &sic le 8910   then FF_IND=34;
          if &sic ge 8911 and &sic le 8911   then FF_IND=34;
          if &sic ge 8920 and &sic le 8999   then FF_IND=34;
          if &sic ge 4220 and &sic le 4229  then FF_IND=34;


*35 Comps  Computers;
          if &sic ge 3570 and &sic le 3579   then FF_IND=35;
          if &sic ge 3680 and &sic le 3680   then FF_IND=35;
          if &sic ge 3681 and &sic le 3681   then FF_IND=35;
          if &sic ge 3682 and &sic le 3682   then FF_IND=35;
          if &sic ge 3683 and &sic le 3683   then FF_IND=35;
          if &sic ge 3684 and &sic le 3684   then FF_IND=35;
          if &sic ge 3685 and &sic le 3685   then FF_IND=35;
          if &sic ge 3686 and &sic le 3686   then FF_IND=35;
          if &sic ge 3687 and &sic le 3687   then FF_IND=35;
          if &sic ge 3688 and &sic le 3688   then FF_IND=35;
          if &sic ge 3689 and &sic le 3689   then FF_IND=35;
          if &sic ge 3695 and &sic le 3695   then FF_IND=35;
          if &sic ge 7373 and &sic le 7373   then FF_IND=35;


*36 Chips  Electronic Equipment;
          if &sic ge 3622 and &sic le 3622   then FF_IND=36;
          if &sic ge 3661 and &sic le 3661   then FF_IND=36;
          if &sic ge 3662 and &sic le 3662   then FF_IND=36;
          if &sic ge 3663 and &sic le 3663   then FF_IND=36;
          if &sic ge 3664 and &sic le 3664   then FF_IND=36;
          if &sic ge 3665 and &sic le 3665   then FF_IND=36;
          if &sic ge 3666 and &sic le 3666   then FF_IND=36;
          if &sic ge 3669 and &sic le 3669   then FF_IND=36;
          if &sic ge 3670 and &sic le 3679   then FF_IND=36;
          if &sic ge 3810 and &sic le 3810   then FF_IND=36;
          if &sic ge 3812 and &sic le 3812   then FF_IND=36;


*37 LabEq  Measuring and Control Equipment;
          if &sic ge 3811 and &sic le 3811   then FF_IND=37;
          if &sic ge 3820 and &sic le 3820   then FF_IND=37;
          if &sic ge 3821 and &sic le 3821   then FF_IND=37;
          if &sic ge 3822 and &sic le 3822   then FF_IND=37;
          if &sic ge 3823 and &sic le 3823   then FF_IND=37;
          if &sic ge 3824 and &sic le 3824   then FF_IND=37;
          if &sic ge 3825 and &sic le 3825   then FF_IND=37;
          if &sic ge 3826 and &sic le 3826   then FF_IND=37;
          if &sic ge 3827 and &sic le 3827   then FF_IND=37;
          if &sic ge 3829 and &sic le 3829   then FF_IND=37;
          if &sic ge 3830 and &sic le 3839   then FF_IND=37;


*38 Paper  Business Supplies;
          if &sic ge 2520 and &sic le 2549   then FF_IND=38;
          if &sic ge 2600 and &sic le 2639   then FF_IND=38;
          if &sic ge 2670 and &sic le 2699   then FF_IND=38;
          if &sic ge 2760 and &sic le 2761   then FF_IND=38;
          if &sic ge 3950 and &sic le 3955   then FF_IND=38;


*39 Boxes  Shipping Containers;
          if &sic ge 2440 and &sic le 2449   then FF_IND=39;
          if &sic ge 2640 and &sic le 2659   then FF_IND=39;
          if &sic ge 3220 and &sic le 3221   then FF_IND=39;
          if &sic ge 3410 and &sic le 3412   then FF_IND=39;


*40 Trans  Transportation;
          if &sic ge 4000 and &sic le 4013   then FF_IND=40;
          if &sic ge 4040 and &sic le 4049   then FF_IND=40;
          if &sic ge 4100 and &sic le 4100   then FF_IND=40;
          if &sic ge 4110 and &sic le 4119   then FF_IND=40;
          if &sic ge 4120 and &sic le 4121   then FF_IND=40;
          if &sic ge 4130 and &sic le 4131   then FF_IND=40;
          if &sic ge 4140 and &sic le 4142   then FF_IND=40;
          if &sic ge 4150 and &sic le 4151   then FF_IND=40;
          if &sic ge 4170 and &sic le 4173   then FF_IND=40;
          if &sic ge 4190 and &sic le 4199   then FF_IND=40;
          if &sic ge 4200 and &sic le 4200   then FF_IND=40;
          if &sic ge 4210 and &sic le 4219   then FF_IND=40;
          if &sic ge 4230 and &sic le 4231   then FF_IND=40;
          if &sic ge 4240 and &sic le 4249   then FF_IND=40;
          if &sic ge 4400 and &sic le 4499   then FF_IND=40;
          if &sic ge 4500 and &sic le 4599   then FF_IND=40;
          if &sic ge 4600 and &sic le 4699   then FF_IND=40;
          if &sic ge 4700 and &sic le 4700   then FF_IND=40;
          if &sic ge 4710 and &sic le 4712   then FF_IND=40;
          if &sic ge 4720 and &sic le 4729   then FF_IND=40;
          if &sic ge 4730 and &sic le 4739   then FF_IND=40;
          if &sic ge 4740 and &sic le 4749   then FF_IND=40;
          if &sic ge 4780 and &sic le 4780   then FF_IND=40;
          if &sic ge 4782 and &sic le 4782   then FF_IND=40;
          if &sic ge 4783 and &sic le 4783   then FF_IND=40;
          if &sic ge 4784 and &sic le 4784   then FF_IND=40;
          if &sic ge 4785 and &sic le 4785   then FF_IND=40;
          if &sic ge 4789 and &sic le 4789   then FF_IND=40;


*41 Whlsl  Wholesale;
          if &sic ge 5000 and &sic le 5000   then FF_IND=41;
          if &sic ge 5010 and &sic le 5015   then FF_IND=41;
          if &sic ge 5020 and &sic le 5023   then FF_IND=41;
          if &sic ge 5030 and &sic le 5039   then FF_IND=41;
          if &sic ge 5040 and &sic le 5042   then FF_IND=41;
          if &sic ge 5043 and &sic le 5043   then FF_IND=41;
          if &sic ge 5044 and &sic le 5044   then FF_IND=41;
          if &sic ge 5045 and &sic le 5045   then FF_IND=41;
          if &sic ge 5046 and &sic le 5046   then FF_IND=41;
          if &sic ge 5047 and &sic le 5047   then FF_IND=41;
          if &sic ge 5048 and &sic le 5048   then FF_IND=41;
          if &sic ge 5049 and &sic le 5049   then FF_IND=41;
          if &sic ge 5050 and &sic le 5059   then FF_IND=41;
          if &sic ge 5060 and &sic le 5060   then FF_IND=41;
          if &sic ge 5063 and &sic le 5063   then FF_IND=41;
          if &sic ge 5064 and &sic le 5064   then FF_IND=41;
          if &sic ge 5065 and &sic le 5065   then FF_IND=41;
          if &sic ge 5070 and &sic le 5078   then FF_IND=41;
          if &sic ge 5080 and &sic le 5080   then FF_IND=41;
          if &sic ge 5081 and &sic le 5081   then FF_IND=41;
          if &sic ge 5082 and &sic le 5082   then FF_IND=41;
          if &sic ge 5083 and &sic le 5083   then FF_IND=41;
          if &sic ge 5084 and &sic le 5084   then FF_IND=41;
          if &sic ge 5085 and &sic le 5085   then FF_IND=41;
          if &sic ge 5086 and &sic le 5087   then FF_IND=41;
          if &sic ge 5088 and &sic le 5088   then FF_IND=41;
          if &sic ge 5090 and &sic le 5090   then FF_IND=41;
          if &sic ge 5091 and &sic le 5092   then FF_IND=41;
          if &sic ge 5093 and &sic le 5093   then FF_IND=41;
          if &sic ge 5094 and &sic le 5094   then FF_IND=41;
          if &sic ge 5099 and &sic le 5099   then FF_IND=41;
          if &sic ge 5100 and &sic le 5100   then FF_IND=41;
          if &sic ge 5110 and &sic le 5113   then FF_IND=41;
          if &sic ge 5120 and &sic le 5122   then FF_IND=41;
          if &sic ge 5130 and &sic le 5139   then FF_IND=41;
          if &sic ge 5140 and &sic le 5149   then FF_IND=41;
          if &sic ge 5150 and &sic le 5159   then FF_IND=41;
          if &sic ge 5160 and &sic le 5169   then FF_IND=41;
          if &sic ge 5170 and &sic le 5172   then FF_IND=41;
          if &sic ge 5180 and &sic le 5182   then FF_IND=41;
          if &sic ge 5190 and &sic le 5199   then FF_IND=41;


*42 Rtail  Retail ;
          if &sic ge 5200 and &sic le 5200   then FF_IND=42;
          if &sic ge 5210 and &sic le 5219   then FF_IND=42;
          if &sic ge 5220 and &sic le 5229   then FF_IND=42;
          if &sic ge 5230 and &sic le 5231   then FF_IND=42;
          if &sic ge 5250 and &sic le 5251   then FF_IND=42;
          if &sic ge 5260 and &sic le 5261   then FF_IND=42;
          if &sic ge 5270 and &sic le 5271   then FF_IND=42;
          if &sic ge 5300 and &sic le 5300   then FF_IND=42;
          if &sic ge 5310 and &sic le 5311   then FF_IND=42;
          if &sic ge 5320 and &sic le 5320   then FF_IND=42;
          if &sic ge 5330 and &sic le 5331   then FF_IND=42;
          if &sic ge 5334 and &sic le 5334   then FF_IND=42;
          if &sic ge 5340 and &sic le 5349   then FF_IND=42;
          if &sic ge 5390 and &sic le 5399   then FF_IND=42;
          if &sic ge 5400 and &sic le 5400   then FF_IND=42;
          if &sic ge 5410 and &sic le 5411   then FF_IND=42;
          if &sic ge 5412 and &sic le 5412   then FF_IND=42;
          if &sic ge 5420 and &sic le 5429   then FF_IND=42;
          if &sic ge 5430 and &sic le 5439   then FF_IND=42;
          if &sic ge 5440 and &sic le 5449   then FF_IND=42;
          if &sic ge 5450 and &sic le 5459   then FF_IND=42;
          if &sic ge 5460 and &sic le 5469   then FF_IND=42;
          if &sic ge 5490 and &sic le 5499   then FF_IND=42;
          if &sic ge 5500 and &sic le 5500   then FF_IND=42;
          if &sic ge 5510 and &sic le 5529   then FF_IND=42;
          if &sic ge 5530 and &sic le 5539   then FF_IND=42;
          if &sic ge 5540 and &sic le 5549   then FF_IND=42;
          if &sic ge 5550 and &sic le 5559   then FF_IND=42;
          if &sic ge 5560 and &sic le 5569   then FF_IND=42;
          if &sic ge 5570 and &sic le 5579   then FF_IND=42;
          if &sic ge 5590 and &sic le 5599   then FF_IND=42;
          if &sic ge 5600 and &sic le 5699   then FF_IND=42;
          if &sic ge 5700 and &sic le 5700   then FF_IND=42;
          if &sic ge 5710 and &sic le 5719   then FF_IND=42;
          if &sic ge 5720 and &sic le 5722   then FF_IND=42;
          if &sic ge 5730 and &sic le 5733   then FF_IND=42;
          if &sic ge 5734 and &sic le 5734   then FF_IND=42;
          if &sic ge 5735 and &sic le 5735   then FF_IND=42;
          if &sic ge 5736 and &sic le 5736   then FF_IND=42;
          if &sic ge 5750 and &sic le 5799   then FF_IND=42;
          if &sic ge 5900 and &sic le 5900   then FF_IND=42;
          if &sic ge 5910 and &sic le 5912   then FF_IND=42;
          if &sic ge 5920 and &sic le 5929   then FF_IND=42;
          if &sic ge 5930 and &sic le 5932   then FF_IND=42;
          if &sic ge 5940 and &sic le 5940   then FF_IND=42;
          if &sic ge 5941 and &sic le 5941   then FF_IND=42;
          if &sic ge 5942 and &sic le 5942   then FF_IND=42;
          if &sic ge 5943 and &sic le 5943   then FF_IND=42;
          if &sic ge 5944 and &sic le 5944   then FF_IND=42;
          if &sic ge 5945 and &sic le 5945   then FF_IND=42;
          if &sic ge 5946 and &sic le 5946   then FF_IND=42;
          if &sic ge 5947 and &sic le 5947   then FF_IND=42;
          if &sic ge 5948 and &sic le 5948   then FF_IND=42;
          if &sic ge 5949 and &sic le 5949   then FF_IND=42;
          if &sic ge 5950 and &sic le 5959   then FF_IND=42;
          if &sic ge 5960 and &sic le 5969   then FF_IND=42;
          if &sic ge 5970 and &sic le 5979   then FF_IND=42;
          if &sic ge 5980 and &sic le 5989   then FF_IND=42;
          if &sic ge 5990 and &sic le 5990   then FF_IND=42;
          if &sic ge 5992 and &sic le 5992   then FF_IND=42;
          if &sic ge 5993 and &sic le 5993   then FF_IND=42;
          if &sic ge 5994 and &sic le 5994   then FF_IND=42;
          if &sic ge 5995 and &sic le 5995   then FF_IND=42;
          if &sic ge 5999 and &sic le 5999   then FF_IND=42;


*43 Meals  Restaraunts, Hotels, Motels;
          if &sic ge 5800 and &sic le 5819   then FF_IND=43;
          if &sic ge 5820 and &sic le 5829   then FF_IND=43;
          if &sic ge 5890 and &sic le 5899   then FF_IND=43;
          if &sic ge 7000 and &sic le 7000   then FF_IND=43;
          if &sic ge 7010 and &sic le 7019   then FF_IND=43;
          if &sic ge 7040 and &sic le 7049   then FF_IND=43;
          if &sic ge 7213 and &sic le 7213   then FF_IND=43;


*44 Banks  Banking;
          if &sic ge 6000 and &sic le 6000   then FF_IND=44;
          if &sic ge 6010 and &sic le 6019   then FF_IND=44;
          if &sic ge 6020 and &sic le 6020   then FF_IND=44;
          if &sic ge 6021 and &sic le 6021   then FF_IND=44;
          if &sic ge 6022 and &sic le 6022   then FF_IND=44;
          if &sic ge 6023 and &sic le 6024   then FF_IND=44;
          if &sic ge 6025 and &sic le 6025   then FF_IND=44;
          if &sic ge 6026 and &sic le 6026   then FF_IND=44;
          if &sic ge 6027 and &sic le 6027   then FF_IND=44;
          if &sic ge 6028 and &sic le 6029   then FF_IND=44;
          if &sic ge 6030 and &sic le 6036   then FF_IND=44;
          if &sic ge 6040 and &sic le 6059   then FF_IND=44;
          if &sic ge 6060 and &sic le 6062   then FF_IND=44;
          if &sic ge 6080 and &sic le 6082   then FF_IND=44;
          if &sic ge 6090 and &sic le 6099   then FF_IND=44;
          if &sic ge 6100 and &sic le 6100   then FF_IND=44;
          if &sic ge 6110 and &sic le 6111   then FF_IND=44;
          if &sic ge 6112 and &sic le 6113   then FF_IND=44;
          if &sic ge 6120 and &sic le 6129   then FF_IND=44;
          if &sic ge 6130 and &sic le 6139   then FF_IND=44;
          if &sic ge 6140 and &sic le 6149   then FF_IND=44;
          if &sic ge 6150 and &sic le 6159   then FF_IND=44;
          if &sic ge 6160 and &sic le 6169   then FF_IND=44;
          if &sic ge 6170 and &sic le 6179   then FF_IND=44;
          if &sic ge 6190 and &sic le 6199   then FF_IND=44;


*45 Insur  Insurance;
          if &sic ge 6300 and &sic le 6300   then FF_IND=45;
          if &sic ge 6310 and &sic le 6319   then FF_IND=45;
          if &sic ge 6320 and &sic le 6329   then FF_IND=45;
          if &sic ge 6330 and &sic le 6331   then FF_IND=45;
          if &sic ge 6350 and &sic le 6351   then FF_IND=45;
          if &sic ge 6360 and &sic le 6361   then FF_IND=45;
          if &sic ge 6370 and &sic le 6379   then FF_IND=45;
          if &sic ge 6390 and &sic le 6399   then FF_IND=45;
          if &sic ge 6400 and &sic le 6411   then FF_IND=45;


*46 RlEst  Real Estate;
          if &sic ge 6500 and &sic le 6500   then FF_IND=46;
          if &sic ge 6510 and &sic le 6510   then FF_IND=46;
          if &sic ge 6512 and &sic le 6512   then FF_IND=46;
          if &sic ge 6513 and &sic le 6513   then FF_IND=46;
          if &sic ge 6514 and &sic le 6514   then FF_IND=46;
          if &sic ge 6515 and &sic le 6515   then FF_IND=46;
          if &sic ge 6517 and &sic le 6519   then FF_IND=46;
          if &sic ge 6520 and &sic le 6529   then FF_IND=46;
          if &sic ge 6530 and &sic le 6531   then FF_IND=46;
          if &sic ge 6532 and &sic le 6532   then FF_IND=46;
          if &sic ge 6540 and &sic le 6541   then FF_IND=46;
          if &sic ge 6550 and &sic le 6553   then FF_IND=46;
          if &sic ge 6590 and &sic le 6599   then FF_IND=46;
          if &sic ge 6610 and &sic le 6611   then FF_IND=46;


*47 Fin    Trading;
          if &sic ge 6200 and &sic le 6299   then FF_IND=47;
          if &sic ge 6700 and &sic le 6700   then FF_IND=47;
          if &sic ge 6710 and &sic le 6719   then FF_IND=47;
          if &sic ge 6720 and &sic le 6722   then FF_IND=47;
          if &sic ge 6723 and &sic le 6723   then FF_IND=47;
          if &sic ge 6724 and &sic le 6724   then FF_IND=47;
          if &sic ge 6725 and &sic le 6725   then FF_IND=47;
          if &sic ge 6726 and &sic le 6726   then FF_IND=47;
          if &sic ge 6730 and &sic le 6733   then FF_IND=47;
          if &sic ge 6740 and &sic le 6779   then FF_IND=47;
          if &sic ge 6790 and &sic le 6791   then FF_IND=47;
          if &sic ge 6792 and &sic le 6792   then FF_IND=47;
          if &sic ge 6793 and &sic le 6793   then FF_IND=47;
          if &sic ge 6794 and &sic le 6794   then FF_IND=47;
          if &sic ge 6795 and &sic le 6795   then FF_IND=47;
          if &sic ge 6798 and &sic le 6798   then FF_IND=47;
          if &sic ge 6799 and &sic le 6799   then FF_IND=47;


*48 Other  Almost Nothing;
          if &sic ge 4950 and &sic le 4959   then FF_IND=48;
          if &sic ge 4960 and &sic le 4961   then FF_IND=48;
          if &sic ge 4970 and &sic le 4971   then FF_IND=48;
          if &sic ge 4990 and &sic le 4991   then FF_IND=48; 
          if &sic ge 9990 and &sic le 9999   then FF_IND=48; 
     

run;	


%mend;



/********************************************************************************
	Author: 	Ed deHaan		staff.washington.edu/edehaan
	
	Macro: 		ind_ff12
	
	Purpose: 	to assign Fama 12 French industry codes

	Versions:	1.0 - 	2/5/11
				1.1 - 	2/17/11 - fixed a few typos in description, etc.
				
	
	Notes:		12 Industry classification codes obtained from French's website in Nov. 2010
				http://mba.tuck.dartmouth.edu/pages/faculty/ken.french/data_library.html


--------------------------------------------

Generates Fama-French industry codes based on four-digit SIC codes.  


Outputs the original dataset with appended industry code information:

	'&ind_code' 	= count variable of industry codes from 1 through 12
	'FF_IND'		= text variable with name of the fama-french industry
	'&bin_var.#'	= 12 individual binary variables, one for each industry
	'_&global'		= global macro variable to include call 12 industry dummies

--------------------------------------------


	Required INPUT parameters:
		dset			-	input dataset name
		sic				-	four-digit sic code variable name
		outp			-	output dataset
		bin_var			-	prefix for industry binary variables
		ind_code		-	name of count variable for industry codes 1 through 12
		global			-	name of global macro variable to call all 12 industry dummies
	
	Optional INPUT parameters
;

	


********************************************************************************/


%let _industry_fe= 	i1 i2 i3 i4 i5 i6 i7 i8 i9 i10 i11 i12 ;

%macro ind_ff12 (dset, outp, sic, ind_code );

********** FF Ind Codes Macro **********;



data &outp;
        set &dset;

        indus2=int(&sic/100);
        indus3=int(&sic/10);

*1 NoDur  Consumer NonDurables -- Food, Tobacco, Textiles, Apparel, Leather, Toys;
         if &sic ge   0100 and &sic le  0999 	then &ind_code=1;
         if &sic ge   2000 and &sic le 	2399 	then &ind_code=1;
         if &sic ge   2700 and &sic le 	2749 	then &ind_code=1;
         if &sic ge   2770 and &sic le 	2799 	then &ind_code=1;
         if &sic ge   3100 and &sic le 	3199 	then &ind_code=1;
         if &sic ge   3940 and &sic le 	3989 	then &ind_code=1;

 *2 Durbl  Consumer Durables -- Cars, TVs, Furniture, Household Appliances;
        if &sic ge   2500 and &sic le   2519 	then &ind_code=2;
        if &sic ge   2590 and &sic le 	2599 	then &ind_code=2;
        if &sic ge   3630 and &sic le 	3659 	then &ind_code=2;
        if &sic ge   3710 and &sic le 	3711 	then &ind_code=2;
        if &sic ge   3714 and &sic le 	3714 	then &ind_code=2;
        if &sic ge   3716 and &sic le 	3716 	then &ind_code=2;
        if &sic ge   3750 and &sic le 	3751 	then &ind_code=2;
        if &sic ge   3792 and &sic le 	3792 	then &ind_code=2;
        if &sic ge   3900 and &sic le 	3939 	then &ind_code=2;
        if &sic ge   3990 and &sic le 	3999 	then &ind_code=2;

* 3 Manuf  Manufacturing -- Machinery, Trucks, Planes, Off Furn, Paper, Com Printing;
        if &sic ge    2520 and &sic le  2589 	then &ind_code=3;
        if &sic ge    2600 and &sic le 	2699 	then &ind_code=3;
        if &sic ge    2750 and &sic le 	2769 	then &ind_code=3;
        if &sic ge    3000 and &sic le 	3099 	then &ind_code=3;
        if &sic ge    3200 and &sic le 	3569 	then &ind_code=3;
        if &sic ge    3580 and &sic le 	3629 	then &ind_code=3;
        if &sic ge    3700 and &sic le 	3709 	then &ind_code=3;
        if &sic ge    3712 and &sic le 	3713 	then &ind_code=3;
        if &sic ge    3715 and &sic le 	3715 	then &ind_code=3;
        if &sic ge    3717 and &sic le 	3749 	then &ind_code=3;
        if &sic ge    3752 and &sic le 	3791 	then &ind_code=3;
        if &sic ge    3793 and &sic le 	3799 	then &ind_code=3;
        if &sic ge    3830 and &sic le 	3839 	then &ind_code=3;
        if &sic ge    3860 and &sic le 	3899 	then &ind_code=3;

* 4 Enrgy  Oil, Gas, and Coal Extraction and Products;
        if &sic ge     1200 and &sic le 	1399 	then &ind_code=4;
        if &sic ge     2900 and &sic le 	2999 	then &ind_code=4;

* 5 Chems  Chemicals and Allied Products;
        if &sic ge     2800 and &sic le 	2829 	then &ind_code=5;
        if &sic ge     2840 and &sic le 	2899 	then &ind_code=5;

* 6 BusEq  Business Equipment -- Computers, Software, and Electronic Equipment;
        if &sic ge    3570 and &sic le 	3579 	then &ind_code=6;
        if &sic ge    3660 and &sic le 	3692 	then &ind_code=6;
        if &sic ge    3694 and &sic le 	3699 	then &ind_code=6;
        if &sic ge    3810 and &sic le 	3829 	then &ind_code=6;
        if &sic ge    7370 and &sic le 	7379 	then &ind_code=6;

* 7 Telcm  Telephone and Television Transmission;
         if &sic ge    4800 and &sic le 	4899 	then &ind_code=7;

* 8 Utils  Utilities;
         if &sic ge    4900 and &sic le 	4949 	then &ind_code=8;

* 9 Shops  Wholesale, Retail, and Some Services (Laundries, Repair Shops);
        if &sic ge    5000 and &sic le 	5999 	then &ind_code=9;
        if &sic ge    7200 and &sic le 	7299 	then &ind_code=9;
        if &sic ge    7600 and &sic le 	7699 	then &ind_code=9;

*10 Hlth   Healthcare, Medical Equipment, and Drugs;
         if &sic ge   2830 and &sic le 	2839 	then &ind_code=10;
         if &sic ge   3693 and &sic le 	3693 	then &ind_code=10;
         if &sic ge   3840 and &sic le 	3859 	then &ind_code=10;
         if &sic ge   8000 and &sic le 	8099 	then &ind_code=10;

*11 Money  Finance;
         if &sic ge   6000 and &sic le 	6999 	then &ind_code=11;

*12 Other  Other -- Mines, Constr, BldMt, Trans, Hotels, Bus Serv, Entertainment;
		 if &sic > .		and &ind_code = .	then &ind_code=12;

	
run;	


%mend ind_ff12;


/*
**********************************************************************;
**********************************************************************;
Macro mycstlink
Created by Richard Price
Updated May 2009
**********************************************************************;
**********************************************************************;
Overview:

This macro creates a file that links GVKEY to PERMNO.  It uses as a
starting point the raw Compustat/CRSP merged files.  Code like this
was used to merge Compustat and CRSP in Beaver, McNichols and Price
(Journal of Accounting and Economics, 2007) which investigates a
number of causes of the inadvertent exclusion of delistings. Depending
on how Compustat and CRSP are merged, many delistings can be excluded
because the effective date range in the Compustat/CRSP merged database
often ends before the security is delisted.

You are entitled to a full refund if you find errors.  However, since
this code is free, that guarantee is pretty hollow.  I am fairly
confident in the output, but since every computer program has at least
one error, I don't exempt myself...  If you discover any errors,
please let me know.  Also, please cite the above paper if you do use
the code.

**********************************************************************;
Purpose:

This main purpose of this code is to extend the date range (linkdt,
linkenddt) where possible to ensure the maximum sample size.  Most of
the time, the date ranges provided by CRSP are fine, but there are
some instances where the date range ends before the security price
data ends.

The link between PERMNO and GVKEY is a many to many relationship. A
GVKEY may be related to different PERMNOs over its history.  Likewise
a PERMNO may be assigned to different GVKEYS.  Both of these issues
must be dealt with separately.

**********************************************************************;
Extending Link Date Ranges:

Of particular interest is allowing the beginning and ending effective
link dates to extend as far as posible, or as far as you want.  I
refer to this as slack.  Preslack is extending the linkdt to the
earliest possible date.  Slack is extending linkenddt to the latest
possible date.  Slack should not be allowed when conflicting GVKEY-PERMNO
matches exist.

In the old version of the CCM database, there were some instances of
overlapping GVKEY-PERMNO links (where a gvkey was linked to multiple
permnos or vice versa).  In the new database, CRSP has apparently
taken care of this problem (although I have found some instances of
overlapping date ranges, but CRSP periodically fixes these errors). In
this macro, I preserve the original date ranges from CRSP, and extend
them where appropriate.

**********************************************************************;
EXAMPLE 1:

As an example, consider GVKEY 5199 and 3998, which both link to permno
30278. 

Below are the valid date ranges from ccmxpf_lnkused
(usedflag=1).  There is an overlapping date range for six months,
which could result in some duplicate observations.  I do nothing to
correct this.  CRSP should fix this type of error eventually.

*** Date ranges from ccmxpf_lnkused;

UGVKEY   ULINKTYPE   UPERMNO   USEDFLAG    ULINKDT   ULINKENDDT

003998      LC        22860        1      19721214    19831229
003998      LC        30278        1      19831230    19850831
005199      LU        30278        1      19820406    19840629

** Date ranges produced by this macro;

  slinkdt slinkenddt extlinkdt extlinkenddt GVKEY  LPERMNO   LINKDT LINKENDDT

17JUN1972 29DEC1983  01JAN1925  29DEC1983   003998  22860  19721214 19831229 
30DEC1983 27FEB1986  30DEC1983  04MAY2009   003998  30278  19831230 19850831 
08OCT1981 29JUN1984  01JAN1925  29JUN1984   005199  30278  19820406 19840629 

Note that the linkdt and linkenddt variables are unchanged.  Two 
additional date ranges are added:

1) slinkdt, slinkenddt
2) extlinkdt, extlinkenddt

The first date range , slinkdt and slinkenddt, extend the date range
up to the number of days specified in the macro options (default
slack=180). The second date range, extlinkdt and extlinkenddt, extend
the date range as far as possible, i.e., if a gvkey links to no other
permnos, and the permno links to no other gvkeys, the extlinkdt is set
to 01JAN1925, and the extlinkenddt is set to today (04MAY2009).

With the first observation (gvkey 003998), it linkdt is fully extended
(slinkdt = 17JUN1972, extlinkdt=01JAN1925).  However, linkenddt is not
extended (linkenddt=slinkenddt=extlinkenddt) because there is another
link after the current link.

With the second observation (also gvkey 003998), linkenddt is fully
extended.  However, linkdt is not extended because GVKEY 003998 links
to permno 22860 prior to that.  Even if it did not, because GVKEY
005199 links to permno 30278, the linkdt would not be extended.

With the third observation (gvkey 005199), linkdt is extended as far
as possible because there is no conflicting prior link.  However,
linkenddt is not extended because the permno links to gvkey 003998
afterwards.

**********************************************************************;
EXAMPLE 2;

*** CCM date ranges for gvkey 001043;

GVKEY     LPERMNO       LINKDT    LINKENDDT    USEDFLAG

001043     18980      19581231    19620130         1
001043     18980      19620131    19820427         1  
** a gap here;
001043     18980      19840510    19930519         1  
** a gap here;
001043     80071      19931206    20000127         1

** this macro produces the following;

  slinkdt  slinkenddt  extlinkdt  extlinkenddt  GVKEY   LPERMNO    LINKDT  LINKENDDT

04JUL1958  30JAN1962   01JAN1925   30JAN1962    001043   18980   19581231  19620130 
31JAN1962  24OCT1982   31JAN1962   09MAY1984    001043   18980   19620131  19820427 
12NOV1983  15NOV1993   28APR1982   05DEC1993    001043   18980   19840510  19930519 
09JUN1993  25JUL2000   20MAY1993   04MAY2009    001043   80071   19931206  20000127 

**********************************************************************;
Identifiers in Compustat;

The primary identifier in Compustat is GVKEY.  In the new version of
Compustat, another identifier is added -- iid, the issue ID for a
security.  This will allow you to merge based on iid in addition.  I
think that there are additional issues that would need to be dealt
with with this type of merge, such as which iid do you use, and there
are apparently some cases where CRSP assigns an iid within CCM because
there is none assigned in Compustat (I have not verified this).  But
fundamentally, the GVKEY can have multiple security issues.

In most cases merges are done only by GVKEY.  I provide date ranges for
both types of merge.  First, for the merge done only on GVKEY, and second
for a merge done with GVKEY and iid.  I expect most people will only use
the merge based on GVKEY.

**********************************************************************;
Delistings after the linkenddt;

I have done some checking, and it still seems that the CCM date ranges
still need to be extended in some cases.  The following code shows
that there are many delistings after the linkenddt, although not as
many as before (it used to be about half of all delistings, it is now
about 5 percent, which is over 1,000 observations).

data delist;
    set crsp.mse;
    where dlstcd > 199;
    keep date permno dlstcd dlret dlpdt;
run;
** 21122 observations;

%mycstlink;
* run the macro to create the mycstlink dataset;

* merge gvkey with delisting file;
proc sql;
    create table delist2 as
        select distinct d.*, c.gvkey
        from delist d left join mycstlink c
        on d.permno=c.lpermno
	and c.linkdt le d.date le c.linkenddt;
** 21130 observations;
** there are some duplicates due to some overlapping date ranges in CRSP;
** I emailed CRSP with the list of problematic links;
** They will probably be fixed sometime;

* delistings that have no gvkey merged;
    select count(*) as n_unmerged
	from delist2
	where gvkey="";
** 5141 observations;

proc sql;
    create table ext_delist2 as
        select distinct d.*, c.gvkey
        from delist d left join mycstlink c
        on d.permno=c.lpermno
	and c.linkdt le d.date le c.extlinkenddt;
** 21130 observations -- my code doesn't cause any additional duplicates;

* delistings that have no gvkey merged after extending linkenddt;
    select count(*) as n_unmerged_extended
	from ext_delist2
	where gvkey="";
** 3995 observations;

The relevant comparison is the number of delistings that do not
merge. Using the CRSP-supplied range, there are 5,141 delistings that
do not merge vs 3,995 using extlinkenddt --- an additional 1,146
merged delistings.

**********************************************************************;
Input Variables:
  preslack: A variable indicating how many days before the linkdt to allow slack
     slack: A variable indicating how many days after the linkenddt to allow slack

          preslack and slack are numbers in DAYS that determines
          whether the variables linkbef or linkaft.  THE DEFAULT IS
          180 days for both.  Preslack can be positive or negative
          (absolute value functions are used to protect from potential
          errors).

          exclude_dual_class: some duplicates may exist because dual classes of
          shares exist.  To exclude them, set exclude_dual_class=1 (the default);
          Otherwise set to 0.

**********************************************************************;
Output: 
This macro creates a file in the working directory called
mycstlink. You can save this dataset somewhere.  But since the macro
runs quickly, there is no need.

  VARIABLES CREATED:
  
  FOR A MERGE BASED ONLY ON GVKEY (NOT IID)
          slinkdt: This is a link date that incorporates preslack (equals linkdt - preslack)
       slinkenddt: This is a link end date that incorporates slack (postslack)
                   This equals linkenddt + slack.
        extlinkdt: This equals 1/1/1925 or the earliest possible date
     extlinkenddt: This equals the latest possible date, with the maximum date being today
                   (the date you run the program).

  FOR A MERGE BASED ON GVKEY AND IID
          slinkdt_iid  (same descriptions as above)
       slinkenddt_iid
        extlinkdt_iid
     extlinkenddt_iid

  Note:
        linkenddt: Note that I set linkenddt to today's date rather than .E.  It makes
                   it easier to work with.

**********************************************************************;
NOTE:  The use of extlink and slink variables may result in some
       overlapping ranges because with extlinkdt and extlinkenddt the
       range expands as far as possible until arriving at another
       active link.  However that active link ext dates also expand as
       far as possible.
       
       For example, for a hypothetical GVKEY:
            linkdt     linkenddt    NPERMNO   extlinkdt  extlinkenddt
            19720101   19761030         X     19250101   19761231
            19770101   19770330         Y     19761031   20090504

       The first observation has extlinkenddt to 19761231.  The second
       observation has extlinkdt as 19761031.  If there was any security
       price data between 19761031 and 19761231, it would merge with both
       GVKEYs if extlinkdt and extlinkenddt are used.

       This can be addressed in two ways.  First, allow slack (after)
       but no preslack (before). This would extend the effective end
       date as far as possible, but leaves the effective beginning
       date alone. Second, the variables in the macro which determine
       the amount of allowable slack (max_slack_gvkey,
       max_slack_permno, max_preslack_gvkey, max_preslack_permno)
       could be adjusted in some way.  However, I do not do this, and
       in general think the first method is fine.

       In general, I think it is more important to ensure that the end
       date range is extended rather than the beginning date
       range.  As documented in Beaver, McNichols and Price (2007)
       nearly half of all delistings occured outside the stated range
       in the CCM database.  (I think this problem is less severe, but
       up to 10% of delistings occur after the end of the date
       range).  I have not investigated whether beginning date ranges
       need to be extended.

       You should pay attention to your datasets to make sure that the
       number of observations is not changing in unexpected ways.  A
       good way to keep duplicate observations out in sas is to use
       the "select distinct" option with proc sql.  However, this may
       not always eliminate duplicate observations.  I have found that
       the most effective methods of debugging are 1) looking at the
       data to make sure your code is working correctly and 2) reading
       the log file and paying close attention to the number of
       observations.

**********************************************************************;
       See the _mycstlink_example.sas for an illustration of how to
       use this macro.
**********************************************************************;
**********************************************************************;
*/ 

%macro mycstlink(preslack=180,slack=180,exclude_dual_class=1);

%* in case slack is ever set to null;
%if &preslack= %then %let preslack=0;
%if &slack= %then %let slack=0;

%* this is the same as ccmxpf_linktable;
proc sql;
    create table cstlink0 as
	select h.*, u.usedflag
	from crspccm.ccmxpf_lnkhist h left join crspccm.ccmxpf_lnkused u
	on (h.gvkey=u.ugvkey) 
	and (h.lpermno=u.upermno) 
	and (h.lpermco=u.upermco) 
	and (h.linkdt=u.ulinkdt) 
	and (h.linkenddt=u.ulinkenddt)
	and (h.liid=u.uiid);

data cstlink1;
    set cstlink0;
    if usedflag=1;
    if linkenddt=.E then linkenddt=today();
    gvkey_liid = gvkey || liid;
run;
%* In the new Compustat database, the variable iid is used;
%* to match gvkeys to securities.  So the CCM database adds;
%* iid in the gvkey-permno match.  I create the variable gvkey_iid;
%* by concatenating gvkey and iid and use it instead of gvkey;

%* --------------------------------------------------------------------------------;
%* Apply slack;
%* Since cst/crsp is a many-to-many merge, need find the maximum amount of slack;
%* allowed going from CRSP--->Compustat and from Compustat---->CRSP;

proc sort data=cstlink1; 
    by LPERMNO linkdt linkenddt; 
run;

data cstlink1;
    set cstlink1;
    lpermnom1=lag(lpermno);
    linkdtm1 =lag(linkdt);
    linkenddtm1 =lag(linkenddt);

    %* if there is no permno before;
    if lpermno ne lpermnom1 then max_preslack_permno = 90000;

    %* there is a permno before but the previous permno-gvkey link;
    %* is outside the slack range;
    else max_preslack_permno = intck('DAY',linkenddtm1,linkdt)-1;
    %* this is complicated by the fact that multiple gvkey_iids can link to the same permno;
run;

proc sort data=cstlink1; 
    by LPERMNO descending linkdt descending linkenddt; 
run;

data cstlink1;
    set cstlink1;
    if lpermno ne .;
    lpermnop1=lag(lpermno);
    linkdtp1 =lag(linkdt);
    linkenddtp1 =lag(linkenddt);

    if lpermno ne lpermnop1 then max_slack_permno = 90000;
    else max_slack_permno = intck('DAY',linkenddt,linkdtp1)-1;

    drop lpermnom1 lpermnop1 linkdtm1 linkdtp1 linkenddtm1 linkenddtp1;
run;


%*** now sort by linkenddt.  This is precautionary, and should only apply;
%*** if there are overlapping ranges.  It may not be necessary;
proc sort data=cstlink1; 
    by LPERMNO linkenddt linkdt; 
run;

data cstlink1;
    set cstlink1;
    lpermnom1=lag(lpermno);
    linkdtm1 =lag(linkdt);
    linkenddtm1 =lag(linkenddt);

    %* if there is no permno before;
    if lpermno ne lpermnom1 then max_preslack_permno_2 = 90000;

    %* there is a permno before but the previous permno-gvkey link;
    %* is outside the slack range;
    else max_preslack_permno_2 = intck('DAY',linkenddtm1,linkdt)-1;
run;

proc sort data=cstlink1; 
    by LPERMNO descending linkenddt descending linkdt;
run;

data cstlink1;
    set cstlink1;
    if lpermno ne .;
    lpermnop1=lag(lpermno);
    linkdtp1 =lag(linkdt);
    linkenddtp1 =lag(linkenddt);

    if lpermno ne lpermnop1 then max_slack_permno_2 = 90000;
    else max_slack_permno_2 = intck('DAY',linkenddt,linkdtp1)-1;

    drop lpermnom1 lpermnop1 linkdtm1 linkdtp1 linkenddtm1 linkenddtp1;
run;

%* ----------------------------------------------------------------------;
%* Next, sorting by gvkey_liid;

proc sort data=cstlink1; 
    by gvkey_liid linkdt linkenddt; 
run;

data cstlink1;
    set cstlink1;
    lpermnom1=lag(LPERMNO);
    gvkey_liidm1  =lag(gvkey_liid);
    linkenddtm1=lag(linkenddt);

    if gvkey_liid ne gvkey_liidm1 then max_preslack_gvkey_liid = 90000;
    else max_preslack_gvkey_liid = intck('DAY',linkenddtm1,linkdt)-1;
run;

proc sort data=cstlink1; 
    by gvkey_liid descending linkdt descending linkenddt; 
run;

data cstlink1;
    set cstlink1;
    lpermnop1=lag(LPERMNO);
    gvkey_liidp1  =lag(gvkey_liid);
    linkdtp1 =lag(linkdt);

    if gvkey_liid ne gvkey_liidp1 then max_slack_gvkey_liid = 90000;
    else max_slack_gvkey_liid = intck('DAY',linkenddt,linkdtp1)-1;

    drop lpermnop: gvkey_liidp: linkdtp: lpermnom: gvkey_liidm: linkenddtm:;
run;

%* ----------------------------------------------------------------------;
%* Next, sorting by gvkey only;

proc sort data=cstlink1; 
    by gvkey linkdt linkenddt;
run;

data cstlink1;
    set cstlink1;
    lpermnom1=lag(LPERMNO);
    gvkeym1  =lag(gvkey);
    linkenddtm1=lag(linkenddt);

    if gvkey ne gvkeym1 then max_preslack_gvkey = 90000;
    else max_preslack_gvkey = intck('DAY',linkenddtm1,linkdt)-1;
run;

proc sort data=cstlink1; 
    by gvkey descending linkdt descending linkenddt; 
run;

data cstlink1;
    set cstlink1;
    lpermnop1=lag(LPERMNO);
    gvkeyp1  =lag(gvkey);
    linkdtp1 =lag(linkdt);

    if gvkey ne gvkeyp1 then max_slack_gvkey = 90000;
    else max_slack_gvkey = intck('DAY',linkenddt,linkdtp1)-1;

    drop lpermnop: gvkeyp: linkdtp: lpermnom: gvkeym: linkenddtm:;
run;


%* ----------------------------------------------------------------------;
%* Finally, add new link date variables;

data mycstlink;
    format slinkdt_iid slinkenddt_iid extlinkdt_iid extlinkenddt_iid slinkdt slinkenddt extlinkdt extlinkenddt date9.;
    set cstlink1;

    %* slack for sort by gvkey_liid;
    preslack = max(0,min(max_preslack_permno,max_preslack_permno_2,max_preslack_gvkey_liid));
    slack = max(0,min(max_slack_permno,max_slack_permno_2,max_slack_gvkey_liid));
    %* there may be cases where max_slack and max_preslack are negative;
    %* They are likely cases where there are overlapping (incorrect) date ranges;
    %* in CRSP.  In these cases, I leave the date ranges alone;
    
    %* add slack to date range;
    slinkdt_iid=intnx('DAY',linkdt,-min(abs(&preslack),preslack));
    slinkenddt_iid=intnx('DAY',linkenddt,min(&slack,slack));

    %* extreme range.  If it is the first permno and gvkey_liid then set the;
    %* range to 1/1/1925 to today;
    extlinkdt_iid = intnx('DAY',linkdt,-preslack);
    extlinkenddt_iid=intnx('DAY',linkenddt,slack);

    if slinkdt_iid < mdy(1,1,1925) then slinkdt_iid=mdy(1,1,1925);
    if slinkenddt_iid > today() then slinkenddt_iid = today();    
    if extlinkdt_iid < mdy(1,1,1925) then extlinkdt_iid=mdy(1,1,1925);
    if extlinkenddt_iid > today() then extlinkenddt_iid = today();    

    %* slack for sort by gvkey;
    preslack2 = max(0,min(max_preslack_permno,max_preslack_permno_2,max_preslack_gvkey));
    slack2 = max(0,min(max_slack_permno,max_slack_permno_2,max_slack_gvkey));
    %* there may be cases where max_slack and max_preslack are negative;
    %* They are likely cases where there are overlapping (incorrect) date ranges;
    %* in CRSP.  In these cases, I leave the date ranges alone;
    
    %* add slack to date range;
    slinkdt=intnx('DAY',linkdt,-min(abs(&preslack),preslack2));
    slinkenddt=intnx('DAY',linkenddt,min(&slack,slack2));
    extlinkdt = intnx('DAY',linkdt,-preslack2);
    extlinkenddt=intnx('DAY',linkenddt,slack2);

    if slinkdt < mdy(1,1,1925) then slinkdt=mdy(1,1,1925);
    if slinkenddt > today() then slinkenddt = today();    
    if extlinkdt < mdy(1,1,1925) then extlinkdt=mdy(1,1,1925);
    if extlinkenddt > today() then extlinkenddt = today();    

    if &exclude_dual_class=1 then do;
        if linkprim in ("J" "N") then delete;
        end;

    drop max_: gvkey_liid usedflag slack: preslack:;
run;
    
proc sort data=mycstlink;
    by gvkey liid linkdt;
run;

%mend mycstlink;
