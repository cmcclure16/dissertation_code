function [ result ] = implications(d, params, Rmat, xmat, simDat, audMat, shocks, real_data)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Function Name: 	implications                                                                       %
% Date created: 	2018-10-30                                                                          %
% Author: 			Charles McClure                                                                     %
% Purpose: 			Computes different implications from estimates                                         %
%------------------------------------------------------------------------------------------------------ %
% Inputs:  d - dataset with parameters not estimated                                                    %
%          params - vector of parameters                                                                %
%          Rmat - a matrix of beginning reserves that is the same dimensionality as the state space     %
%          simDat - a matrix of simulated data where column 1 (2) is R (x) for each NxS observation    %
%          audMat - a matrix random numbers that set whether the firm is audited                        %
%          shocks - a matrix random profitability shocks unrelated to tax avoidance                        %
%          real_data - a structure of real data %
% Outputs: result - a structure with all of the implications from the optimal parameter values % 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% TASK: 	Simulate data with the optimized parameters
%                                                                                                       
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

final_data = simulate_data(d, params, Rmat, xmat, simDat, audMat, shocks);


% Split out columns into variables
R = final_data( : , 2 );
res = final_data( : , 3 );
preTax = final_data( : , 4 );
audits = final_data( : , 5 );
auditRate = final_data( : , 6 );
lapse = final_data( : , 7 );
x = final_data( : , 8 );
tau = final_data( : , 9 );

% Create a a structure for the parameter values
parm.alpha = params( 1 );
parm.eta = params( 2 );
parm.p = params( 3 );
parm.theta = params( 4 );
parm.phi = params( 5 );
parm.beta = params( 6 );

% Compute lagged assets for each simulated observation. Used to compute
% dollar amounts
at = repelem( real_data.at_lag , d.S );

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% TASK: 	Determine tax rate summary statistics                                                   %
%                                                                                                       %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

result.tau = mean( tau );
result.tauMedian = median( tau );



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% TASK: 	Determine non-tax costs                                                     %
%                                                                                                       %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%NTC for each simulated observation
ntc = parm.phi.*(0.35 - tau ); 

% Summary statistics of NTC as a percent of pre-tax profit
result.ntc = mean( ntc );
result.ntcMedian = median( ntc );

% Summary statistics of NTC in dollars
result.ntcDollar = mean( x.* ntc .* at / 100 );
result.ntcMedianDollar = median( x.*(1 - ntc) .* at / 100 );


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% TASK: 	Reported reserves compared to expected repayment amount %
%                                                                                                       %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Compute expected repayment amount
parm.omtheta = 1.0 - parm.theta;
parm.alphap1 = 1.0 + parm.alpha;
parm.alpha35 = parm.alpha * 0.35;
mu = (tau/0.35) .^ params(1);
underlyingout = x .* ( 1 - parm.phi .* 0.35 + parm.phi .* tau ) .* ( ( parm.alpha35 + tau .* ( mu - parm.alphap1 ) ) ./ parm.alphap1 );


% Compute ratio of reserve to expected repayment amount
result.reserveToActual = mean( res ./ underlyingout );
result.reserveToActualMedian = median( res ./ underlyingout );


% Summary statistics of differences in dollars
result.reserveToActualDollar = mean( ( underlyingout - res ) .* at / 100  );
result.reserveToActualMedianDollar = median( ( underlyingout - res ) .* at / 100 );




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% TASK: 	Reported reserves compared to expected repayment amount including audits and inspections %
%                                                                                                       %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Compute the expected repayment amount with audits and inspections
numYears = 1 / parm.theta; %Number of years on the books is 1 / lapse rate for unaudited firms
probSurvive = (  1 - inspection_rate( parm , R , res )  .* parm.p)  .^ numYears; %Probability 1 dollar of tax risk survives over the auditable period
expectRepay = probSurvive .* underlyingout;


% Compute the expected repayment amount with audits and inspections
result.reserveToExpect = mean( res ./ expectRepay);
result.reserveToExpectMedian = median( res ./ expectRepay);


% Summary statistics of differences in dollars
result.reserveToExpectDollar = mean( ( expectRepay - res ) .* at / 100 );
result.reserveToExpectMedianDollar = median( ( expectRepay - res ) .* at / 100 );



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% TASK: 	Total Reserves to expected repayments                                                    %
%                                                                                                       %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Compute the amount (in percent of total assets) of total R that is not
% reserved
result.totalReserveToActual = mean( R .* (underlyingout ./ res - 1.0) );
result.totalReserveToActualMedian = median( R .* (underlyingout ./ res - 1.0) );

result.totalReserveToActualDollar = mean( R .* (underlyingout ./ res - 1.0) .* at / 100 );
result.totalReserveToActualMedianDollar = median( R .* (underlyingout ./ res - 1.0) .* at / 100 );




% Compute the amount (in percent of total assets) of total R that is not
% reserved when we include audits and inspections
result.totalReserveToExpect = mean( R .* (1.0 - expectRepay ./ res) );
result.totalReserveToExpectMedian = median( R .* (1.0 -  expectRepay ./ res ) );

result.totalReserveToExpectDollar = mean( R .* (1.0 - expectRepay ./ res) .* at / 100);
result.totalReserveToExpectMedianDollar = median( R .* (1.0 - expectRepay ./ res) .* at / 100 );


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% TASK: 	Increase in NI from engaging in tax avoidance %
%                                                                                                       %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

netIncomeNoTA = x .* ( 1 - 0.35); 
netIncomeTA = preTax .* ( 1 - tau);

% Compute summary statistics of increase in Net Income
result.benefitToNI = mean( (netIncomeTA - netIncomeNoTA ) ./ netIncomeNoTA );
result.benefitToNIMedian = median( (netIncomeTA - netIncomeNoTA ) ./ netIncomeNoTA );


% Compute summary statistics of increase in ROA
result.benefitToNIAT = mean( (netIncomeTA - netIncomeNoTA ) );
result.benefitToNIATMedian = median( (netIncomeTA - netIncomeNoTA ) );


% Compute summary statistics of increase in Net Income in Dollars
result.benefitToNIDollars = mean( (netIncomeTA - netIncomeNoTA ) .* at / 100 );
result.benefitToNIMedianDollars = median( (netIncomeTA - netIncomeNoTA ) .* at / 100 );




end