function [ momentVector ] = momentvec(d, data)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Function Name: 	momentvec                                                                           %
% Date created: 	2018-10-08                                                                          %
% Author: 			Charles McClure                                                                     %
% Purpose: 			Computes the moments used in the estimation                                         %
%------------------------------------------------------------------------------------------------------ %
% Inputs:  d - dataset with parameters not estimated                                                    %
%          data - matrix of data
% Outputs: momentVector - A vector of moments                                                           %
%------------------------------------------------------------------------------------------------------ %
% Notes: If you make any changes to the moments, NEED to ensure the ordering of the influence function  %
%        matches the moments listed here. The moments are (means are assumed unless otherwise stated):  %
% 1. Reserve                                                                                         %
% 2. Audit Rate                                                                                         %
% 3. Pre-tax Income                                                                                         %
% 4. Res / PI
% 5. Lapse Rate - NA                                                                   %
% 6. Lapse Rate - A                                                                   %
% 7. Settle Rate - A                                                                   %
% 8. Settle Amt - A                                                                   %
% 9. Variance of Res 
% 10. Covariance of Res and PI
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Pre-define output vector - set to the number of moments
momentVector = NaN([6 1]);

% Extract data into individual variables from data matrix
% sim = data(:,1); %Simulation number
res = data(:,3); %Current period reserve
pre_tax = data(:,4); %Current period reserve
audit = data(:,5); %0/1 indicator for whether the firm is audited




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% TASK: 	Compute Means for pooled data                                                               %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% 1. Reserve
momentVector(1) = mean( res );

% 2. Audit Rate
momentVector(2) = mean( audit );

% 3. Pre-tax Income
momentVector(3) = mean( pre_tax );


% 4 Reserve / Pre-tax Income
momentVector(4) = mean( res ./ pre_tax );

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% TASK: 	Compute Means for subsets of data                                                           %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%Split data into whether it is audited or not;
dataNA = data(audit==0,:);
dataA = data(audit==1,:);


% 5. Lapse Rate - Not Audited Firms 
lapseNA = dataNA(:, 7);
momentVector(5) = mean( lapseNA );


% 6. Lapse Rate - Audited Firms 
settleA= dataA(:, 6);
settleAmtA = settleA.*(dataA( : , 3) + dataA( : , 2 ));
lapseA = dataA(:, 7);
momentVector(6) = mean( lapseA );


% 7 Settle Rate  
momentVector(7) = mean( settleA );

% 8 Settle Amount 
momentVector(8) = mean( settleAmtA );


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% TASK: 	Compute Variances for pooled data                                                           %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% % Preallocate temporary variance matrix that holds variance for each iteration
% tempVar = NaN([d.S, 3]);
% tempCov = cov(res , pre_tax );
% 
% 
% % %Split data into simulations so I can sum variances over them
% % for ii=1:d.S
% %     tempData = data(sim==ii,:);
% %     resTemp = tempData(:,3);
% %     pre_taxTemp = tempData(:,4);
% % 
% %     
% %     tempCov = cov(resTemp , pre_taxTemp );
% %     tempVar(ii, 1) = tempCov( 1, 1);
% %     tempVar(ii, 2) = tempCov( 2, 2);
% %     tempVar(ii, 3) = tempCov(1,2);
% % end
% 
% % 9. Variance of  res 
% momentVector(9) = tempCov( 1 , 1 );
% 
% 
% % 10. Covariance of PI and res
% momentVector(10) = tempCov( 1 , 2 ) ;

end


