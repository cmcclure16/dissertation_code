
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Program Name: 	UTB_Estimation                                                                      %
% Date created: 	2018-09-05                                                                          %
% Author: 			Charles McClure                                                                     %
% Purpose: 			Estimates model parameters for structural tax paper                                 %
%------------------------------------------------------------------------------------------------------ %
% Notes: 			functions are defined in folder "functions"                                         %
%                                                                                                       %
%--------------------------------------------------------------------------------------------------     %
% Last Modified: 	2018-09-14                                                                          %
% Modified by: 	Charles McClure                                                                         %
% Description: 	Initialize variables and prepare for simulations                                        %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% TASK: 	Set up system for evalulation                                                               %
%                                                                                                       %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%Clear data
close all;
clear; clc;

%Create a log file
diary
diary('log_file.txt')


%Determine paralleleization
parpool('local',18)

%Set working directory
cd 'P:\My Documents\Research\Dissertation\results'


%Link folder with all functions
addpath('P:\My Documents\Research\Dissertation\code\Analysis\functions')
addpath('P:\My Documents\Research\Dissertation\code\Analysis\functions\Influence Functions')
addpath('P:\My Documents\Research\Dissertation\code\Analysis\functions\counterfactuals')
addpath('P:\My Documents\Research\Dissertation\code\Analysis\functions\tax_change')
addpath('P:\My Documents\Research\Dissertation\code\analysis\diagnostic')



%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% TASK: 	Initialize Parameters. All inputs are set here                                              %
%                                                                                                       %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%Initialize parameters.
alpha = 1.5;
eta = .1;
p = 0.5;
theta = 0.085;
phi = 1.1;
beta = 0.75;


% Create these into a vector (for optimization) and a dataset
params = [alpha eta p theta phi beta];
parm.alpha = alpha;
parm.eta = eta;
parm.p = p;
parm.theta = theta;
parm.phi = phi;
parm.beta = beta;


% Create bounds for parameters
alphalb = 0.5;
alphaub = 3.5;
etalb = 0.04;
etaub = 0.48;
plb = 0.01;
pub = 0.99;
thetalb =0.01;
thetaub = 0.99;
philb = 0.75;
% philb = 0.25;
phiub = 1.53;
betalb = 0.5;
betaub = 1.25;

lb = [alphalb etalb plb thetalb philb betalb];
up =[alphaub etaub pub thetaub phiub betaub];


% Set number of iterations for varying parameter in diagnostic testing;
% d.numParamValues = 50;
% d.sections = 5;
% d.power = 4;


% Set additional parameters that don't change
d.delta = 0.9; %Discount rate
d.node2 = 500; %Number of points for integration of r(t_t-1) 
d.pts = 13; %Number of grid points in state space
d.pts2 = 7; %Number of grid points in x dimesion
d.tol = 1e-5; % Set tolerance
d.maxit = 1000; % Maximum iterations in policy iteration
d.delta= 0.9;
d.S = 20; %Number of simulations for each observation
d.T = 50; %Number of time periods for the burn-in period in simulation
d.tMax = 0.34; %Max value tau can take
d.tMin = 0.05; %Max value tau can take
d.epsilon = 0.01; %How much (in percentage) to perterb values in standard error calculation


% Set parameters for numerical integration of E(x)
d.xmin = 1.1*.8; %80% of lower bound of reported PI
d.xmax = 30*1.2; %120% of upper bound of reported PI
d.nodes = 7; %Number of nodes for Gauss Hermite integration
[y, w] = GaussHermite_2(d.nodes);
% d.beta0 = 2.20; % Intercept of AR1 process
% d.beta0 = 2.25; % Intercept of AR1 process
d.beta1 = 0.684; % Persistence of AR1 process from Hennessy and Whited 2007
d.sig = 0.118; % Std Dev of Error term of AR1 process from Hennessy and Whited 2007
d.z = exp(sqrt(2)*d.sig*y); %Change of variables
d.w = w./sqrt(pi); % Adjust weights by the Jacobian so I don't have to deal with it later


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% TASK: 	Set options for optimations                                                     %
%                                                                                                       %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Set options for optimizations
global seed options

% Optimization inside the value function iteration
options = optimoptions(@fmincon,'MaxFunEvals',1000,'MaxIter',1000, 'Display', 'off', 'Algorithm',...
                       'sqp','Diagnostics','off');
                   
% Simulated anneal - used to get close to optimum. For more information,
% see https://www.mathworks.com/help/gads/how-simulated-annealing-works.html
sa_options = optimoptions(@simulannealbnd,'MaxIterations',10000, 'Display', 'iter', 'FunctionTolerance',1e-5);
sa_options.ReannealInterval = 25; %Set a lower reannealing value to ensure we have found the global minima
sa_options.InitialTemperature = 20000*((lb + up)./2 - lb); %Set temperature to be roughly 20000x the size of the paramater space. Should be proportional to the size of objective function



% Fminseach to get the final value
fminseachOpts=optimset('MaxIter',10000,'Display','iter');


% Set seed
seed = 2018;
stream = RandStream('mt19937ar','Seed',seed);
RandStream.setGlobalStream(stream);
    

%Turn off warnings for inveting beta
warning('off','stats:betainv:NoConvergence')
warning('off','MATLAB:xlswrite:AddSheet')
warning off all;

 
%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% TASK: 	Load data and set up matrices of data                                                       %
%                                                                                                       %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Load data
real_data = readtable('P:\My Documents\Research\Dissertation\data\final\estimation_data.csv');
% real_data.pi = real_data.pi/100;
% real_data.res = real_data.res/100;
% real_data.R = real_data.R/100;
% Create matrices of data
real_mat = [real_data.fyear real_data.R real_data.res  real_data.pi  real_data.audit real_data.settle real_data.lapse];             


% Create the state variables for all observations
states = real_data.R;



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% TASK: 	Compute moments with Actual Data                                                                %
%Notes: If there are ANY changes to the moments, this and the influence
%matrix must be adjusted and ensured to match in order. Moments are:
%                                                                                                       %
% 1. Reserve                                                                                         %
% 2. Audit Rate                                                                                         %
% 3. Pre-tax Income                                                                                         %
% 4. Res / PI
% 5. Lapse Rate - NA                                                                   %
% 6. Lapse Rate - A                                                                   %
% 7. Settle Rate - A                                                                   %
% 8. Settle Amt - A                                                                   %
% 9. Variance of Res 
% 10. Covariance of Res and PI
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% To use the same moment function with actual data, first set a temporary variable equal to the simulation
temp = d.S;
d.S = 1;
real_mat( : , 1) = 1;

% Compute moments
momActual = momentvec(d, real_mat);
    
% Reset simulation number so I can use the moment function with simulated
% data
d.S = temp;



%% 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% TASK: 	Create grid of state spaces                                                                 %
%                                                                                                       %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Grid is not uniform but is set at the quantiles at [0, 10, 20..., 100%]
% The grid dimensions are:
% 1. Reported reserve (R)
% 2. Core earnings
Rvec = zeros([1, d.pts]);
xvec = zeros([1, d.pts2]);


Rvec(1) = quantile(states(:,1), 0)/10;

quants = linspace(0, 0.95, d.pts-1); %Split up the non-winsorized space into quantiles
for ii = 2:d.pts-1
    Rvec(ii) = quantile(states(:,1),quants(ii));
end
Rvec(d.pts) = max(states(:,1))*1.5;

% Set mins and max for R
d.minR = min(Rvec);
d.maxR = max(Rvec);


xvec = linspace(d.xmin, d.xmax, d.pts2);



% Create a grid of state space
[Rmat, xmat] = ndgrid(Rvec,xvec);




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% TASK: 	Compute influence functions                                                                  %
%                                                                                                       %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Compute additional data like ratios
real_data = additional_data(real_data);

% Compute all possible influence functions
inflAll = influence(real_data);



 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% TASK: 	Select influence functions corresponding to the moments used and compute weight matrix                                                            %
%                                                                                                       %
%Notes: If there are ANY changes to the moments, this and the momentvec
%function must be adjusted and ensured to match in order. Moments are:
%                                                                                                       %
% 1. Reserve                                                                                         %
% 2. Audit Rate                                                                                         %
% 3. Pre-tax Income                                                                                         %
% 4. Res / PI
% 5. Lapse Rate - NA                                                                   %
% 6. Lapse Rate - A                                                                   %
% 7. Settle Rate - A                                                                   %
% 8. Settle Amt - A                                                                   %
% 9. Variance of Res 
% 10. Covariance of Res and PI
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


infl = [inflAll.res ... 1. Reserve 
        inflAll.audit ... 2. Audit Rate
        inflAll.pi ... 3. Pre-tax Income
        inflAll.resPi ... 4. Reserve to Pre-tax Income
        inflAll.lapseNA... 5. Lapse Rate - Not Audited
        inflAll.lapseA... 6. Lapse Rate - Audited
        inflAll.settleA ... 7. Settle Rate Audited
        inflAll.settleAmtA ... 8. Settle Rate Audited
    ];


N = length(infl(:,1));
W = inv((1/N^2)*(infl'*infl));


%% 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% TASK: 	Set up simulated data                                                                 %
%                                                                                                       %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

states = NaN([length(real_data.R) 2]);

%State space matrix
states( : , 1 ) = real_data.R;
states( : , 2 ) = real_data.pi;

% rng(2018,'twister');
% states( : , 1 )  = rand( [1000 1] ) * ( max( real_data.R ) - min( real_data.R ) ) + min( real_data.R );
% states( : , 2 )  =  rand( [1000 1] ) * ( d.xmax  - d.xmin ) + d.xmin;


% Simulate Data
Rsim = repelem(states(:,1) , d.S);
xsim = repelem( states( : , 2) , d.S );
simDat = [Rsim xsim];


% Set matrix of random numbers that will determine the audit outcomes;
NS = length(simDat);
rng(2018,'twister');
audMat = rand([NS (d.T +1)]);

% Set matrix of profitability shocks. This can be computed outside. Note
% that if x goes outside of the bounds of xmat, it is pulled back in
rng(2018,'twister');
shocks = exp(normrnd(0,d.sig, [NS d.T + 1]));


% % Here until the end for simulated data
% xnew = NaN(size(audMat));
% for ii = 1:d.T
%     
%         if ii == 1       
%             tempX = exp( beta ) .* xsim.^d.beta1 .* shocks( : , ii);        
%         else
%             tempX = exp( beta ) .* xnew( : , ii - 1).^d.beta1 .* shocks( : , ii);       
%         end
% 
%         %     Define the vector of xnew. If the expectation of xt+1 will cause
%         %     it to be out of the bounds of xmat, pull back in
%         xnew( : , ii ) = max( min( tempX , d.xmax/d.z(d.nodes) ) , d.xmin/d.z(1) );
%     
% end
% histogram(xnew(:,d.T))
% 
% % Create a matrix of simulated data
% real_mat =  simulate_data(d, params, Rmat, xmat, simDat, audMat , shocks);
% 
% momActual = momentvec(d, real_mat);


%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% TASK: 	Find optimal parameter through simulated annealing  and Nelder Meade                                                              %
%                                                                                                       %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Set seed
stream = RandStream('mt19937ar','Seed',seed);
% stream.State = output.rngstate.State;
RandStream.setGlobalStream(stream);

% Define objective function
Obj = @(params) SMM( params, d, momActual, Rmat, xmat, simDat, audMat, shocks, W);

% Obj(params)
% Run the simulated annealing program


tic;
[params2,fvalSA, exitflagSA]  = simulannealbnd(Obj,params, lb, up, sa_options );
toc;

simfile = 'params_SA.csv';
dlmwrite(simfile, params2);

fprintf('Simaulted Annealing Function Value: %i \n', fvalSA)
fprintf('Simaulted Annealing Exit flag: %i \n', exitflagSA)




% Finish off estimation with Nelder Meade to get at the local minimum
[params3,fval, exitflag, output]  = fminsearch(Obj,params2, fminseachOpts);

simfile = 'params.csv';
dlmwrite(simfile, params3);

fprintf('Nelder Meade Function Value: %i \n', fval)
fprintf('Nelder Meade Exit flag: %i \n', exitflag)



%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% TASK: 	Compute Standard Errors and moment sensitivity for parameters                                                             %
%                                                                                                       %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Load optimized parameter values
params3 = csvread('params.csv');

% Compute Standard errors;
[ covar , se , t_stat , lambda , lambda_std ] = SE( params3 , d , Rmat , xmat , simDat , audMat , shocks , W);


% Combine outputs into a data structura and save down
results.params = params3;
results.covar = covar;
results.se = se;
results.t_stat = t_stat;
results.lambda = lambda;
results.lambda_std = lambda_std;
save('results.mat','results');

% Combine parameter values with t-stats and standard errors
output = [ params3 ; se ; t_stat ];
simfile = 'params_se.csv';
dlmwrite( simfile , output );


%Save down standardized lambdas
simfile = 'lambda_std.csv';
dlmwrite( simfile , lambda_std );





%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% TASK: 	Compare simulated to actual moments                                                             %
%                                                                                                       %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% Set seed
seed = 2018;
stream = RandStream('mt19937ar','Seed',seed);
RandStream.setGlobalStream(stream);
 
% Compare moments
momentComparison = moment_comparison( params3, d, momActual, Rmat, xmat, simDat, audMat, shocks);


% Save output
simfile = 'moment_comparison.csv';
dlmwrite( simfile , momentComparison );



%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% TASK: 	Compute model implications
%                                                                                                       %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


params3 = csvread('params.csv');
% Compare moments
model_implications = implications(d, params3, Rmat, xmat, simDat, audMat, shocks, real_data);


% Save output
simfile = 'implications.csv';
summary_table = struct2table(model_implications);
writetable(summary_table, simfile)




%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% TASK: 	Compute the counterfactuals
%                                                                                                       
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%  Counterfactuals are "numbered" in that different values of counter
%  result in different counterfactuals. The list of counterfactuals:
%  counter | Descriptions
%  --------- |-------------------- 
%       1     | Reserve = Expected Repayment
%       2     | Reserve = Expected Repayment w/ audits               
%       3     | No FIN 48 reserves      
%       4     | NTC only
%       5     | TJCA Counterfactuals (see below)


% Reserve = Expected Repayment
expect_reserve = counterfactuals(d, params3, params3, Rmat, xmat, simDat, audMat, shocks, real_data, 1);
expect_reserve_table = struct2table(expect_reserve);
expect_reserve_table= [table(1, 'VariableNames', {'counterfactual'}) expect_reserve_table];


% Reserve = Expected Repayment w/ audits
expect_reserve_with_audits = counterfactuals(d, params3, params3, Rmat, xmat, simDat, audMat, shocks, real_data, 2);
expect_reserve_with_audits_table = struct2table(expect_reserve_with_audits);
expect_reserve_with_audits_table = [table(2, 'VariableNames', {'counterfactual'}) expect_reserve_with_audits_table];


% No FIN 48 Reserves
no_reserve = counterfactuals(d, params3, params3, Rmat, xmat, simDat, audMat, shocks, real_data, 3);
no_reserve_table = struct2table(no_reserve);
no_reserve_table = [table(3, 'VariableNames', {'counterfactual'}) no_reserve_table];


% NTC only
ntc_only = counterfactuals(d, params3, params3, Rmat, xmat, simDat, audMat, shocks, real_data, 4);
ntc_only_table = struct2table(ntc_only);
ntc_only_table = [table(4, 'VariableNames', {'counterfactual'}) ntc_only_table];


% Combine and output to CSV
counter_output = [expect_reserve_table ; expect_reserve_with_audits_table ; ...
                             no_reserve_table ; ntc_only_table];

simfile = 'counterfactuals.csv';
writetable(counter_output, simfile)



%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% TASK: 	Compute the counterfactuals for TCJA
%                                                                                                       
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Set ranges of alpha and phi (as a percent of estimated values)
alpha_head = [75 100 125];
phi_head = [75 100 125];


for ii = 1:length(alpha_head)
    
    for jj = 1:length(phi_head)
        
        % Compute the parameter values for the TCJA counterfactual
        param_counter = params3;
        param_counter( 1 ) = params3( 1 ) * alpha_head( ii ) / 100;
        param_counter( 5 ) = params3( 5 ) * phi_head( jj ) / 100;

        % Compute the counterfactual amounts
        tcja_table = counterfactuals(d, params3, param_counter, Rmat, xmat, simDat, audMat, shocks, real_data, 5);
        tcja_table2 = struct2table( tcja_table );

        % Save counterfactual down
        name = strcat( 'TCJA' , num2str( alpha_head( ii ) ) ,  num2str(phi_head( jj )));
        tcja.(name) = [ table(alpha_head( ii ) ,  'VariableNames', {'alpha'}) , table(phi_head( jj ) , 'VariableNames', {'phi'}) tcja_table2];
        
        if ii ==1 && jj ==1
        tcja_output = tcja.(name);
        else
        tcja_output = [tcja_output ; tcja.(name)];
        end
    end
    
end

% Save down output
simfile = 'counterfactuals_tcja.csv';
writetable( tcja_output , simfile )





%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% TASK: 	Estimate parameter values and implications for multi-nationals
%                                                                                                       
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Filter data to keep the desired subset
subset_obs = real_data( real_data.for_ind ==1, : );

% Create the matrix for estimation and state variable
subset_mat = [subset_obs.fyear subset_obs.R subset_obs.res  subset_obs.pi  subset_obs.audit subset_obs.settle subset_obs.lapse];             
states = subset_obs.R;

% To use the same moment function with actual data, first set a temporary variable equal to the simulation
temp = d.S;
d.S = 1;
subset_mat( : , 1) = 1;

% Compute moments
momActual = momentvec(d, subset_mat);
    
% Reset simulation number so I can use the moment function with simulated
% data
d.S = temp;


% Create Grid
Rvec = zeros([1, d.pts]);
xvec = zeros([1, d.pts2]);


Rvec(1) = quantile(states(:,1), 0)/10;

quants = linspace(0, 0.95, d.pts-1); %Split up the non-winsorized space into quantiles
for ii = 2:d.pts-1
    Rvec(ii) = quantile(states(:,1),quants(ii));
end
Rvec(d.pts) = max(states(:,1))*1.5;

% Set mins and max for R
d.minR = min(Rvec);
d.maxR = max(Rvec);


xvec = linspace(d.xmin, d.xmax, d.pts2);



% Create a grid of state space
[Rmat, xmat] = ndgrid(Rvec,xvec);


% Compute additional data like ratios
subset_obs = additional_data(subset_obs);

% Compute all possible influence functions
inflAll = influence(subset_obs);

infl = [inflAll.res ... 1. Reserve 
        inflAll.audit ... 2. Audit Rate
        inflAll.pi ... 3. Pre-tax Income
        inflAll.resPi ... 4. Reserve to Pre-tax Income
        inflAll.lapseNA... 5. Lapse Rate - Not Audited
        inflAll.lapseA... 6. Lapse Rate - Audited
        inflAll.settleA ... 7. Settle Rate Audited
        inflAll.settleAmtA ... 8. Settle Rate Audited
    ];


N = length(infl(:,1));
W = inv((1/N^2)*(infl'*infl));

% Set up simulated data
states = NaN([length(subset_obs.R) 2]);

%State space matrix
states( : , 1 ) = subset_obs.R;
states( : , 2 ) = subset_obs.pi;

% Simulate Data
Rsim = repelem(states(:,1) , d.S);
xsim = repelem( states( : , 2) , d.S );
simDat = [Rsim xsim];


% Set matrix of random numbers that will determine the audit outcomes;
NS = length(simDat);
rng(2018,'twister');
audMat = rand([NS (d.T +1)]);

% Set matrix of profitability shocks. This can be computed outside. Note
% that if x goes outside of the bounds of xmat, it is pulled back in
rng(2018,'twister');
shocks = exp(normrnd(0,d.sig, [NS d.T + 1]));



% Set seed for optimzation
stream = RandStream('mt19937ar','Seed',seed);
RandStream.setGlobalStream(stream);

% Define objective function
Obj = @(params) SMM( params, d, momActual, Rmat, xmat, simDat, audMat, shocks, W);

tic;
[params2_mne,fvalSA, exitflagSA]  = simulannealbnd(Obj,params, lb, up, sa_options );
toc;

simfile = 'params_SA_mne.csv';
dlmwrite(simfile, params2_mne);



% Finish off estimation with Nelder Meade to get at the local minimum
[params3_mne,fval, exitflag, output]  = fminsearch(Obj,params2_mne, fminseachOpts);

simfile = 'params_mne.csv';
dlmwrite(simfile, params3_mne);


% Compute Standard errors;
[ covar_mne , se_mne , t_stat_mne , lambda_mne , lambda_std_mne ] = SE( params3_mne , d , Rmat , xmat , simDat , audMat , shocks , W);


% Combine parameter values with t-stats and standard errors
output = [ params3_mne ; se_mne ; t_stat_mne ];
simfile = 'params_se_mne.csv';
dlmwrite( simfile , output );


% Compute Model Implications
model_implications_mne = implications(d, params3_mne, Rmat, xmat, simDat, audMat, shocks, subset_obs);


% Save output
simfile = 'implications_mne.csv';
summary_table = struct2table(model_implications_mne);
writetable(summary_table, simfile)




%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% TASK: 	Estimate parameter values and implications for domestic firms
%                                                                                                       
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Filter data to keep the desired subset
subset_obs = real_data( real_data.for_ind ==0, : );

% Create the matrix for estimation and state variable
subset_mat = [subset_obs.fyear subset_obs.R subset_obs.res  subset_obs.pi  subset_obs.audit subset_obs.settle subset_obs.lapse];             
states = subset_obs.R;

% To use the same moment function with actual data, first set a temporary variable equal to the simulation
temp = d.S;
d.S = 1;
subset_mat( : , 1) = 1;

% Compute moments
momActual = momentvec(d, subset_mat);
    
% Reset simulation number so I can use the moment function with simulated
% data
d.S = temp;


% Create Grid
Rvec = zeros([1, d.pts]);
xvec = zeros([1, d.pts2]);


Rvec(1) = quantile(states(:,1), 0)/10;

quants = linspace(0, 0.95, d.pts-1); %Split up the non-winsorized space into quantiles
for ii = 2:d.pts-1
    Rvec(ii) = quantile(states(:,1),quants(ii));
end
Rvec(d.pts) = max(states(:,1))*1.5;

% Set mins and max for R
d.minR = min(Rvec);
d.maxR = max(Rvec);


xvec = linspace(d.xmin, d.xmax, d.pts2);



% Create a grid of state space
[Rmat, xmat] = ndgrid(Rvec,xvec);


% Compute additional data like ratios
subset_obs = additional_data(subset_obs);

% Compute all possible influence functions
inflAll = influence(subset_obs);

infl = [inflAll.res ... 1. Reserve 
        inflAll.audit ... 2. Audit Rate
        inflAll.pi ... 3. Pre-tax Income
        inflAll.resPi ... 4. Reserve to Pre-tax Income
        inflAll.lapseNA... 5. Lapse Rate - Not Audited
        inflAll.lapseA... 6. Lapse Rate - Audited
        inflAll.settleA ... 7. Settle Rate Audited
        inflAll.settleAmtA ... 8. Settle Rate Audited
    ];


N = length(infl(:,1));
W = inv((1/N^2)*(infl'*infl));

% Set up simulated data
states = NaN([length(subset_obs.R) 2]);

%State space matrix
states( : , 1 ) = subset_obs.R;
states( : , 2 ) = subset_obs.pi;

% Simulate Data
Rsim = repelem(states(:,1) , d.S);
xsim = repelem( states( : , 2) , d.S );
simDat = [Rsim xsim];


% Set matrix of random numbers that will determine the audit outcomes;
NS = length(simDat);
rng(2018,'twister');
audMat = rand([NS (d.T +1)]);

% Set matrix of profitability shocks. This can be computed outside. Note
% that if x goes outside of the bounds of xmat, it is pulled back in
rng(2018,'twister');
shocks = exp(normrnd(0,d.sig, [NS d.T + 1]));



% Set seed for optimzation
stream = RandStream('mt19937ar','Seed',seed);
RandStream.setGlobalStream(stream);

% Define objective function
Obj = @(params) SMM( params, d, momActual, Rmat, xmat, simDat, audMat, shocks, W);

tic;
[params2_dom,fvalSA, exitflagSA]  = simulannealbnd(Obj,params, lb, up, sa_options );
toc;

simfile = 'params_SA_dom.csv';
dlmwrite(simfile, params2_dom);



% Finish off estimation with Nelder Meade to get at the local minimum
[params3_dom,fval, exitflag, output]  = fminsearch(Obj,params2_dom, fminseachOpts);

simfile = 'params_dom.csv';
dlmwrite(simfile, params3_dom);


% Compute Standard errors;
[ covar_dom , se_dom , t_stat_dom , lambda_dom , lambda_std_dom ] = SE( params3_dom , d , Rmat , xmat , simDat , audMat , shocks , W);


% Combine parameter values with t-stats and standard errors
output = [ params3_dom ; se_dom ; t_stat_dom ];
simfile = 'params_se_dom.csv';
dlmwrite( simfile , output );


% Compute Model Implications
model_implications_dom = implications(d, params3_dom, Rmat, xmat, simDat, audMat, shocks, subset_obs);


% Save output
simfile = 'implications_dom.csv';
summary_table = struct2table(model_implications_dom);
writetable(summary_table, simfile)

