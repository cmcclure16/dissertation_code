function [ V1 ] = value_func_counter(d, parm, t, V, Rmat, R, xmat, x, r, counter)
  
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Function Name: 	value_func_counter                                                                          %
% Date created: 	2018-11-13                                                                          %
% Author: 			Charles McClure                                                                     %
% Purpose: 			Computes one step of the value function for counterfactuals                                            %
%------------------------------------------------------------------------------------------------------ %
% Notes: 			Can can either 1 element for a matrix of values. t, V, R must the same dimension    %
%------------------------------------------------------------------------------------------------------ %
% Inputs:  d - dataset with parameters not estimated                                                    %
%          parm - dataset with model parameters                                                         %
%          t - tax payment. Can be 1 element or a matrix                                                %
%          V - Value from prior iteration                                                               %
%          Rmat - Matrix of total Reserve                                                               %
%          R - Total Reserve for the particular data point. If this is a matrix, should be equal to Rmat%
%          r - dataset of current reserves computed from the function rho. Also includes the gridded    % 
%              data interpolant                                                                         %
%          counter - a value to indicate which counterfactual is used  
% Outputs: V1 - Update value. Will be the same size as t, V, R, and rp                                  %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% Compute reserve 
if( counter == 1)
    res = x.* underlying( parm , t );
    
elseif counter == 2

    res = x .*  reserve_expect( parm , R , t );
    
else
    
    res = x.*r.F(t);
    
end
    


% Calculate the audit probability
audit_risk  = (R + res);
audit_rate =  inspection_rate(parm, R, res);
        

% Calculate the current period net income
if counter == 3
    
    ni = x .* ( 1.0 - parm.phi * 0.35 +  parm.phi .* t ) .* ( 1.0 - t );
    
elseif counter == 4
    
    ni = x .* ( 1.0 - parm.phi * 0.35 +  parm.phi .* t ) .* ( 1.0 - t );
    
else
    
    ni = x .* ( 1.0 - parm.phi * 0.35 +  parm.phi .* t ) .* ( 1.0 - t ) - res;

end


% Compute new total Reserve for the audit branch
Rnew   = (1.0 - audit_rate).*(res + parm.omtheta.* R);
Rnew   = max( min( Rnew , d.maxR) , d.minR);

% Compute new total Reserve for the no-audit branch
Rnew0  = res + parm.omtheta * R;
Rnew0  = max(min(Rnew0, d.maxR),d.minR);


% Loop through each possible combination of x_{t+1} and compute the
% expected value for each branch
VtempA = NaN([size(x) d.nodes]);
VtempNA = VtempA;

for ii = 1:d.nodes    

    x1 = x .* d.z( ii );
    VtempA( : , : , ii )= d.w( ii ) * interp( Rmat , xmat , V , Rnew, x1 );
    VtempNA( : , : , ii )= d.w( ii ) * interp( Rmat , xmat , V , Rnew0, x1 );

end

V_aud = sum(VtempA, 3);
V_no_aud = sum(VtempNA, 3);        
        


% Tax risk for the current period
mu = (t/0.35) .^ parm.alpha;
underlyingout = x.*(1 - parm.phi.*0.35 + parm.phi.*t) .* ( ( parm.alpha35 + t .* ( mu - parm.alphap1 ) ) ./ parm.alphap1 );


% Ratio for tax risk to reserve for the current period. Used to approximate
% coverage for total tax risk
Ratio = underlyingout ./ res;


% Calculate the value function
omaudit = 1.0 - audit_rate;
Rtheta = parm.theta.*R;

if counter == 1

    V1 = ni +  d.delta * ( (1-parm.p) .* (V_no_aud + Rtheta) + parm.p .* ( V_aud + omaudit.*Rtheta) );
    
    
elseif counter == 3
    
    V1 = ni +  d.delta * ( (1-parm.p) .* (V_no_aud ) + parm.p .* ( V_aud + (audit_rate).* audit_risk .* (0.0 - Ratio) ) );
    
elseif counter == 4
    
    V1 = ni +  d.delta .* V_no_aud;
    
else
    
    V1 = ni +  d.delta * ( (1-parm.p) .* (V_no_aud + Rtheta) + parm.p .* ( V_aud + (audit_rate).* audit_risk .* (1.0 - Ratio) + omaudit.*Rtheta) );

end

end

