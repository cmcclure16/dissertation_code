function [ objective ] = SMM( params, d, momActual, Rmat, xmat, simDat, audMat, shocks, W)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Function Name: 	SMM                                                                       %
% Date created: 	2018-10-11                                                                          %
% Author: 			Charles McClure                                                                     %
% Purpose: 			Computes the objective function for a set of model paramters %
%------------------------------------------------------------------------------------------------------ %
% Inputs: params - vector of parameters                                                                %
%           d - dataset with parameters not estimated                                                    %
%          momActual - vector of actual moments     %
%          Rmat - a matrix of beginning reserves that is the same dimensionality as the state space     %
%          xmat - a matrix of current core earnings that is the same dimensionality as the state space     %
%          simDat - a matrix of simulated data where column 1 (2) is R (rp) for each NxS observation    %
%          audMat - a matrix random numbers that set whether the firm is audited                        %
%          shocks - a matrix that contains profitability shocks unrealted to tax avoidance                        %
%          W - a matrix random numbers that set whether the firm is audited                        %
% Outputs: objective - a single value of the objective function %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



%  Simulate data for the set of paramters                                                             
simulatedData = simulate_data(d, params, Rmat, xmat, simDat, audMat, shocks);

% Compute simulated moments
momSimulated = momentvec(d, simulatedData);

% Compute difference in moments                                                              
momDiff = momActual - momSimulated;

objective = momDiff'*W*momDiff;

end
