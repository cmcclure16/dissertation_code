function [ output ] = simulate_moments(d, data)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Function Name: 	simulate_moments                                                                    %
% Date created: 	2018-09-18                                                                          %
% Author: 			Charles McClure                                                                     %
% Purpose: 			Computes all possible moments for simulated data                                    %
%------------------------------------------------------------------------------------------------------ %
% Inputs:  d - dataset with parameters not estimated                                                    %
%          data - cell with parameters as the first element and simulated data as the second. Note      %
%                   that cell with sim data has first column as tau, second                             %
%                   as R and third as rp                                                                %
% Outputs: output - a table with the parameter of interest as the first column                          % 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% TASK: 	Extract parameters from first cell and create variables                                     %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

simDat = data{2};


% Extract data into individual variables from data matrix
sim = simDat(:,1);
R = simDat(:,2);
res = simDat(:,3);
preTax = simDat(:,4);
audit = simDat(:,5);
inspectRate = simDat(:,6);
lapse = simDat(:,7);

% rp = simDat(:,8);

% Compute additional amounts that may or may not be used in actual
% estimation
lapseAmt = lapse.*R.*(1 - audit.*inspectRate);
resPI = res ./ preTax;
inspectR = inspectRate.*R;
Rres = R + res;
inspectRres = inspectRate.*Rres;



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% TASK: 	Compute Means for pooled data                                                               %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
output.R = mean(R);
output.res = mean(res);
output.preTax = mean(preTax);
output.lapse = mean(lapse);
output.settle = mean(inspectRate);
output.settleAmt = mean(inspectRres);
output.lapseAmt = mean(lapseAmt);
output.resPI = mean(resPI);
output.PIRes = mean(preTax./res);
output.PIR = mean(preTax./R);
output.PIRRes = mean(preTax./(R + res));
output.audit = mean(audit);
output.inspectR = mean(inspectR);
output.resR = mean(res ./ R);



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% TASK: 	Compute Variances for pooled data                                                           %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Preallocate temporary variance matrix that holds variance for each
% iteration
tempVar = NaN([d.S, 20]);

%Split data into simulations so I can sum variances over them
for ii=1:d.S
    tempDat = simDat(sim==ii,:);
    resTemp = tempDat(:,3);
    preTaxTemp = tempDat(:,4);
    RTemp = tempDat(:,2);
    inspectTemp = tempDat(:,6);
    auditTemp = tempDat(:,5);
    lapseTemp = tempDat(:,7).*RTemp.*(1 - auditTemp.*inspectTemp);
    preTaxRTemp = preTaxTemp ./ RTemp;
    resRTemp = resTemp ./ RTemp;
    
    
    %Create a large covariance matrix     
    tempCov = cov([resTemp RTemp preTaxTemp inspectTemp lapseTemp preTaxRTemp resRTemp]);
    tempVar(ii, 1) = tempCov( 1 , 1 );
    tempVar(ii, 2) = tempCov( 1 , 2 );
    tempVar(ii, 3) = tempCov( 1 , 3 );
    tempVar(ii, 4) = tempCov( 1 , 4 );
    tempVar(ii, 5) = tempCov( 1 , 5 );
    tempVar(ii, 6) = tempCov( 2 , 3 );
    tempVar(ii, 7) = tempCov( 2 , 4 );
    tempVar(ii, 8) = tempCov( 2 , 5 );
    tempVar(ii, 9) = tempCov( 3 , 4 );
    tempVar(ii, 10) = tempCov( 3 , 5 );
    tempVar(ii, 11) = tempCov( 6 , 7 );
   
    
    tempVar(ii, 12) = var(preTaxTemp);
    tempVar(ii, 13) = var(log(resTemp));
    tempVar(ii, 14) = var(log(preTaxTemp));
    tempVar(ii, 15) = var(preTaxTemp ./ RTemp);
    tempVar(ii, 16) = var(preTaxTemp ./ resTemp);
    tempVar(ii, 17) = var(resTemp ./ preTaxTemp);
    tempVar(ii, 18) = var(resTemp ./ RTemp);
    
    % Skewnewss
    tempVar(ii, 19) = skewness(resTemp);
    tempVar(ii, 20) = skewness(resTemp ./ RTemp);
    
end

% Compute averages across all simulations
tempMean = mean(tempVar);
output.resVar = tempMean(1);
output.resRCov = tempMean(2);
output.resPreTaxCov= tempMean(3);
output.resSettleCov= tempMean(4);
output.resLapseCov= tempMean(5);
output.RPreTaxCov= tempMean(6);
output.RSettleCov= tempMean(7);
output.RLapseCov= tempMean(8);
output.PreTaxSettleCov= tempMean(9);
output.PreTaxLapseCov= tempMean(10);
output.preTaxRResRCov= tempMean(11);


output.preTaxVar = tempMean(12);
output.logResVar = tempMean(13);
output.logPretaxVar = tempMean(14);
output.preTaxRVar= tempMean(15);
output.preTaxResVar= tempMean(16);
output.resPreTaxVar= tempMean(17);
output.resRVar= tempMean(18);
output.resSkew= tempMean(19);
output.resRSkew= tempMean(20);



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% TASK: 	Compute moments using subsamples of data                                                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%Split data into whether it is audited or not;
simDatAud = simDat(audit==1,:);
simAud = simDatAud(:,1);
simDatNoAud = simDat(audit==0,:);
simNoAud = simDatNoAud(:,1);

% Compute means for subsamples
output.settleRateAud = mean(simDatAud(:,6));
output.settleAmountAud = mean(simDatAud(:,6).*(simDatAud(:,2) + simDatAud(:,3)));
output.lapseAud = mean(simDatAud(:,7));
output.lapseNoAud = mean(simDatNoAud(:,7));
output.lapseAmtNoAud = mean(simDatNoAud(:,7) .* simDatNoAud(:,2));


% Split data into simulations so I can sum variances over them
tempVarAud = NaN([d.S, 2]);
tempVarNoAud = NaN([d.S, 1]);

for ii =1:d.S
    % Compute variances for Audited Firms     
    tempDatAud = simDatAud(simAud==ii,:);
    resTemp = tempDatAud(:,3);
    preTaxTemp = tempDatAud(:,4);
    RTemp = tempDatAud(:,2);
    inspectTemp = tempDatAud(:,6);
    resRTemp = resTemp ./ RTemp;
    piRTemp = preTaxTemp ./ RTemp;
    tempCovAud = cov([resTemp RTemp preTaxTemp inspectTemp]);
    tempVarAud(ii, 1) = tempCovAud(4 , 4);
    tempVarAud(ii, 2) = tempCovAud(1 , 4);
    tempVarAud(ii, 3) = tempCovAud(2 , 4);
    tempVarAud(ii, 4) = tempCovAud(3 , 4);

    
    % Compute variances for Not Audited Firms     
    tempDatNoAud = simDatNoAud(simNoAud==ii,:);
    tempVarNoAud(ii, 1) = var(tempDatNoAud(:,7) .* tempDatNoAud(:,2));
end

%Compute means over simulations
tempMeanAud = mean(tempVarAud);
output.settleAudVar = tempMeanAud(1);
output.resSettleAudCov = tempMeanAud(2);
output.RSettleAudCov = tempMeanAud(3);
output.piSettleAudCov = tempMeanAud(4);


tempMeanNoAud = mean(tempVarNoAud);
output.lapseNoAudVar = tempMeanNoAud(1);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% TASK: 	Compute Moments using serial correlation                                                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% X = [ones([length(res) 1]) rp];
% beta = (X'*X)\X'*(res);
% output.serialIntercept = beta(1);
% output.serialSlope = beta(2);

end