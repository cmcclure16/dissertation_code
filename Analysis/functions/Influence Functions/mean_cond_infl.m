function [ infl ] = mean_cond_infl(x, dum)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Function Name: 	mean_cond_infl                                                                      %
% Date created: 	2018-10-04                                                                          %
% Author: 			Charles McClure                                                                     %
% Purpose: 			Computes the influence function for a conditional mean                              %
%------------------------------------------------------------------------------------------------------ %
% Inputs:  x - vector from which to compute the mean                                       %
%          dum - vector to indicate whether to include the observation                                  %
% Outputs: infl - influence function which has dimensions N x 1                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


    mu_dum =  mean(dum);
    mu_x_dum = mean(x.*dum);
    infl = (mu_dum.*(x.*dum - mu_x_dum) - mu_x_dum.*(dum - mu_dum))./(mu_dum.^2);


end
