function [ comparisonOutput ] = moment_comparison( params, d, momActual, Rmat, xmat, simDat, audMat, shocks)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Function Name: 	moment_comparison                                                                       %
% Date created: 	2018-10-26                                                                          %
% Author: 			Charles McClure                                                                     %
% Purpose: 			Compares the actual moments to the simulated moments. Significance is determined by bootstrapped  %
%------------------------------------------------------------------------------------------------------ %
% Inputs: params - vector of parameters                                                                %
%           d - dataset with parameters not estimated                                                    %
%          momActual - vector of actual moments     %
%          Rmat - a matrix of beginning reserves that is the same dimensionality as the state space     %
%          xmat - a matrix of current core earnings that is the same dimensionality as the state space     %
%          simDat - a matrix of simulated data where column 1 (2) is R (rp) for each NxS observation    %
%          audMat - a matrix random numbers that set whether the firm is audited                        %
%          shocks - a matrix that contains profitability shocks unrealted to tax avoidance                        %
% 
% Outputs: comparisonOutput - A 4 row matrix. First row is the actual moments. 
%               Second row is the simulated moment. Thrid row is the difference between the two. 
%               Fourth row is the p-value
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% TASK: 	Calculate simulated data                                                                       %
%                                                                                                       %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
simulatedData = simulate_data(d, params, Rmat, xmat, simDat, audMat, shocks);
NS = length(simulatedData);




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% TASK: 	Loop through bootstraps to compute simulated moments and statistics                                                                     %
%                                                                                                       %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% Initialize the matrix of simulated moments
simMoments = NaN( [ 10000 length( momActual ) ] );


parfor ii = 1:10000
    
%   Sample data with replacement
    tempData = datasample( simulatedData , NS);
    
%  Compute moments on the bootstrapped sample
    simMomentsB( ii , : ) = momentvec(d, tempData );
  
end

% Take the average of the moments
momSim = mean(simMomentsB);


% Compute the difference between the actual and the simulated moments
momDifference = momActual' - momSim;




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% TASK: 	Compute p-value for the differnce in moments                                                                       %
%                                                                                                       %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% Determine the number of bootstrapped samples that are less than the
% actual moment
inds = sum(simMomentsB < momActual');



% Compute p-values. If the sum is less than half of observations, p-value
% is fraction of observations less than actual moment
for ii = 1:length(inds)
    
    %   If more simulated moments are less than the actual moment
    if  inds( ii ) < NS / 2
        pValue( ii ) = inds( ii ) / NS;
   
    %   If more simulated moments are more than the actual moment
    else 
        
        pValue( ii ) = (NS - inds( ii ) ) / NS;
        
    end
end


comparisonOutput = [momActual'; momSim; momDifference; pValue];



end