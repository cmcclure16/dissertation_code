function [ V1 ] = value_func_wrap(d, parm, t_update, V, Rmat, R, rpmat, rp, r)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Function Name: 	value_func_wrap                                                                          %
% Date created: 	2018-09-05                                                                          %
% Author: 			Charles McClure                                                                     %
% Purpose: 			Wrapper for value function iteration to ensure reserve does not exceed state bounds %
%------------------------------------------------------------------------------------------------------ %
% Notes: 			Can can either 1 element for a matrix of values. t, V, R, rp must the same dimension%
%------------------------------------------------------------------------------------------------------ %
% Inputs:  d - dataset with parameters not estimated                                                    %
%          parm - dataset with model parameters                                                         %
%          t - tax payment. Can be 1 element or a matrix                                                %
%          V - Value from prior iteration                                                               %
%          Rmat - Matrix of total Reserve                                                               %
%          R - Total Reserve for the particular data point. If this is a matrix, should be equal to Rmat%
%          rpmat - Matrix of prior period reserve                                                       %
%          rp - Prior period reserve for the particular data point. If this is a matrix, should  = rpmat%
%          r - dataset of current reserves computed from the function rho. Also includes the gridded    % 
%              data interpolant                                                                         %
%          res - Reserve for the given t. Only needed if t is a single element                          %
% Outputs: V1 - Update value. Will be the same size as t, V, R, and rp                                  %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Ensure reserve is within bounds. Make the value function so bad that it
% forces an interior solution
res = r.F(t_update);
if res>d.maxrp
    V1 = -100000.0;
    
elseif res<d.minrp
    V1 = -100000.0;
    
    
else
    V1 = value_func(d, parm, t_update, V, Rmat, R, rpmat, rp, r, res);
end

end

