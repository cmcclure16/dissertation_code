/****************************************************************************************************/
/* Program Name: 	Tax_Data_Pull																	*/
/* Date created: 	2018-09-04																		*/
/* Author: 			Charles McClure																	*/
/* Purpose: 		Pulls all Compustat / CRSP data for tax model estimation 						*/
/*--------------------------------------------------------------------------------------------------*/
/* Macros: 			Macros are defined in Macros. WRDS links to Compustat / CRSP					*/
/*																									*/
/*--------------------------------------------------------------------------------------------------*/
/* Notes: 			None 																			*/
/*																									*/
/*--------------------------------------------------------------------------------------------------*/
/* Last Modified: 	2018-09-04																		*/
/* Modified by: 	Charles McClure																	*/
/* Description: 	First Complete Program															*/
/****************************************************************************************************/


*Set paths for project;
%let path = P:\My Documents\Research\Dissertation;
%let path_data = &path\data;
%let path_code = &path\code\build;
%let path_results = &path\results;
%let path_logs = &path\logs;

*Set file paths for raw data;
%let raw_data = D:data;
%let raw_data_comp = &raw_data\comp;
%let raw_data_crsp = &raw_data\crsp;
%let raw_data_crsp_ccm = &raw_data\crsp\ccm;
libname comp "&raw_data_comp.";
libname crsp "&raw_data_crsp.";
libname crspccm "&raw_data_crsp_ccm.";

*Set folders for data;
options user= "&path_data.\temp";
libname final "&path_data.\final";


*Load macros;
%include "&path_code.\macros\Macros.sas";


proc printto log="&path_logs.\build.txt"; run; quit;



/****************************************************************************************************/
/* TASK: 	Grab necessary data from Compustat and link together with WRDS										*/
/****************************************************************************************************/

*Pull data from Compustat/CRSP;
data compa;
	set comp.funda
		(keep=gvkey conm cusip datadate fyear at revt ceq seq exchg ebit oibdp 
		at consol datafmt popsrc curcd indfmt ni ib mii pi spi txt txpd txc txtubbegin TXTUBPOSPINC
		TXTUBPOSPDEC TXTUBPOSINC TXTUBPOSDEC TXTUBSETTLE TXTUBSOFLIMIT TXTUBADJUST TXTUBEND
		TXTUBXINTBS TXTUBXINTIS TXTUBTXTR ceq csho at lt xrd OANCF FINCF IVNCF prcc_f

		/*Variables for Frank et al (2009) measure*/
		TXFED TXFO TXDI INTAN ESUB MII TXS TLCF

		/*Variables for Rego et al (2012) measure*/
		revt xrd xsga DLTT DLC rect ppent oancf IBC XIDOC recch


		where=(fyear>= 2002 & consol="C" & datafmt="STD" & 
		popsrc="D" & curcd="USD" & indfmt="INDL" & gvkey ne "."));
run;

*Link Compustat and CRSP;
%mycstlink(preslack=180,slack=180);



*Link to CRSP;
proc sql;
create table compa2 as
	select a.*, b.lpermno as permno, b.linkdt, b.extlinkenddt
	from compa as a left join mycstlink as b
	on a.gvkey=b.gvkey
	and b.linkdt <= a.datadate <= b.extlinkenddt;
quit;


*Use CRSP to get SIC code and price/shares outstanding 3 months after FYE;
proc sql;
create table compa2 as select distinct a.*, b.siccd, b.namedt from compa2
	a left join crsp.stocknames b on 
	a.permno = b.permno and a.datadate between b.namedt and b.nameenddt; 
quit;

proc sql;
	create table compa2 as select distinct a.*, b.prc, b.shrout
	from compa2 a left join crsp.msf b on a.permno = b.permno and
	intck('Month', a.datadate, b.date) =3;
quit;




/****************************************************************************************************/
/* TASK: 	Clean up data set																		*/
/****************************************************************************************************/

*Clean up dataset by dropping unnecessary variables and reordering;
data compa3;
	set compa2;
	drop indfmt consol popsrc datafmt curcd linkdt extlinkenddt;
run;

proc sql;
	create table compa3 as select distinct gvkey, permno, fyear, datadate,
		conm, *
	from compa3;
quit;



/****************************************************************************************************/
/* TASK: 	Calculate Additional Variables 															*/
/****************************************************************************************************/


*Calculate BM, market cap, and ETR amounts;
data compa3;
	set compa3;
	bm = at / (at + prcc_f*csho - ceq);
	label bm = 'book to market';

	mkt_cap = prc*shrout/1000;
	label mkt_cap = 'Market Capitalization 3 months after FYE';

	mkt_cap_compustat = prcc_f*csho;
	label mkt_cap_compustat = 'Mkt Cap at FYE';

	
	*ETR data;
	gaap_etr = txt / (sum(pi,- SPI));
	cash_etr = txpd / (sum(pi,- SPI));
	pretax = (sum(pi,- SPI));

	*Set ETRs to missing if either the numerator or denominator are negative;
	if txt <0 or sum(pi,- SPI) <0 then gaap_etr=.;
	if txpd<0 or sum(pi,- SPI) <0 then gaap_etr=.;

	*Create 2-digit SIC code;
	sic2 = int(siccd/100);
	label sic2 = '2-digit SIC';

run;


*Calculate metrics based on rolling values (e.g., 3-year average ETRs);
proc sort data=compa3 out=compa4;
	by gvkey fyear;
quit;

*Open a new log for proc expand;
proc printto log="&path_logs.\build_expand.txt"; run;

proc expand data=compa4 out=compa4 method=none;
by gvkey;
id fyear;
	convert oancf = cfo_vol/ transformout=(movstd 5);
	convert cash_etr = cash_etr_vol/ transformout=(movstd 5);
	*Forward cash etr vol;
	convert cash_etr = cash_etr_vol_fwd / transformout=(reverse movstd 5 reverse);
run;

proc expand data=compa4 out=compa4 method=none;
by gvkey;
id fyear; 
	convert oancf = cfo_mean / transformout= (movave 5);
	
	*Components to calculate the long-run ETRs;
	convert txpd = cash_tax3 / transformout= (movave 3);
	convert txpd = cash_tax5 / transformout= (movave 5);
	convert txt = txt3 / transformout= (movave 3);
	convert txt = txt5 / transformout= (movave 5);

	convert pretax = pre_tax3 / transformout= (movave 3);
	convert pretax = pre_tax5 / transformout= (movave 5);

	convert pretax = pre_tax / transformout= (lag 5);

	*Lagged Assets;
	convert at = at_lag / transformout=(lag 1);
	
run;

*Close expand log and reopen main log file;
proc printto; run;
proc printto log="&path_logs.\build.txt"; run; quit;




*Set vol = 0 to missing becuase anything with only 1 observation will have sd = 0
Negative denominators in long-run ETRs are set to missing;
data compa4;
	set compa4;

	if cfo_vol=0 then
		cfo_vol = .;

	*Long-run ETRs;
	cash_etr3 = cash_tax3 / pre_tax3;
		label cash_etr3 = '3 Year average cash ETR';
	cash_etr5 = cash_tax5 / pre_tax5;
		label cash_etr3 = '5 Year average cash ETR';
	gaap_etr3 = txt3 / pre_tax3;
		label gaap_etr3 = '3 Year average GAAP ETR';
	gaap_etr5 = txt5 / pre_tax5;
		label gaap_etr3 = '5 Year average GAAP ETR';
	
	if pre_tax5 <0 then
		do;
			cash_etr5 = .;
			gaap_etr5 = .;
		end;

	if pre_tax3 <0 then
		do;
			cash_etr3 = .;
			gaap_etr3 = .;
		end;


run;


*Reorder variables;
proc sql;
	create table compa5 as select distinct gvkey, fyear, permno, datadate, conm, cusip, * 
	from compa4;
quit;



/****************************************************************************************************/
/* TASK: 	Calculate Predicted UTB as in Rego and Wilson (2012) 									*/
/****************************************************************************************************/

*Grab Geographic Segments from Compustat;
proc sql; create table geo_seg as select distinct gvkey, stype, sid, datadate, sales, 
	sics1, int(input(sics1,12.)/100) as sic2, GEOTP from 
	comp.wrds_segmerged(where=(stype = 'GEOSEG' and datadate>mdy(1,1,2000)));
quit;


*Calculate the foreign sales;
data geo_seg;
	set geo_seg;

	if geotp = 3;
run;

proc means data=geo_seg noprint sum;
	var sales;
	class datadate / order=unformatted ascending;
	class gvkey / order=unformatted ascending;
	output out=geo_summary sum()=For_Sales / autoname autolabel ways;
run;

proc sql;
	create table compa6 as select a.*, b.For_Sales 'Foreign Sales'
		from compa5 a left join geo_summary b on a.gvkey = b.gvkey and a.datadate= b.datadate;
quit;

data compa6;
	set compa6;

	if For_Sales = . then
		For_Sales = 0;
	For_Sales_revt = For_Sales / revt;

	for_ind = 0;
	if For_Sales > 0 then for_ind = 1;
run;



*Calculate sales growth - 3 year average;
proc printto log="&path_logs.\build_expand.txt"; run;

proc expand data=compa6 out=compa6 method=none;
	by gvkey;
	id fyear; 
	convert revt = revt_lag1 / transformout=(lag 1);
run;

data compa6;
	set compa6;
	sales_growth = (revt - revt_lag1) / revt_lag1;
	label sales_growth = 'YoY Sales growth';
run;


proc expand data=compa6 out=compa6 method=none;
	by gvkey;
	id fyear; 
	convert sales_growth = sales_growth_3yr / transformout= (movave 3);
run;

*Close proc expand log and re-open main log file;
proc printto;run;
proc printto log="&path_logs.\build.txt"; run; quit;


*Calculate Discretionary Accruals;
data compa6;
	set compa6;
	TACCR = (IBC - (OANCF - XIDOC))/ at_lag;
	label TACCR = 'Total Accruals by assets';
	SSA = ((revt - revt_lag1) + recch) / at_lag;
	label SSA = 'Change in sales plus A/R';
	SPPENT = PPENT / at_lag;
	label SPPENT = 'PP&E (net) by assets';
	roa = sum(pi,-spi) / at_lag;
	label roa = 'ROA - PI / AT';
	at_inv = 1 / at;
	label at_inv = 'Inverse of total assets';
run;

proc sort data=compa6;
by fyear sic2;
quit;

proc reg data=compa6 (where=(TACCR ne . and SSA ne . and SPPENT ne . and roa ne . and at_inv ne .)) noprint;
	by fyear sic2;
	model TACCR = at_inv SSA SPPENT ROA;
	output out=DA_dset p = DA;
quit;


proc sql;
	create table compa6 as select distinct a.*, b.DA 
		from compa6 a left join DA_dset b on a.gvkey = b.gvkey and a.datadate = b.datadate;
quit;



*Calculate other variables used in the Rego regression;
data compa6;
	set compa6;
	log_at = log(at);
	label log_at = 'Natural log of assets';

	lev = sum(DLTT, DLC) / at_lag;
	rnd = xrd / at_lag;
	label rnd = 'R&D by assets';

	mtb = (csho*prcc_f) / ceq;
	SGA = xsga / at_lag;
	label SGA = 'Sales, General, and Administrative by assets';

	utb_at_lag = txtubend / at_lag;
run;



*Winsorize and run regression;
%Winsorize_Truncate(dsetin = compa6, dsetout = rego_regression, 
	vars =  utb_at_lag roa log_at For_Sales_revt rnd lev DA sga mtb sales_growth_3yr, 
	type = W, pctl = 1 99);


proc surveyreg data= rego_regression (where=(roa ne . and log_at ne . and For_Sales_revt ne . and rnd ne . and lev ne . and DA ne . and sga ne .
	and mtb ne . and sales_growth_3yr ne . and txtubend ne .));

	model utb_at_lag = roa log_at For_Sales_revt rnd lev DA sga mtb sales_growth_3yr  / solution;
	output out=Pred_UTB_dset p = Pred_UTB;
quit;


*Merge back into main dataset;
proc sql;
	create table compa7 as select distinct a.*, b.Pred_UTB 'Predicted UTB by assets'
		from compa6 a left join Pred_UTB_dset b on a.gvkey = b.gvkey and a.datadate = b.datadate;
quit;



/****************************************************************************************************/
/* TASK: 	Compute variables for Matlab 															*/
/****************************************************************************************************/


*Calculate FF12 and 48 industry classification;
%ind_ff12(dset=compa7, outp = compa7, sic=siccd, ind_code = ff12);
%FF_Industry(dsetin=compa7, dsetout=compa7, sic=siccd);

*Set certain amounts that are missing to zero;
data analysis_data;
	set compa7;

	if spi = . then spi = 0;

	if mii = . then mii = 0;

	if xrd = . then xrd = 0;
	
	*Adjustments to UTB;
	if TXTUBPOSPINC =. then TXTUBPOSPINC =0;
	if TXTUBPOSPDEC =. then TXTUBPOSPDEC =0;
	if TXTUBPOSINC =. then TXTUBPOSINC =0;
	if TXTUBPOSDEC =. then TXTUBPOSDEC =0;
	if TXTUBSETTLE  =. then TXTUBSETTLE =0;
	if TXTUBSOFLIMIT =. then TXTUBSOFLIMIT =0;
	if TXTUBADJUST =. then TXTUBADJUST =0;
run;

*Calculate particular UTB amounts;
data analysis_data;
	set analysis_data;
	UTB_change = sum(txtubend, -txtubbegin);
	UTB_curr = TXTUBPOSINC - TXTUBPOSDEC;
	UTB_prior = TXTUBPOSPINC - TXTUBPOSPDEC;
run;



*Open proc expand log;
proc printto log="&path_logs.\build_expand.txt"; run;

*Compute next year settle and limit amounts;
proc expand data=analysis_data out=analysis_data method=none;
by gvkey;
id fyear;
	convert txtubsettle = txtubsettle_lead / transformout=(lead 1);
	convert txtubsoflimit = txtubsoflimit_lead / transformout=(lead 1); 
run;


*Close proc expand log and re-open main log file;
proc printto;run;
proc printto log="&path_logs.\build.txt"; run; quit;



*Compute audit, pre-tax income and future R;
data analysis_data;
	set analysis_data;

	audit = 0;
	if txtubsettle_lead >0 then audit = 1;
	label audit = 'Indicates if a firm was audited';

	R = txtubend - UTB_curr;
	label R = 'Beginning Reserve';
	
	pre_tax = sum(pi, -spi);

	R_t1 = txtubend - txtubsettle_lead - txtubsoflimit_lead;
	label R_t1 = 'Next Period Reserve';

run;

*Lagged prior reserve, current reserve, assets, and sales;
proc sort data=analysis_data;
	by gvkey fyear;
quit;

*Open proc expand log;
proc printto log="&path_logs.\build_expand.txt"; run;

proc expand data=analysis_data method=none out=analysis_data;
	by gvkey;
	id fyear;
	
	*Lagged values;
	convert UTB_curr = UTB_curr_lag /transformout= (lag 1) method=none;
	convert R = R_lag /transformout= (lag 1) method=none;
quit;

*Close proc expand log and re-open main log file;
proc printto;run;
proc printto log="&path_logs.\build.txt"; run; quit;


*Calculate scaled values that will be used in matlab;
data analysis_data;
	set analysis_data;

	pre_tax = sum(pi,-spi);

	*Scale by lagged assets outstanding;
	R_s = 100*R / (at_lag);
	R_t1_s = 100*R_t1 / at_lag;
	pre_tax_s = 100*pre_tax / (at_lag);
	UTB_curr_s = 100*UTB_curr / (at_lag);
	UTB_curr_lag_s = 100*UTB_curr_lag / (at_lag);
	txc_s = 100*txc / (at_lag);
	txc_no_res_s = 100*sum(txc, -UTB_curr) / at_lag;
	settle_s = 100*txtubsettle_lead / (at_lag);
	lapse_s = 100*txtubsoflimit_lead / (at_lag);
	TXTUBPOSINC_s = 100*TXTUBPOSINC / at_lag;
	TXTUBPOSDEC_s = 100*TXTUBPOSDEC / at_lag;
	txtubend_s = txtubend / at_lag;
	
	*Other stuff for moments;
	lapse_ratio = txtubsoflimit / (R);
	settle_ratio = txtubsettle_lead / (R + UTB_curr);
	res_pi = UTB_curr / pre_tax;
	res_rp = UTB_curr / UTB_curr_lag;
	res_R = UTB_curr / R;
	res_var = (UTB_curr - UTB_curr_lag)*(UTB_curr - UTB_curr_lag);
		res_var_s = res_var / at_lag;
	audit_risk = UTB_curr +R;
		audit_risk_s = 100*audit_risk / at_lag;
	settle_rp = txtubsettle_lead / UTB_curr_lag;
	settle_res = txtubsettle_lead / UTB_curr;

	*Other Data;
	sga_ratio = xsga / pre_tax;

run;



/****************************************************************************************************/
/* TASK: 	Compute Compustat Percentiles															*/
/****************************************************************************************************/

proc sort data=analysis_data;
	by fyear;
quit;


proc rank data=analysis_data groups=100 out=analysis_data;
	by fyear;
	var at roa bm mkt_cap_compustat txc R utb_curr utb_curr_lag pre_tax audit TXTUBPOSINC TXTUBPOSDEC
		txtubend txtubsettle_lead txtubsoflimit gaap_etr txt;
	ranks at_rank roa_rank bm_rank mkt_cap_compustat_rank txc_rank R_rank utb_curr_rank utb_curr_lag_rank pre_tax_rank
		audit_rank TXTUBPOSINC_rank TXTUBPOSDEC_rank txtubend_rank  txtubsettle_rank txtubsoflimit_rank gaap_etr_rank
		txt_rank;
quit;



/****************************************************************************************************/
/* TASK: 	Create final sample for estimation 														*/
/****************************************************************************************************/

***Sample Selection;
data final_data; /*Should be 101517 */
	set analysis_data;
	
	valid_obs = 0;
	if fyear>2006 and fyear < 2017 then valid_obs =1;
	label valid_obs = 'Valid Observation';
	if valid_obs = 1;
run;

data final_data; /*Should be 73582 */
	set final_data;

	valid_obs = 0;
	if at_lag>0 then 
		valid_obs = 1;

	if valid_obs = 1;
run;


data final_data; /*Should be 46715*/
	set final_data;

	valid_obs = 0;
	if sum(pi,-spi)>0 then 
		valid_obs = 1;

	if valid_obs = 1;
run;


data final_data; /*Should be 19048 */
	set final_data;

	valid_obs = 0;
	if R>0 then 
		valid_obs = 1;
	if valid_obs = 1;
run;


data final_data; /*Should be 14404*/
	set final_data;

	valid_obs = 0;
	if UTB_curr>0  then 
		valid_obs = 1;
	if valid_obs = 1;
run;


data final_data; /*Should be 12787*/
	set final_data;

	valid_obs = 0;
	if 	txtubsettle_lead ~=.  and txtubsoflimit_lead~=.  then 
		valid_obs = 1;
	if valid_obs = 1;
run;



/****************************************************************************************************/
/* TASK: 	Winsorize Data 																			*/
/****************************************************************************************************/

%Winsorize_Truncate(dsetin = final_data, dsetout = final.final_data, 
	vars = at lt ni ib pi mii spi xrd xsga cfo_vol ebit oibdp txt txpd txc txtubbegin UTB_change
	UTB_curr UTB_prior txtubend ceq bm csho roa mkt_cap_compustat TXTUBPOSINC TXTUBPOSDEC
	sga_ratio txtubend_s 

	/*Stuff for matlab*/
	R R_t1 pre_tax UTB_curr UTB_curr_lag txc txtubsettle txtubsoflimit gaap_etr
	R_s pre_tax_s UTB_curr_s UTB_curr_lag_s txc_s settle_s lapse_s TXTUBPOSINC_s TXTUBPOSDEC_s prcc_f txc_no_res_s
	lapse_ratio settle_ratio R_t1_s res_pi res_rp res_var res_var_s audit_risk audit_risk_s res_R settle_res 
	settle_rp at_lag
		
	/*Additional avoidance and risk measures*/
	Pred_UTB cash_etr_vol cash_etr_vol_fwd cash_etr3 cash_etr5 gaap_etr3 gaap_etr5 utb_at_lag, 
	type = W, pctl = 5 95);


	proc export data=final.final_data
	    outfile="&path_data./final/all_data.csv"
	    dbms=csv
	    replace;
	run;




/****************************************************************************************************/
/* TASK: 	Create CSV file for output to Matlab													*/
/****************************************************************************************************/

*Keep on the columns used in estimation;
proc sql;
	create table final.estimation_data as select distinct gvkey, fyear, R_s as R, utb_curr_s as res, utb_curr_lag_s as rp, txc_s as txc, 
		txc_no_res_s, pre_tax_s as pi, audit, settle_ratio as settle, lapse_ratio as lapse, R_t1_s as Rt1, res_pi, res_rp, 
		res_R, res_var_s as res_var, audit_risk_s as audit_risk, settle_res, settle_rp, at_lag, for_ind
	from final.final_data;
quit;

proc export data=final.estimation_data
    outfile="&path_data./final/estimation_data.csv"
    dbms=csv
    replace;
run;



/****************************************************************************************************/
/* TASK: 	Compute summary statistics																*/
/****************************************************************************************************/

%better_means(data=final.final_data, keepdata=Y, out=utb_summary_stats, 
		stts= N mean median stddev p5 p25 p75 p95, 
		varlst= at lt ni ib pi mii spi xrd cfo_vol ebit oibdp txt txpd txc txtubbegin UTB_change
			UTB_curr utb_curr_lag UTB_prior txtubend ceq bm roa mkt_cap_compustat TXTUBPOSINC 
			TXTUBPOSDEC txtubsettle txtubsoflimit pre_tax txt sga_ratio xsga revt

			/*Per share data*/
			prc gaap_etr cash_etr
			TXTUBPOSINC_s TXTUBPOSDEC_s prcc_f R_s pre_tax_s utb_curr_lag_s utb_curr_s audit settle_s lapse_s

			settle_ratio lapse_ratio
	
			/*Tax avoidance and risk measures*/
			Pred_UTB cash_etr_vol cash_etr_vol_fwd cash_etr3 cash_etr5 gaap_etr3 gaap_etr5 utb_at_lag
			utb_curr_lag
			
			/*Compustat Ranked amounts*/
			at_rank roa_rank bm_rank mkt_cap_compustat_rank gaap_etr_rank);




ods tagsets.ExcelXP path="&path_results."
	file='Raw Summary Stats.xls' style=printer options(sheet_name='Summary Stats (Raw)');
proc print data=utb_summary_stats noobs;
 run; quit;
ods tagsets.ExcelXP close;


/****************************************************************************************************/
/* TASK: 	Determine number of unique firms														*/
/****************************************************************************************************/

*Number of unique firms;
proc sql;
create table unique_firms as select distinct gvkey
from final.final_data;
quit;



/****************************************************************************************************/
/* TASK: 	Closing the log / Finishing the program													*/
/* 																									*/
/****************************************************************************************************/
/* Turning off the log */

proc printto; run; quit;





