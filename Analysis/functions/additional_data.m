function [ real_data ] = additional_data(real_data)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Function Name: 	additional_data                                                                     %
% Date created: 	2018-10-04                                                                          %
% Author: 			Charles McClure                                                                     %
% Purpose: 			Computes additional ratios of actual data that can be used as moments               %
%------------------------------------------------------------------------------------------------------ %
% Inputs:  real_data - table with real_data                                                             %
% Outputs: real_data - Returns the input but with the additional ratios etc. as columns                  %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Settle amount (in dollars)
real_data.settleAmt = real_data.settle.*(real_data.res + real_data.R);

% Lapse amount (in dollars)
real_data.lapseAmt = real_data.lapse.*(real_data.R);

% Log of preTax Income 
real_data.logPreTax = log(real_data.pi);

% Log of current reserve 
real_data.logRes = log(real_data.res);

% Settle Rate x R
real_data.settleR = real_data.settle.*real_data.R;

% Reserve / preTax
real_data.resPi = real_data.res ./ real_data.pi;

% PreTax / Reserve
real_data.piRes = real_data.pi ./ real_data.res;

% PreTax / R
real_data.piR = real_data.pi ./ real_data.R;

% PreTax / R + res

real_data.piRRes = real_data.pi ./ (real_data.R + real_data.res);

% Reserve / R
real_data.resR = real_data.res./ real_data.R;

end
