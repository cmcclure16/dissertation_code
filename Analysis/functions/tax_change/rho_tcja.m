function [ resOptions ] = rho_tcja(d, parm)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Function Name: 	rho                                                                                 %
% Date created: 	2018-09-05                                                                          %
% Author: 			Charles McClure                                                                     %
% Purpose: 			Computes the current period UTB reserve                                             %
%------------------------------------------------------------------------------------------------------ %
% Inputs:  d - dataset with parameters not estimated                                                    %
%          parm - dataset with model parameters                                                         %
% Outputs: resOptions - A three row matrix with all possible reserves for differen levels of tau        %
%                       1. Vector of taus                                                               %
%                       2. Reserve for the last project for a given tau                                 %
%                       3. Integrates reserves for all project (row 2) via simpson rule to get current  %
%                          reserve                                                                      %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    % Create variables for model parameters 
    alpha = parm.alpha;
    phi = parm.phi;
    
    
    % Create a 1-D grid of all possible values of tau
    tOptions = linspace(0.209, 0.03, d.node2);
    
    
    % Compute expected repayment and reserve for each project
	mu_s = (tOptions./0.21).^ alpha;
    rhos = 1.0 - betainv(0.5,1.0./mu_s,(1.0-mu_s)./(mu_s.^2));
    
    
    % Create output, leaving last row blank
    resOptions = [tOptions; rhos]; 
    resOptions(3,:)  = zeros([1 length(resOptions(1,:))]);
    
    % Compute current period reserve for each option
    for ii = 3:length(resOptions)
        resOptions(3,ii) = (1- phi*0.21 + phi*resOptions(1,ii))*simpsons(resOptions(2,1:ii),resOptions(1,ii),0.209);
    end

end