function [ output ] = reserve_expect(parm , R, t)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Function Name: 	reserve_expect
% Date created: 	2018-11-13                                                                          
% Author:             Charles McClure                                                                     
% Purpose:          Computes the underlying repayment when audits and insepction rates are included
%                       See Wolfram Alpha code for the formula
%---------------------------------------------------------------------------------------------------------------------------------- 
% Inputs:  parm - dataset of parameters                                                               
%              R - Total UTB reserve. Can be a vector or scalar
%              t - tax rate. Can be a vector or a scalar
% Outputs: counter_effect - Table that reports the effect of the counterfactual                                                        
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Create a variable for the underlying distribution
lambda = underlying(parm, t);

% Compute the new reserve assuming they report the new reserve to the tax
% authority
output = ( 1.0 - parm.phi * 0.35 +  parm.phi .* t ) .* ...
     (-1.0 - parm.eta .* R + parm.p .* parm.eta .* lambda  + ...
    sqrt(4 .* parm.p .* (parm.eta .^ 2) .* R .* lambda + ( 1 + parm.eta .* R - parm.p .* parm.eta .*lambda ) .^2 )) ./ (2 .* parm.eta); 



end
