function [ infl ] = cov_infl(x, y)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Function Name: 	cov_infl                                                                           %
% Date created: 	2018-10-04                                                                          %
% Author: 			Charles McClure                                                                     %
% Purpose: 			Computes the influence function for an unconditional covaraince                     %
%------------------------------------------------------------------------------------------------------ %
% Inputs:  x - vector from which to compute the covariance                                              %
%          y - vector from which to compute the covariance                                              %
% Outputs: infl - influence function as (xy - mean(xy) - mean(y)*(x-mean(x)) mean(x)*(y-mean(y)))       %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

mu_xy = mean(x.*y);
mu_x = mean(x);
mu_y = mean(y);


infl = x.*y - mu_xy - mu_y.*(x - mu_x) - mu_x.*(y - mu_y);

end
