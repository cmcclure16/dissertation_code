function [Covar, se, t_stat, lambda, lambda_std] = SE( params, d, Rmat, xmat, simDat, audMat, shocks, W)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Function Name: 	SE                                                                    
% Date created: 	2018-10-19                                                                          
% Author: 			Charles McClure                                                                     
% Purpose: 			Computes the standard errors, covariance, t-stats, and lambda for the model                                    
%------------------------------------------------------------------------------------------------------ 
% Inputs:  d - dataset with parameters not estimated                                                   
%          params - vector of parameters
%           d - dataset with parameters not estimated                                                    
%          Rmat - a matrix of beginning reserves that is the same dimensionality as the state space     
%          xmat - a matrix of current core earnings that is the same dimensionality as the state space     
%          simDat - a matrix of simulated data where column 1 (2) is R (x) for each NxS observation    
%          audMat - a matrix random numbers that set whether the firm is audited                        
%          shocks - a matrix that contains profitability shocks unrealted to tax avoidance                        
%          W - a matrix random numbers that set whether the firm is audited                        
% Outputs: Covar - covariance matrix
%          se - Vector of standard errors for the parameters
%          t_stat - Vector of t-stats for the parameters
%          lambda - Parameter sensitivity to a particular moment (this has the dimension as parameters x moments) as in Alexander et al. (2016)
%          lambda_std - Standardized lambda that scales by the weight and covariance matrix 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% TASK: 	Loop through each paramter, perterb up and down by d.epsilon to compute the Jacobian                                   %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


for pp = 1:length(params)
 
%  Determine step size
 h = abs( params( pp ) ) .* d.epsilon;
 
%Upper Bound    
params_temp = params;
params_temp(pp) = params_temp( pp ) + h;
data = simulate_data(d , params_temp , Rmat , xmat , simDat , audMat , shocks);
temp_moment_high = momentvec( d , data );


%Lower Bound
params_temp = params;
params_temp( pp ) = params_temp( pp ) - h;
data = simulate_data(d , params_temp , Rmat , xmat , simDat , audMat , shocks);
temp_moment_low = momentvec( d , data);

% Compute Jacobian
jacobian( pp , : ) = ( temp_moment_high - temp_moment_low) / (2 * h);

end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% TASK: 	Compute the covariance of model parameters                                   %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%Covariance
Covar = (1 + 1/d.S) * inv( jacobian * W * jacobian' );

%Standard Errors
se = diag( sqrt( Covar ) )';

%T-stats
t_stat = params ./ se;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% TASK: 	Compute lambda and standardized lambda as in Andrews et al. (2016)                                   %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%Lambda
lambda = -inv( jacobian * W * jacobian' ) * jacobian * W;

%Standardized Lambda
[p, j] = size( lambda );
lambda_std = NaN( size( lambda ) );

for pp = 1:p
    for jj = 1:j
        lambda_std( pp, jj ) = lambda( pp , jj ) * sqrt( inv( W( jj , jj ) ) / Covar( pp, pp ) );
    end
end

lambda_std = abs( lambda_std );


end

