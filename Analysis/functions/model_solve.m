function [ t_update, V ] = model_solve(parm, d, t, Rmat, xmat, r)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Function Name: 	model_solve                                                                         %
% Date created: 	2018-09-05                                                                          %
% Author: 			Charles McClure                                                                     %
% Purpose: 			Computes the optimal policy and value function using two-step policy iteration      %
%------------------------------------------------------------------------------------------------------ %
% Inputs:  parm - dataset with model parameters                                                         %
%          d - dataset with parameters not estimated                                                    %
%          t - initial guess for policy function. Should be a scalar                                    %
%          Rmat - a matrix of beginning reserves that is the same dimensionality as the state space     %
%          xmat - a matrix of core earnings %
%          r - dataset of current reserves computed from the function rho. Also includes the gridded    % 
%              interpolant data                                                                         %
% Outputs: t_update - Updated policy function                                                           %
%          V - Updated value function                                                                   % 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%  Pull in globals
global options
options_optim = options;

%Set tolerance, iteration counts and initialize stopping criteria
tol = d.tol;
dist = 10*tol;
dist2 = dist;
iter = 0;
iter2 = 0;
max_it = d.maxit;
temp_pts = d.pts2;    

% Define "compound" params to save computation time
parm.omtheta = 1.0 - parm.theta;
parm.alphap1 = 1.0 + parm.alpha;
parm.alpha35 = parm.alpha * 0.35;


% Initialize policy function such that I get variation along 2nd
% dimension which is necessary for interpolation
tmat = linspace(0.2,t,d.pts)'*linspace(0.51,1,d.pts2);


% Initialize value function to be pre-tax income
V =  xmat.* ( 1.0 - parm.phi*0.35 + parm.phi.*tmat ).* ( 1 - tmat);
    
    
% Compute 2-step policy iteration based on the tolerance and stopping
% criteria
while dist2 > tol && iter2 <=max_it 


  % Solve Bellman Equation
    while dist > tol && iter <= max_it
        
        % Compute value function        
        V1 =  value_func(d, parm, tmat, V, Rmat, Rmat, xmat, xmat, r);
        
       
        % Calculate Distance and update iteration count
        temp = abs(max(V - V1)); 
        dist = max(temp(:));
        iter = iter + 1;
        V = V1;
    end

    
  % Solve for optimal policy
    parfor ii = 1:d.pts
        
            % Pull unique R for each data point. Note that each column has the
            % same vector of R
            R_temp = Rmat( ii , 1 );

            for jj = 1:temp_pts
                
                % Pull unique rp for each data point. Note that row is the
                % same
                x_temp = xmat( 1 , jj );
                
                % Pull unique policy and ensure it is bounded between appropirate values                
                t_temp = tmat( ii , jj );
                
                if t_temp>0.34
                    t_temp = 0.34;
                elseif t_temp<0.05
                    t_temp = 0.05;
                end       


                % Set anonomyous function and optimize
                Manager_Obj = @(t)( -value_func(d , parm , t , V , Rmat , R_temp , xmat , x_temp , r));
                [t_update( ii , jj ), ~, ~, ~] = fmincon( Manager_Obj , t_temp , [-1; 1] , [-0.05; 0.34] , [] , [] , [] , [] , [] , options_optim );

            end
    end
    
    
       %Update steps and determine stopping criteria
       iter2 = iter2 + 1;
       iter = 0;
       dist = tol * 100;
       dist2 = max(max(abs(t_update - tmat)));
       tmat= t_update;

end


end